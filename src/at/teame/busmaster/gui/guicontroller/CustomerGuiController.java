package at.teame.busmaster.gui.guicontroller;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.NotUpdateableException;
import at.teame.busmaster.controller.coredata.CustomerController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.ContactDetailsTypeDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.MailContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.TelContactDetailsDI;
import at.teame.busmaster.domain.guidata.coachtour.GuiCoachTourCustomer;
import at.teame.busmaster.gui.ConfigLoading;

public class CustomerGuiController extends ContactDetailGuiController {
	private CustomerController _controller = new CustomerController();
	
    private static CustomerGuiController _instance;
    
    public static CustomerGuiController getInstance()
    {
        if(_instance == null && ConfigLoading.getSetting() == ConfigLoading.DO_LOADING)
        {
            _instance = new CustomerGuiController();
        }
        return _instance;
    }
    
    private CustomerGuiController()
    {
        
    }
    
    /**
     * 
     * @return the list of all states
     */
    public List<? extends StateDI>getAllStates(){
        return _controller.getAllStates();
    }
	/**
	 * 
	 * @return the list of all customers
	 */
	public List<? extends CoachTourCustomerDI> getAllCustomers(){
		return _controller.getAllCustomer();
	}
	/**
	 * 
	 * @param firstName of the customer
	 * @param lastName of the customer
	 * @param jurPerson legal person if the customer is one
	 * @param street of the address
	 * @param houseNumber of the address
	 * @param zip of the address
	 * @param city of the address
	 * @param state of the address
	 * @param numbers list with all phone numbers
	 * @param phoneTypes list with the types to the phone numbers
	 * @param addresses list with all mail addresses
	 * @param mailTypes list with the types to the mail addresses
	 */
	public void saveCustomer(String firstName, String lastName, String jurPerson, String street, String houseNumber, String zip, String city, StateDI state,List<String> numbers, List<ContactDetailsTypeDI> phoneTypes, List<String> addresses, List<ContactDetailsTypeDI> mailTypes) throws UncompatibleConvertionException, PeriodException{
		
		 List<TelContactDetailsDI> phones = new LinkedList<>();
		 List<MailContactDetailsDI> mails = new LinkedList<>();
		 GuiCoachTourCustomer customer = new GuiCoachTourCustomer(firstName, lastName, jurPerson, street, houseNumber, zip, city, state,phones, mails);
		 
		 Iterator<String> phone = numbers.iterator();
		 Iterator<ContactDetailsTypeDI> phoneType = phoneTypes.iterator();
		 Iterator<String> mail = addresses.iterator();
		 Iterator<ContactDetailsTypeDI> mailType = mailTypes.iterator();
		 while(phone.hasNext()){
			 phones.add(createPhoneContact(phone.next(), phoneType.next(), customer, null));
		 }
		 while(mail.hasNext()){
			 mails.add(createMailContact(mail.next(), mailType.next(), customer, null));
		 }
		_controller.saveCustomer(customer);
	}
	/**
	 * 
	 * @param toUpdate the customer to update
	 * @param firstName of the customer
	 * @param lastName of the customer
	 * @param jurPerson legal person if the customer is one
	 * @param street of the address
	 * @param houseNumber	of the address
	 * @param zip of the address
	 * @param city of the address
	 * @param state of the address
	 * @param numbers list with all phone numbers
	 * @param phoneTypes list with the types to the phone numbers
	 * @param addresses list with all mail addresses
	 * @param mailTypes list with the types to the mail addresses
	 * @throws UncompatibleConvertionException
	 * @throws NotUpdateableException
	 * @throws PeriodException
	 */
	public void updateCustomer(CoachTourCustomerDI toUpdate,String firstName, String lastName, String jurPerson, String street, String houseNumber, String zip, String city, StateDI state,List<String> numbers, List<ContactDetailsTypeDI> phoneTypes, List<String> addresses, List<ContactDetailsTypeDI> mailTypes) throws UncompatibleConvertionException, NotUpdateableException, PeriodException{
		 List<TelContactDetailsDI> phones = new LinkedList<>();
		 List<MailContactDetailsDI> mails = new LinkedList<>();
		 GuiCoachTourCustomer customer = new GuiCoachTourCustomer(firstName, lastName, jurPerson, street, houseNumber, zip, city, state,phones, mails);
		 
		 Iterator<String> phone = numbers.iterator();
		 Iterator<ContactDetailsTypeDI> phoneType = phoneTypes.iterator();
		 Iterator<String> mail = addresses.iterator();
		 Iterator<ContactDetailsTypeDI> mailType = mailTypes.iterator();
		 while(phone.hasNext()){
			 phones.add(createPhoneContact(phone.next(), phoneType.next(), customer, null));
		 }
		 while(mail.hasNext()){
			 mails.add(createMailContact(mail.next(), mailType.next(), customer, null));
		 }
		_controller.update(toUpdate, customer);
	}
	/**
	 * 
	 * @param customer the customer to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeCustomer(CoachTourCustomerDI customer) throws PeriodException, UncompatibleConvertionException{
		_controller.removeCustomer(customer);
	}

}
