package at.teame.busmaster.gui.guicontroller.wrapper;

import at.gruppe2.domain.bus.IBus;

public class BusWrapper {
	private IBus _contains;
    private DayTypeWrapper _dayType;
	
	@SuppressWarnings("unused")
	private BusWrapper(){};
	
	public BusWrapper(IBus toWrap) {
		_contains = toWrap;
	}
	
    @Override
	public String toString() {
        if(_contains == null)
        {
            return "";
        }
        if(_dayType != null && !_contains.isAvailable(_dayType.getDayType()))
        {
            return _contains.toString() + "(!)";
        }
        return _contains.toString();
	}
	
	public IBus getBus() {
		return _contains;
	}

    public void setDayType(DayTypeWrapper _dayType)
    {
        this._dayType = _dayType;
    }
}
