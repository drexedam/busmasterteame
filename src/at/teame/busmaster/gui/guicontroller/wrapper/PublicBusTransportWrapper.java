package at.teame.busmaster.gui.guicontroller.wrapper;

import at.gruppe2.domain.publicbustransport.IPublicBusTransport;
import at.gruppe2.domain.publicbustransport.IRouteSection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class PublicBusTransportWrapper {
	private IPublicBusTransport _contains;
    private static final DateFormat _FORMAT = new SimpleDateFormat("HH:mm");
	
	@SuppressWarnings("unused")
	private PublicBusTransportWrapper(){};
	
	public PublicBusTransportWrapper(IPublicBusTransport toWrap) {
		_contains = toWrap;
	}
	
	public String toString() {
		return _contains.toString();
	}
	
	public IPublicBusTransport getPublicBusTransport() {
		return _contains;
	}
    
    public String getNumber()
    {
        return _contains.getRoute().getNumber();
    }
    public String getStart()
    {
        return _FORMAT.format(_contains.getStart());
    }
    public String getEnd()
    {
        return _FORMAT.format(_contains.getEnd());
    }

    public String getStartStaion()
    {
        //idea: delegating but this is part of the excanges and TeamF does not want to.
        List<IRouteSection> section = (List<IRouteSection>) _contains.getRoute().getAllSections();
        if(section == null || section.isEmpty())
        {
            return "ERROR";
        }
        return _contains.getRoute().getAllSections().get(0).getStart().getName();
    }

    public String getEndStaion()
    {
        //idea: delegating but this is part of the excanges and TeamF does not want to.
        List<IRouteSection> section = (List<IRouteSection>) _contains.getRoute().getAllSections();
        if(section == null || section.isEmpty())
        {
            return "ERROR";
        }
        return section.get(section.size()-1).getEnd().getName();
    }
}
