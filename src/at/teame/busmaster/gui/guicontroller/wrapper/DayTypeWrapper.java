package at.teame.busmaster.gui.guicontroller.wrapper;

import at.gruppe2.domain.schedule.IDayType;

public class DayTypeWrapper {
	private IDayType _contains;

	@SuppressWarnings("unused")
	private DayTypeWrapper() {}
	
	public DayTypeWrapper(IDayType toWrap) {
		_contains = toWrap;
	}
	
	public String toString() {
		return _contains.getSpecification() + " " + _contains.getDescription();
	}
	
	public IDayType getDayType() {
		return _contains;
	}
}
