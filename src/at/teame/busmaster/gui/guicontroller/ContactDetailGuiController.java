package at.teame.busmaster.gui.guicontroller;

import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.controller.coredata.ContactDetailsTypeController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.ContactDetailsTypeDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.MailContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.TelContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.guidata.contactdetails.GuiContactDetailsType;
import at.teame.busmaster.domain.guidata.contactdetails.GuiMailContactDetails;
import at.teame.busmaster.domain.guidata.contactdetails.GuiTelContactDetails;

public class ContactDetailGuiController {
	
	private ContactDetailsTypeController _controller= new ContactDetailsTypeController();
	/**
	 * 
	 * @return the list of all contact details types
	 */
	public List<? extends ContactDetailsTypeDI> getAllContactDetailsTypes(){
		return _controller.getAllContactDetailsTypes();
	}
	/**
	 * 
	 * @param specification of the contact type
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 * @throws DBException
	 */
	public void createContactDetailsType(String specification) throws PeriodException, UncompatibleConvertionException, DBException{
		GuiContactDetailsType type = new GuiContactDetailsType(specification);
		_controller.createContactDetailsType(type);
	}
	/**
	 * 
	 * @param type the contact details type to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeContactDetailsType(ContactDetailsTypeDI type) throws PeriodException, UncompatibleConvertionException{
		_controller.removeContactDetailsType(type);
	}
	
	/**
	 * 
	 * @param number telephone number
	 * @param type the phone type
	 * @param customer the customer of the number
	 * @param driver the driver of the number
	 * @return
	 */
	protected TelContactDetailsDI createPhoneContact(String number, ContactDetailsTypeDI type, CoachTourCustomerDI customer, DriverDI driver){
		return new GuiTelContactDetails(number, driver, customer, type);
	}
	/**
	 * 
	 * @param address the mail address
	 * @param type the mail type
	 * @param customer the customer of the mail
	 * @param driver the driver of the mail
	 * @return
	 */
	protected MailContactDetailsDI createMailContact(String address, ContactDetailsTypeDI type, CoachTourCustomerDI customer, DriverDI driver){
		return new GuiMailContactDetails(address, driver, customer, type);
	}
	
}
