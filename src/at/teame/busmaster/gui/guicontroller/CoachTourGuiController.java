package at.teame.busmaster.gui.guicontroller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.controller.IncompatibleDateException;
import at.teame.busmaster.controller.LoginController;
import at.teame.busmaster.controller.NotUpdateableException;
import at.teame.busmaster.controller.NullArgumentException;
import at.teame.busmaster.controller.coachtour.BookCoachTourController;
import at.teame.busmaster.controller.coachtour.CancelCoachTourController;
import at.teame.busmaster.controller.coachtour.ChangeCoachTourController;
import at.teame.busmaster.controller.coachtour.CreateCoachTourOfferController;
import at.teame.busmaster.controller.coachtour.ReserveCoachTourController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.guidata.coachtour.GuiAdditionalExpense;
import at.teame.busmaster.domain.guidata.coachtour.GuiCoachTour;
import at.teame.busmaster.domain.guidata.coachtour.GuiCoachTourAddress;
import at.teame.busmaster.domain.guidata.coachtour.GuiCoachTourOrder;
import at.teame.busmaster.domain.guidata.coachtour.GuiCoachTourSchedule;
import at.teame.busmaster.gui.ConfigLoading;

/**
 * @author Team E
 * @version 0.3
 */
public class CoachTourGuiController
{

    public static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
    public static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy, HH:mm");

    public static final int NUM_OF_DRIVERS = 0;
    public static final int NUM_OF_BUSSES = 1;
    public static final int START_DATE = 2;
    public static final int END_DATE = 3;
    public static final int START_TIME = 4;
    public static final int END_TIME = 5;
    public static final int SPECIFICATION = 6;

    public static final int DISTANCE = 0;
    public static final int TRAVEL_TIME = 1;
    public static final int STANDING_TIME = 2;

    public static final int STREET = 0;
    public static final int HOUSENUMBER = 1;
    public static final int POSTCODE = 2;
    public static final int CITY = 3;

    public static final int DESCRIPTION = 0;
    public static final int PRICE = 1;

    private static CoachTourGuiController INSTANCE;

    /**
     *
     * @return An instance of CoachTourGuiController
     */
    public static CoachTourGuiController getInstance()
    {
        if (ConfigLoading.getSetting() == ConfigLoading.LET_IT_BE)
        {
            return null;
        }
        if (INSTANCE == null)
        {
            INSTANCE = new CoachTourGuiController();
        }
        return INSTANCE;
    }

    private CoachTourGuiController()
    {
    }

    private CreateCoachTourOfferController _createController = new CreateCoachTourOfferController();
    private CancelCoachTourController _cancleController = new CancelCoachTourController();
    private ReserveCoachTourController _reserveController = new ReserveCoachTourController();
    private BookCoachTourController _bookController = new BookCoachTourController();
    private ChangeCoachTourController _changeController = new ChangeCoachTourController();
    private LoginController _loginController = new LoginController();

    /**
     *
     * @return a List of all coachtour customers existing
     * @throws DBException
     */
    public List<? extends CoachTourCustomerDI> getAllCustomers() throws DBException
    {
        return _createController.getAllCustomers();
    }

    /**
     *
     * @return a List of all modules existing
     * @throws DBException
     */
    public List<? extends ModuleDI> getAllModules() throws DBException
    {
        return _createController.getAllModules();
    }

    /**
     *
     * @return a List of all states existing
     * @throws DBException
     */
    public List<? extends StateDI> getAllStates() throws DBException
    {
        return _createController.getAllStates();
    }

    /**
     *
     * @return a List of all coachtour orders existing
     * @throws DBException
     */
    public List<? extends CoachTourOrderDI> getAllCoachTourOrders() throws DBException
    {
        return _createController.getAllCoachTourOrders();
    }

    /**
     *
     * @param customer coachtour customer who belongs to this order
     * @param numOfPersons number of persons
     * @param specification a description of the order
     * @param tours all tours belonging to this order
     * @param distanceAndTimes all dinstances and times
     * @param addresses all adresses and times
     * @param states all states the order includes
     * @param charges additional charges
     * @param modules wished modules
     * @throws IncompatibleDateException
     * @throws NullArgumentException
     * @throws PeriodException
     * @throws UncompatibleConvertionException
     */
    public void saveOrder(CoachTourCustomerDI customer, String numOfPersons, String specification, List<String[]> tours, List<int[]> distanceAndTimes, List<List<String[]>> addresses, List<List<StateDI>> states, List<String[]> charges, List<List<ModuleDI>> modules) throws IncompatibleDateException, NullArgumentException, PeriodException, UncompatibleConvertionException
    {
        if (customer == null)
        {
            throw new NullArgumentException("Customer null", "validator.NULL");
        }

        GuiCoachTourOrder order;

        try
        {
            order = createGuiOrder(customer, numOfPersons, specification, tours, distanceAndTimes, addresses, states, charges, modules);
        } catch (ParseException pe)
        {
            throw new IncompatibleDateException("Incompatible Date Format", "", pe);
        }
        _createController.createOrder(order);
    }

    /**
     *
     * @param orderDI the order to update
     * @param customer the customer
     * @param numOfPersons number of Persons
     * @param specification a specification of the order
     * @param tours all tours belonging to the order
     * @param distanceAndTimes all distances and times
     * @param addresses all addresses
     * @param states all states belonging to this order
     * @param charges additional charges
     * @param modules Wished modules
     * @throws IncompatibleDateException
     * @throws NotUpdateableException
     * @throws UncompatibleConvertionException
     * @throws PeriodException
     */
    public void updateOrder(CoachTourOrderDI orderDI, CoachTourCustomerDI customer, String numOfPersons, String specification, List<String[]> tours, List<int[]> distanceAndTimes, List<List<String[]>> addresses, List<List<StateDI>> states, List<String[]> charges, List<List<ModuleDI>> modules) throws IncompatibleDateException, NotUpdateableException, UncompatibleConvertionException, PeriodException
    {

        GuiCoachTourOrder order;
        try
        {
            order = createGuiOrder(customer, numOfPersons, specification, tours, distanceAndTimes, addresses, states, charges, modules);
        } catch (ParseException pe)
        {
            throw new IncompatibleDateException("Incompatible Date Format", "", pe);
        }
        _createController.update(orderDI, order);
    }

    /**
     *
     * @param charg addtional charges
     * @param modules wished modules
     * @param tours all tours belonging to this order
     * @param distTimes all distances and times
     * @return the estimated cost of this order
     * @throws PeriodException
     * @throws UncompatibleConvertionException
     */
    public double calculatePrice(List<String[]> charg, List<List<ModuleDI>> modules, List<String[]> tours, List<int[]> distTimes) throws PeriodException, UncompatibleConvertionException
    {
        int drivers = 0;
        int busses = 0;
        // get number of drivers and number of busses
        for (String[] s : tours)
        {
            drivers += Integer.parseInt(s[NUM_OF_DRIVERS]);
            busses += Integer.parseInt(s[NUM_OF_BUSSES]);
        }

        int traveltime = 0;
        int standingtime = 0;
        int distance = 0;
        // get traveltime, standingtime and distance
        for (int[] i : distTimes)
        {
            traveltime += i[TRAVEL_TIME];
            standingtime += i[STANDING_TIME];
            distance += i[DISTANCE];
        }

        //forward to controller and return result
        return _createController.calculatePrice(charg, modules, drivers, traveltime, standingtime, busses, distance);
    }

    /**
     *
     * @param customer the customer
     * @param numOfPersons the number of persons
     * @param specification a specification
     * @param tours all tours belonging to the order
     * @param distanceAndTimes all distances and times
     * @param addresses all adresses
     * @param states all states belonging to the order
     * @param charges additional charges
     * @param modules wished modules
     * @return order object
     * @throws NumberFormatException
     * @throws ParseException
     */
    private GuiCoachTourOrder createGuiOrder(CoachTourCustomerDI customer, String numOfPersons, String specification, List<String[]> tours, List<int[]> distanceAndTimes, List<List<String[]>> addresses, List<List<StateDI>> states, List<String[]> charges, List<List<ModuleDI>> modules) throws NumberFormatException, ParseException
    {
        Iterator<String[]> itTours = tours.iterator();
        Iterator<int[]> itDistTimes = distanceAndTimes.iterator();
        Iterator<List<ModuleDI>> itModules = modules.iterator();
        Iterator<List<String[]>> itAdr = addresses.iterator();
        Iterator<List<StateDI>> itState = states.iterator();
        Iterator<String[]> itItAdr;
        Iterator<StateDI> itItState;

        List<GuiCoachTourSchedule> schedules = new LinkedList<>();
        GuiCoachTourSchedule schedule;
        String[] tour;
        int[] dist;
        String[] adr;
        List<ModuleDI> moduleL;
        List<GuiCoachTourAddress> addressesL;
        List<GuiAdditionalExpense> expenses = new LinkedList<>();
        GuiAdditionalExpense expense;

        //get additional charges
        for (String[] charge : charges)
        {
            expense = new GuiAdditionalExpense(charge[DESCRIPTION], Integer.parseInt(charge[PRICE]));
            expenses.add(expense);
        }

        GuiCoachTourOrder order = new GuiCoachTourOrder(Integer.parseInt(numOfPersons), new Date(), null, null, null, specification, customer, schedules, expenses);

        //get tours, distances, times, modules, addresses and states
        while (itTours.hasNext() && itDistTimes.hasNext() && itModules.hasNext() && itAdr.hasNext() && itState.hasNext())
        {
            tour = itTours.next();
            dist = itDistTimes.next();
            moduleL = itModules.next();
            GuiCoachTourAddress address;
            itItAdr = itAdr.next().iterator();
            itItState = itState.next().iterator();
            addressesL = new LinkedList<>();

            //get addresses and states
            while (itItAdr.hasNext() && itItState.hasNext())
            {
                adr = itItAdr.next();
                address = new GuiCoachTourAddress(adr[STREET], adr[POSTCODE], adr[HOUSENUMBER], adr[CITY], itItState.next());
                addressesL.add(address);
            }

            schedule = new GuiCoachTourSchedule(dist[DISTANCE], dist[TRAVEL_TIME], dist[STANDING_TIME], Integer.parseInt(tour[NUM_OF_DRIVERS]), Integer.parseInt(tour[NUM_OF_BUSSES]), DATE_TIME_FORMAT.parse(tour[START_DATE] + ", " + tour[START_TIME]), DATE_TIME_FORMAT.parse(tour[END_DATE] + ", " + tour[END_TIME]), tour[SPECIFICATION], moduleL, order, addressesL, new LinkedList<CoachTourDI>());
            schedules.add(schedule);
        }
        return order;
    }

    /**
     *
     * @param toCancle the coach tour to cancel
     * @throws UncompatibleConvertionException
     */
    public void cancleCoachTour(CoachTourOrderDI toCancle) throws UncompatibleConvertionException
    {
        _cancleController.cancleCoachTour(toCancle);
    }

    /**
     *
     * @return a list of cancelable orders
     * @throws PeriodException
     */
    public List<? extends CoachTourOrderDI> getCancleableOrders() throws PeriodException
    {
        return _cancleController.getCancelableCoachTourOrders();
    }

    /**
     *
     * @param toReserve the coach tour to reserver
     * @param schedules the schedules
     * @param drivers its drivers
     * @param additionalDrivers its additional drivers
     * @param busses its busses
     * @throws DBException
     * @throws PeriodException
     * @throws UncompatibleConvertionException
     */
    public void reserveCoachTour(CoachTourOrderDI toReserve, List<CoachTourScheduleDI> schedules, List<DriverDI> drivers, List<DriverDI> additionalDrivers, List<BusDI> busses) throws DBException, PeriodException, UncompatibleConvertionException
    {
        List<CoachTourDI> tours = new LinkedList<>();
        GuiCoachTour tour;

        Iterator<CoachTourScheduleDI> itSchedule = schedules.iterator();
        Iterator<DriverDI> itDriver = drivers.iterator();
        Iterator<DriverDI> itAdDriver = additionalDrivers.iterator();
        Iterator<BusDI> itBus = busses.iterator();

        //creats all tours
        while (itSchedule.hasNext() && itDriver.hasNext() && itAdDriver.hasNext() && itBus.hasNext())
        {
            tour = new GuiCoachTour(itDriver.next(), itAdDriver.next(), itBus.next(), itSchedule.next());
            tours.add(tour);
        }

        _reserveController.reserveCoachTourOrder(toReserve, tours);

    }

    /**
     *
     * @param toUpdate the coach tour to update
     * @param schedules its (new) schedules
     * @param drivers its (new) drivers
     * @param additionalDrivers its (new) addidtional drivers
     * @param busses its (new) busses
     * @throws PeriodException
     * @throws UncompatibleConvertionException
     * @throws NotUpdateableException
     */
    public void manageCoachTour(CoachTourOrderDI toUpdate, List<CoachTourScheduleDI> schedules, List<DriverDI> drivers, List<DriverDI> additionalDrivers, List<BusDI> busses) throws PeriodException, UncompatibleConvertionException, NotUpdateableException
    {
        List<CoachTourDI> tours = new LinkedList<>();
        List<CoachTourDI> updatetours = new LinkedList<>(toUpdate.getTours());
        GuiCoachTour tour;

        Iterator<CoachTourScheduleDI> itSchedule = schedules.iterator();
        Iterator<DriverDI> itDriver = drivers.iterator();
        Iterator<DriverDI> itAdDriver = additionalDrivers.iterator();
        Iterator<BusDI> itBus = busses.iterator();

        //creates new tours
        while (itSchedule.hasNext() && itDriver.hasNext() && itAdDriver.hasNext() && itBus.hasNext())
        {
            tour = new GuiCoachTour(itDriver.next(), itAdDriver.next(), itBus.next(), itSchedule.next());
            tours.add(tour);
        }
        Iterator<CoachTourDI> itUpdateTours = updatetours.iterator();
        Iterator<CoachTourDI> itTours = tours.iterator();
        //update each tour
        while (itUpdateTours.hasNext() && itTours.hasNext())
        {
            _reserveController.update(itUpdateTours.next(), itTours.next());
        }
    }

    /**
     *
     * @return a list of reservable orders
     * @throws PeriodException
     */
    public List<? extends CoachTourOrderDI> getReservableOrders() throws PeriodException
    {
        return _reserveController.getReservableCoachTourOrders();
    }

    /**
     *
     * @param toBook the order to book
     * @throws UncompatibleConvertionException
     */
    public void bookCoachTour(CoachTourOrderDI toBook) throws UncompatibleConvertionException
    {
        _bookController.bookCoachTourOrder(toBook);
    }

    /**
     *
     * @return a list of bookable orders
     * @throws PeriodException
     */
    public List<? extends CoachTourOrderDI> getBookableOrders() throws PeriodException
    {
        return _bookController.getBookableCoachTourOrders();
    }

    /**
     *
     * @return a list of changeable orders
     * @throws PeriodException
     */
    public List<? extends CoachTourOrderDI> getChangeableOrders() throws PeriodException
    {
        return _changeController.getChangeableCoachTourOrders();
    }

    /**
     *
     * @return a list of manageable orders
     * @throws PeriodException
     */
    public List<? extends CoachTourOrderDI> getManageableOrders() throws PeriodException
    {
        return _changeController.getManageableCoachTourOrders();
    }

    /**
     *
     * @param order to check if a specific driver is available
     * @return an ordered list of all drivers
     */
    public List<? extends DriverDI> getAllDrivers(CoachTourOrderDI order)
    {
        return _reserveController.getBusdriver(order);
    }

    /**
     *
     * @param order to check if a specific bus is available
     * @return an orderd list of all busses
     */
    public List<? extends BusDI> getAllBusses(CoachTourOrderDI order)
    {
        return _reserveController.getBusses(order);
    }

    /**
     *
     * @return a list of tours that have to be reservered soon
     */
    public List<? extends CoachTourOrderDI> reminder()
    {
        return _createController.reminder();
    }

    /**
     *
     * @param username
     * @param password
     * @return if the login was successfull
     */
    public boolean login(String username, String password)
    {
        return _loginController.login(username, password);
    }

    /**
     *
     * @param toBook the order to book
     * @return if all drivers and busses are available
     * @throws PeriodException
     */
    public boolean isBookable(CoachTourOrderDI toBook) throws PeriodException
    {
        return _bookController.isBookable(toBook);
    }

    public void updateSchedule(CoachTourScheduleDI toUpdate, CoachTourScheduleDI source)
            throws UncompatibleConvertionException, NotUpdateableException, PeriodException
    {
        _changeController.update(toUpdate, source);
    }
}
