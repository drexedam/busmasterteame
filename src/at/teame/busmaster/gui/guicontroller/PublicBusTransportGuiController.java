package at.teame.busmaster.gui.guicontroller;

import java.util.Date;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.controller.NotUpdateableException;
import at.teame.busmaster.controller.pbtParts.PublicBusTransportController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.publicbustransport.PublicBusTransportDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteDI;
import at.teame.busmaster.domain.domaininterface.schedule.DayTypeDI;
import at.teame.busmaster.domain.guidata.publicbustransport.GuiPublicBusTransport;

public class PublicBusTransportGuiController {

	private PublicBusTransportController _controller = new PublicBusTransportController();
	
	/**
	 * 
	 * @return the list of all public bus transports
	 */
	public List<? extends PublicBusTransportDI> getAllPublicBusTransports(){
		return _controller.getAllPublicBusTransports();
	}
	/**
	 * 
	 * @param start time of start
	 * @param dayType day type of the public bus transport
	 * @param route the route of the public bus transport
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createPublicBusTransport(Date start, DayTypeDI dayType, RouteDI route ) throws DBException, PeriodException, UncompatibleConvertionException{
		GuiPublicBusTransport transport = new GuiPublicBusTransport(start, dayType, route);
		_controller.createPublicBusTransport(transport);
	}
	/**
	 * 
	 * @param toUpdate the public bus transport to update
	 * @param start time of start
	 * @param dayType day type of the public bus transport
	 * @param route the route of the public bus transport
	 * @throws UncompatibleConvertionException
	 * @throws NotUpdateableException
	 * @throws PeriodException
	 */
	public void updatePublicBusTransport(PublicBusTransportDI toUpdate,Date start, DayTypeDI dayType, RouteDI route) throws UncompatibleConvertionException, NotUpdateableException, PeriodException{
		GuiPublicBusTransport transport = new GuiPublicBusTransport(start, dayType, route);
		_controller.update(toUpdate, transport);
	}
	/**
	 * 
	 * @param transport the public bus transport to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removePublicBusTransport(PublicBusTransportDI transport) throws PeriodException, UncompatibleConvertionException{	
		_controller.removePublicBusTransport(transport);
	}
}
