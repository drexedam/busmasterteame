package at.teame.busmaster.gui.guicontroller;

import java.util.LinkedList;
import java.util.List;

import at.gruppe2.domain.bus.IBus;
import at.gruppe2.domain.publicbustransport.IPublicBusTransport;
import at.gruppe2.domain.schedule.IDayType;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.tb2.CircleDefinitionController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.guidata.tb2.CircleDefinitionDummy;
import at.teame.busmaster.gui.ConfigLoading;
import at.teame.busmaster.gui.guicontroller.wrapper.BusWrapper;
import at.teame.busmaster.gui.guicontroller.wrapper.DayTypeWrapper;
import at.teame.busmaster.gui.guicontroller.wrapper.PublicBusTransportWrapper;

public class CircleDefinitonGuiController {
	private static CircleDefinitonGuiController INSTANCE;
    
    /**
     * 
     * @return An instance of CoachTourGuiController
     */
    public static CircleDefinitonGuiController getInstance()
    {
        if(ConfigLoading.getSetting() == ConfigLoading.LET_IT_BE)
        {
            return null;
        }
        if(INSTANCE == null)
        {
            INSTANCE = new CircleDefinitonGuiController();
        }
        return INSTANCE;
    }
    private CircleDefinitonGuiController()
    {
        
    }
    
	private CircleDefinitionController _controller = new CircleDefinitionController();

	public List<BusWrapper> getAllBusses(){
		List<BusWrapper> busses = new LinkedList<>();
		for(IBus b : _controller.getAllBusses()) {
			busses.add(new BusWrapper(b));
		}
		
		return busses;
	}
	
	public List<DayTypeWrapper> getAllDayTypes(){
		List<DayTypeWrapper> dayTypes = new LinkedList<>();
		for(IDayType d : _controller.getAllDayTypes()) {
			dayTypes.add(new DayTypeWrapper(d));
		}
		
		return dayTypes;
	}
	
	public List<PublicBusTransportWrapper> getPublicBusTransports(DayTypeWrapper dayType){
		List<PublicBusTransportWrapper> pbts = new LinkedList<>();
		for(IPublicBusTransport pbt :  _controller.getPublicBusTransports(dayType.getDayType())) {
			pbts.add(new PublicBusTransportWrapper(pbt));
		}
		
		return pbts;
	}
	
	public void saveCircleDefinition(BusWrapper bus, DayTypeWrapper dayType, List<PublicBusTransportWrapper> publicTransports, String specification) throws PeriodException, UncompatibleConvertionException{
		CircleDefinitionDummy dummy = new CircleDefinitionDummy();
		dummy.setBus(bus.getBus());
		dummy.setDayType(dayType.getDayType());
		List<IPublicBusTransport> ipb = new LinkedList<>();
		for(PublicBusTransportWrapper pbtw : publicTransports) {
			ipb.add(pbtw.getPublicBusTransport());
		}
		dummy.setPublicTransports(ipb);
		dummy.setSpecification(specification);
		_controller.saveCircleDefinition(dummy);
	}
}
