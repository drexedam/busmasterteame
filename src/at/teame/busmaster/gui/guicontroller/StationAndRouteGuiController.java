package at.teame.busmaster.gui.guicontroller;

import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.controller.NotUpdateableException;
import at.teame.busmaster.controller.pbtParts.StationAndRouteController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteSectionDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.StationDI;
import at.teame.busmaster.domain.guidata.publicbustransport.GuiRoute;
import at.teame.busmaster.domain.guidata.publicbustransport.GuiRouteSection;
import at.teame.busmaster.domain.guidata.publicbustransport.GuiStation;

public class StationAndRouteGuiController {

	private StationAndRouteController _controller = new StationAndRouteController();
	/**
	 * 
	 * @return the list of all stations
	 */
	public List<? extends StationDI> getAllStations(){
		return _controller.getAllStations();
	}
	/**
	 * 
	 * @return the list of all routes
	 */
	public List<? extends RouteDI> getAllRoutes(){
		return _controller.getAllRoutes();
	}
	/**
	 * 
	 * @return the list of all route sections
	 */
	public List<? extends RouteSectionDI> getAllRouteSections(){
		return _controller.getAllRouteSections();
	}
	/**
	 * 
	 * @param number the number of the station
	 * @param name the name of the station
	 * @param description the description of the station
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createStation(String number, String name, String description) throws DBException, PeriodException, UncompatibleConvertionException{
		GuiStation station = new GuiStation(number, name, description);
		_controller.createStation(station);
	}
	/**
	 * 
	 * @param station the station to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeStation(StationDI station) throws PeriodException, UncompatibleConvertionException{
		_controller.removeStation(station);
	}
	/**
	 * 
	 * @param number the number of the route
	 * @param specification the specification of the route
	 * @param sections list of all route section of the route
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createRoute(String number, String specification, List<RouteSectionDI> sections) throws DBException, PeriodException, UncompatibleConvertionException{
		GuiRoute route = new GuiRoute(number, specification, sections);
		_controller.createRoute(route);
	}
	/**
	 * 
	 * @param toUpdate the route to update
	 * @param number the number of the route
	 * @param specification the specification of the route
	 * @param sections list of all route section of the route
	 * @throws UncompatibleConvertionException
	 * @throws NotUpdateableException
	 * @throws PeriodException
	 */
	public void updateRoute(RouteDI toUpdate, String number, String specification, List<RouteSectionDI> sections) throws UncompatibleConvertionException, NotUpdateableException, PeriodException{
		GuiRoute route = new GuiRoute(number, specification, sections);
		_controller.update(toUpdate, route);
	}
	/**
	 * 
	 * @param route the route to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeRoute(RouteDI route) throws PeriodException, UncompatibleConvertionException{
		_controller.removeRoute(route);
	}
	/**
	 * 
	 * @param distance the distance of the route section
	 * @param drivingtime the driving time of the route section
	 * @param start the start station
	 * @param end the end station
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createRouteSection(int distance, int drivingtime, StationDI start, StationDI end) throws DBException, PeriodException, UncompatibleConvertionException{
		GuiRouteSection section = new GuiRouteSection(distance, drivingtime, start, end);
		_controller.createRouteSection(section);
	}
	/**
	 * 
	 * @param section the route section to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeRouteSection(RouteSectionDI section) throws PeriodException, UncompatibleConvertionException{
		_controller.removeRouteSection(section);
	}
}
