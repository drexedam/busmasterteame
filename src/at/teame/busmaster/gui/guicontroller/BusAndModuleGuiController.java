package at.teame.busmaster.gui.guicontroller;

import java.util.Date;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.controller.NotUpdateableException;
import at.teame.busmaster.controller.coredata.BusAndModuleController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.bus.BusTypeDI;
import at.teame.busmaster.domain.domaininterface.bus.BusUnavailabilityDI;
import at.teame.busmaster.domain.domaininterface.bus.BusUnavailabilityTypeDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleTypeDI;
import at.teame.busmaster.domain.guidata.bus.GuiBus;
import at.teame.busmaster.domain.guidata.bus.GuiBusType;
import at.teame.busmaster.domain.guidata.bus.GuiBusUnavailability;
import at.teame.busmaster.domain.guidata.bus.GuiBusUnavailabilityType;
import at.teame.busmaster.domain.guidata.bus.GuiBusWrapper;
import at.teame.busmaster.domain.guidata.module.GuiModule;
import at.teame.busmaster.domain.guidata.module.GuiModuleType;
import at.teame.busmaster.domain.guidata.module.GuiModuleTypeWrapper;
import at.teame.busmaster.gui.ConfigLoading;
import at.teame.busmaster.gui.guicontroller.wrapper.BusWrapper;
import java.util.LinkedList;

public class BusAndModuleGuiController {
	
	private BusAndModuleController _controller = new BusAndModuleController();
    
    private static BusAndModuleGuiController _instance;
    public  static BusAndModuleGuiController getInstance()
    {
        if(_instance == null && ConfigLoading.getSetting() == ConfigLoading.DO_LOADING)
        {
            _instance = new BusAndModuleGuiController();
        }
        return _instance;
    }
    
    private BusAndModuleGuiController()
    {
        
    }
    
	/**
	 * 
	 * @return the list of all modules
	 */
	public List<? extends ModuleDI> getAllModules(){
		return _controller.getAllModules();
	}
	/**
	 * 
	 * @return the list of all modules which are not part of an Bus
	 */
	public List<? extends ModuleDI> getNotUsedModules(){
		return _controller.getAllModulesNotInBus();
	}
	/**
	 * 
	 * @return the list of all buses
	 */
	public List<? extends BusDI> getAllBuses(){
		return _controller.getAllBuses();
	}
	/**
	 * 
	 * @return the list of all buses as wrapped version
	 */
	public List<? extends GuiBusWrapper> getAllBusesWrapped(){
        List<GuiBusWrapper> ret = new LinkedList<>();
        
        for(BusDI b : getAllBuses())
        {
            ret.add(new GuiBusWrapper(b));
        }
        
		return ret;
	}
	/**
	 * 
	 * @return the list of all bus types
	 */
	public List<? extends BusTypeDI> getAllBusTypes(){
		return _controller.getAllBusTypes();
	}
	/**
	 * 
	 * @return the list of all module types
	 */
	public List<? extends ModuleTypeDI> getAllModuleTypes(){
		return _controller.getAllModuleTypes();
	}
	/**
	 * 
	 * @return the list of all module types as wrapped version
	 */
	public List<? extends GuiModuleTypeWrapper> getAllModuleTypesWrapped(){
		List<GuiModuleTypeWrapper> ret = new LinkedList<>();
        
        for(ModuleTypeDI b : getAllModuleTypes())
        {
            ret.add(new GuiModuleTypeWrapper(b));
        }
        
		return ret;
	}
	/**
	 * 
	 * @param bus for which the unavailabilities will return
	 * @return the list of all bus unavailabilities
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public List<? extends BusUnavailabilityDI> getAllBusUnavailabilities(BusDI bus) throws DBException, PeriodException, UncompatibleConvertionException{
		return _controller.getAllBusUnavaiabilities(bus);
	}
	/**
	 * 
	 * @return the list of all bus unavaiability types
	 */
	public List<? extends BusUnavailabilityTypeDI> getAllBusUnavailabilityTypes(){
		return _controller.getAllBusUnavailabilityTypes();
	}
	/**
	 * 
	 * @param numberPlate the number plate of the bus
	 * @param type the bus type
	 * @param modules belonging to the bus
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createBus(String numberPlate, BusTypeDI type, List<ModuleDI> modules) throws PeriodException, UncompatibleConvertionException{
		GuiBus bus = new GuiBus(numberPlate, type, modules);
		_controller.createBus(bus);
	}
	/**
	 * 
	 * @param numberPlate the number plate of the bus
	 * @param type the bus type
	 * @param modules belonging to the bus
	 * @param toUpdate the bus to update
	 * @throws UncompatibleConvertionException
	 * @throws NotUpdateableException
	 * @throws PeriodException
	 */
	public void updateBus(String numberPlate, BusTypeDI type, List<ModuleDI> modules,BusDI toUpdate) throws UncompatibleConvertionException, NotUpdateableException, PeriodException{
		GuiBus bus = new GuiBus(numberPlate, type, modules);
		_controller.update(toUpdate, bus);
		
	}
	/**
	 * 
	 * @param bus the bus to be deleted
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeBus(BusDI bus) throws PeriodException, UncompatibleConvertionException{
		_controller.removeBus(bus);
	}
	/**
	 * 
	 * @param bus which belongs to the module
	 * @param type the module type of the module
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createModule(BusDI bus, ModuleTypeDI type) throws PeriodException, UncompatibleConvertionException{
		GuiModule module = new GuiModule(bus, type);
		_controller.createModule(module);
	}
	/**
	 * 
	 * @param bus which belongs to the module
	 * @param type the module type of the module
	 * @param toUpdate the module to update
	 * @throws UncompatibleConvertionException
	 * @throws NotUpdateableException
	 * @throws PeriodException
	 */
	public void updateModule(BusDI bus, ModuleTypeDI type, ModuleDI toUpdate) throws UncompatibleConvertionException, NotUpdateableException, PeriodException{
		GuiModule module = new GuiModule(bus, type);
		_controller.update(toUpdate, module);
	}
	/**
	 * 
	 * @param module the module to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeModule(ModuleDI module) throws PeriodException, UncompatibleConvertionException{
		_controller.removeModule(module);
	}
	/**
	 * 
	 * @param standingPlaces the number of standing places
	 * @param seats the number of seats
	 * @param type description of the type
	 * @param basePrice the base price of the bus type
	 * @param kmPrice the kilometer price of the bus type
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createBusType(int standingPlaces, int seats, String type, int basePrice, int kmPrice) throws DBException, PeriodException, UncompatibleConvertionException{
		GuiBusType busType = new GuiBusType(standingPlaces, seats, type, basePrice, kmPrice);
		_controller.createBusType(busType);
	}
	/**
	 * 
	 * @param busType the bus type to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeBusType(BusTypeDI busType) throws PeriodException, UncompatibleConvertionException{
		_controller.removeBusType(busType);
	}
	/**
	 * 
	 * @param specification the  specification of the module type
	 * @param requiredSpace the required space of the module type
	 * @param price	the price of the module type
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 * @throws DBException
	 */
	public void createModuleType(String specification, int requiredSpace, int price) throws PeriodException, UncompatibleConvertionException, DBException{
		GuiModuleType type = new GuiModuleType(specification, requiredSpace, price);
		_controller.createModuleType(type);
	}
	/**
	 * 
	 * @param moduleType the module type to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeModuleType(ModuleTypeDI moduleType) throws PeriodException, UncompatibleConvertionException{
		_controller.removeModuleType(moduleType);
	}
	/**
	 * 
	 * @param start the start date of the unavailability
	 * @param end the end date of the unavailability
	 * @param bus the bus which is unavailable
	 * @param type the bus unavailability type
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createBusUnavailability(Date start, Date end, BusDI bus,BusUnavailabilityTypeDI type) throws DBException, PeriodException, UncompatibleConvertionException{
		GuiBusUnavailability unavailability = new GuiBusUnavailability(start, end, bus, type);
		_controller.createBusUnavailability(unavailability);
	}
	/**
	 * 
	 * @param unavailability the bus unavailability to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeBusUnavailability(BusUnavailabilityDI unavailability) throws PeriodException, UncompatibleConvertionException{
		_controller.removeBusUnavailability(unavailability);
	}
	/**
	 * 
	 * @param specification the specification of the bus unavailability type
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createBusUnavailabilityType(String specification) throws DBException, PeriodException, UncompatibleConvertionException{
		GuiBusUnavailabilityType unavailabilityType= new GuiBusUnavailabilityType(specification);
		_controller.createBusUnavailabilityType(unavailabilityType);
	}
	/**
	 * 
	 * @param unavailabilityType the bus unavailability type to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeBusUnavailabilityType(BusUnavailabilityTypeDI unavailabilityType) throws PeriodException, UncompatibleConvertionException{
		_controller.removeBusUnavailabilityType(unavailabilityType);
	}
}
