package at.teame.busmaster.gui.guicontroller;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.controller.NotUpdateableException;
import at.teame.busmaster.controller.coredata.DriverController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.ContactDetailsTypeDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.MailContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.TelContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverTypeDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverUnavailabilityDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverUnavailabilityTypeDI;
import at.teame.busmaster.domain.guidata.driver.GuiDriver;
import at.teame.busmaster.domain.guidata.driver.GuiDriverType;
import at.teame.busmaster.domain.guidata.driver.GuiDriverUnavailability;
import at.teame.busmaster.domain.guidata.driver.GuiDriverUnavailabilityType;

public class DriverGuiController extends ContactDetailGuiController{
	private DriverController _controller = new DriverController();
	
	/**
	 * 
	 * @return the list of all drivers
	 */
	public List<? extends DriverDI> getAllDrivers(){
		return _controller.getAllDrivers();
	}
	/**
	 * 
	 * @return the list of all driver types
	 */
	public List<? extends DriverTypeDI> getAllDriverTypes(){
		return _controller.getAllDriverTypes();
	}
	
	/**
	 * 
	 * @param driver for which the unavailabilities will return
	 * @return the list of all driver unavailabilities
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public List<? extends DriverUnavailabilityDI> getAllDriverUnavailabilities(DriverDI driver) throws DBException, PeriodException, UncompatibleConvertionException{
		return _controller.getAllDriverUnavailabilities(driver);
	}
	/**
	 * 
	 * @return list of all driver unavailability types
	 */
	public List<? extends DriverUnavailabilityTypeDI> getAllDriverUnavailabilityTypes(){
		return _controller.getAllDriverUnavailabilityTypes();
	}
	/**
	 * 
	 * @param firstName of the driver
	 * @param lastName of the driver
	 * @param street of the address
	 * @param houseNumber of the address
	 * @param zip of the address
	 * @param city of the address
	 * @param state of the address
	 * @param driverType driver type of the driver
	 * @param numbers list with phone numbers which belongs to the driver
	 * @param phoneTypes list with the contact types to the phone numbers
	 * @param addresses list with mail addresses which belongs to the driver
	 * @param mailTypes list with the contact types to the mail addresses
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createDriver(String firstName, String lastName, String street, String houseNumber, String zip, String city, StateDI state, DriverTypeDI driverType, List<String> numbers, List<ContactDetailsTypeDI> phoneTypes, List<String> addresses, List<ContactDetailsTypeDI> mailTypes) throws DBException, PeriodException, UncompatibleConvertionException{
		
		 List<TelContactDetailsDI> phones = new LinkedList<>();
		 List<MailContactDetailsDI> mails = new LinkedList<>();
		 GuiDriver driver = new GuiDriver(firstName, lastName, street, houseNumber, zip, city, state, driverType, phones, mails);
		 
		 Iterator<String> phone = numbers.iterator();
		 Iterator<ContactDetailsTypeDI> phoneType = phoneTypes.iterator();
		 Iterator<String> mail = addresses.iterator();
		 Iterator<ContactDetailsTypeDI> mailType = mailTypes.iterator();
		 while(phone.hasNext()){
			 phones.add(createPhoneContact(phone.next(), phoneType.next(), null, driver));
		 }
		 while(mail.hasNext()){
			 mails.add(createMailContact(mail.next(), mailType.next(), null, driver));
		 }
		
		_controller.createDriver(driver);
	}
	/**
	 * 
	 * @param toUpdate driver to update
	 * @param firstName of the driver
	 * @param lastName of the driver
	 * @param street of the address
	 * @param houseNumber of the address
	 * @param zip of the address
	 * @param city of the address
	 * @param state of the address
	 * @param driverType driver type of the driver
	 * @param numbers list with phone numbers which belongs to the driver
	 * @param phoneTypes list with the contact types to the phone numbers
	 * @param addresses list with mail addresses which belongs to the driver
	 * @param mailTypes list with the contact types to the mail addresses
	 * @throws UncompatibleConvertionException
	 * @throws NotUpdateableException
	 * @throws PeriodException
	 */
	public void updateDriver(DriverDI toUpdate, String firstName, String lastName, String street, String houseNumber, String zip, String city, StateDI state, DriverTypeDI driverType, List<String> numbers, List<ContactDetailsTypeDI> phoneTypes, List<String> addresses, List<ContactDetailsTypeDI> mailTypes) throws UncompatibleConvertionException, NotUpdateableException, PeriodException{
		 List<TelContactDetailsDI> phones = new LinkedList<>();
		 List<MailContactDetailsDI> mails = new LinkedList<>();
		 GuiDriver driver = new GuiDriver(firstName, lastName, street, houseNumber, zip, city, state, driverType, phones, mails);
		 
		 Iterator<String> phone = numbers.iterator();
		 Iterator<ContactDetailsTypeDI> phoneType = phoneTypes.iterator();
		 Iterator<String> mail = addresses.iterator();
		 Iterator<ContactDetailsTypeDI> mailType = mailTypes.iterator();
		 while(phone.hasNext()){
			 phones.add(createPhoneContact(phone.next(), phoneType.next(), null, driver));
		 }
		 while(mail.hasNext()){
			 mails.add(createMailContact(mail.next(), mailType.next(), null, driver));
		 }
		
		_controller.update(toUpdate, driver);
	}
	/**
	 * 
	 * @param driver the driver to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeDriver(DriverDI driver) throws PeriodException, UncompatibleConvertionException{
		_controller.removeDriver(driver);
	}
	/**
	 * 
	 * @param fare price of the driver type
	 * @param standingPrice of the driver type
	 * @param specification of the driver type
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createDriverType(int fare, int standingPrice, String specification) throws DBException, PeriodException, UncompatibleConvertionException{
		GuiDriverType type = new GuiDriverType(fare, standingPrice, specification);
		_controller.createDriverType(type);
	}
	/**
	 * 
	 * @param type the driver type to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeDriverType(DriverTypeDI type) throws PeriodException, UncompatibleConvertionException{
		_controller.removeDriverType(type);
	}
	/**
	 * 
	 * @param start date of the driver unavailability
	 * @param end date of the driver unavailability
	 * @param type unavailability type of the driver unavailability
	 * @param driver the driver which is unavailable
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createDriverUnavailability(Date start, Date end, DriverUnavailabilityTypeDI type, DriverDI driver) throws DBException, PeriodException, UncompatibleConvertionException{
		GuiDriverUnavailability unavailability = new GuiDriverUnavailability(start, end, type, driver);
		_controller.createDriverUnavailability(unavailability);
	}
	/**
	 * 
	 * @param unavailability the driver unavailability to delete
	 * @throws UncompatibleConvertionException 
	 * @throws PeriodException 
	 */
	public void removeDriverUnavailability(DriverUnavailabilityDI unavailability) throws PeriodException, UncompatibleConvertionException{
		_controller.removeDriverUnavailability(unavailability);
	}
	/**
	 * 
	 * @param unavailabilityType the driver unavailability type to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createDriverUnavailabilityType(String specification) throws DBException, PeriodException, UncompatibleConvertionException{
		GuiDriverUnavailabilityType type = new GuiDriverUnavailabilityType(specification);
		_controller.createDriverUnavailabilityType(type);
	}
	/**
	 * 
	 * @param unavailabilityType the driver unavailability type to delete
	 * @throws UncompatibleConvertionException 
	 * @throws PeriodException 
	 */
	public void removeDriverUnavailabilityType(DriverUnavailabilityTypeDI unavailabilityType) throws PeriodException, UncompatibleConvertionException{
		_controller.removeDriverUnavailabilityType(unavailabilityType);
	}
}
