/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.gui.coachtour;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.bundles.Msg;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.controller.NotUpdateableException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.guidata.bus.GuiBusWrapper;
import at.teame.busmaster.domain.guidata.driver.GuiDriverWrapper;
import at.teame.busmaster.gui.guicontroller.CoachTourGuiController;

/**
 *
 * @author Morent Jochen <jochen.morent@students.fhv.at>
 */
public class CoachTourManagePanel extends javax.swing.JPanel
{
    private CoachTourGuiController _CTGC = CoachTourGuiController.getInstance();
    private List<CoachTourOrderDI> _reserveableOrders;
    private boolean _changedComboBox = true;
    private CoachTourPanel _parent;
    private boolean        _init;

    /**
     * Creates new form CoachTourManage
     */
    public CoachTourManagePanel()
    {
        initComponents();
    }

    @Override
    public void setVisible(boolean aFlag)
    {
        if(aFlag)
        {
            if(_CTGC != null)
            {
                try
                {
                    _reserveableOrders = (List<CoachTourOrderDI>) _CTGC.getManageableOrders();
                } catch (PeriodException ex)
                {
                    _parent.setErrorMsg(ex.getBundleMessage());
                }
            }
            initGUI();
        }
        super.setVisible(aFlag);
    }
    
    public void initGUI()
    {
        _init = true;
        if(_CTGC != null && _coachTourOrders != null)
        {
            _fillingOrders = true;
            _coachTourOrders.removeAllItems();
            
            _coachTourOrders.addItem(Msg.getString("manageoffer.coachtour.selectorder"));
            
            if(_coachTourOrders != null && !_reserveableOrders.isEmpty())
            {
                for(CoachTourOrderDI cto : _reserveableOrders)
                {
                    CoachTourCustomerDI ctc = cto.getCustomer();
                    //noinspection unchecked
                    _coachTourOrders.addItem(cto.getSpecification() + "; " +
                                             ctc.getLastName() + " " +
                                             ctc.getFirstName());
                }
            }
            _fillingOrders   = false;
            _changedComboBox = false;
        }
        updateGUI();
        _init = false;
    }

    private boolean _fillingOrders = false;
    public void updateGUI()
    {
        if(_fillingOrders)
        {
            return;
        }
        //ComboBox leeren und neu befüllen
        if(_coachTourOrders != null && _changedComboBox)
        {
            _fillingOrders = true;
            _coachTourOrders.removeAllItems();
            
            _coachTourOrders.addItem(Msg.getString("manageoffer.coachtour.selectorder"));
            
            if(_reserveableOrders != null && !_reserveableOrders.isEmpty())
            {
                for(CoachTourOrderDI cto : _reserveableOrders)
                {
                    CoachTourCustomerDI ctc = cto.getCustomer();
                    //noinspection unchecked
                    _coachTourOrders.addItem(cto.getSpecification() + "; " +
                                             ctc.getLastName() + " " +
                                             ctc.getFirstName());
                }
            }
            
            _fillingOrders   = false;
            _changedComboBox = false;
        }
        //Tabelle leeren und neu befüllen
        if(_coachTourTable != null)
        {
            DefaultTableModel tm = (DefaultTableModel) _coachTourTable.getModel();
            
            //clear Table
            for(int i = tm.getRowCount()-1; i >= 0; i--)
            {
                tm.removeRow(i);
            }
            
            if(_reserveableOrders != null && !_reserveableOrders.isEmpty() &&
               _coachTourOrders != null)
            {
                int index = _coachTourOrders.getSelectedIndex();
                if(index > 0)
                {
                    //minus 1 because of the "pleas-select"
                    CoachTourOrderDI cto = _reserveableOrders.get(index - 1);
                    List<CoachTourScheduleDI> lcts = (List<CoachTourScheduleDI>) cto.getSchedules();
                    
                    //Data loading
                    List<DriverDI> drivers    = (List<DriverDI>) _CTGC.getAllDrivers(cto);
                    List<DriverDI> addDrivers = new LinkedList<>();
                    addDrivers.add(null);
                    addDrivers.addAll(drivers);
                    List<BusDI>    buses      = (List<BusDI>)    _CTGC.getAllBusses (cto);

                    //Wrappers
                    List<GuiDriverWrapper> driversW    = new LinkedList<>();
                    List<GuiDriverWrapper> addDriversW = new LinkedList<>();
                    List<GuiBusWrapper>    busesW      = new LinkedList<>();

                    for(DriverDI d : drivers)
                    {
                        driversW.add(new GuiDriverWrapper(d, cto));
                    }
                    for(DriverDI d : addDrivers)
                    {
                        addDriversW.add(new GuiDriverWrapper(d, cto));
                    }
                    for(BusDI b : buses)
                    {
                        busesW.add(new GuiBusWrapper(b, cto));
                    }

                    JComboBox<GuiDriverWrapper> driver    = new JComboBox<>();
                    JComboBox<GuiDriverWrapper> addDriver = new JComboBox<>();
                    JComboBox<GuiBusWrapper>    bus       = new JComboBox<>();


                    for(GuiDriverWrapper d : driversW)
                    {
                        driver.addItem(d);
                    }
                    for(GuiDriverWrapper d : addDriversW)
                    {
                        addDriver.addItem(d);
                    }
                    for(GuiBusWrapper b : busesW)
                    {
                        bus.addItem(b);
                    }

                    TableColumnModel tcm = _coachTourTable.getColumnModel();
                    tcm.getColumn(1).setCellEditor(new DefaultCellEditor(driver));
                    tcm.getColumn(2).setCellEditor(new DefaultCellEditor(addDriver));
                    tcm.getColumn(3).setCellEditor(new DefaultCellEditor(bus));
                    
                    if(!lcts.isEmpty())
                    {
                        for(CoachTourScheduleDI cts : lcts)
                        {
                            for(int i = 0; i < cts.getNumberOfBusses(); i++)
                            {
                                Object[] row = {cts.getSpecification(),
                                                driversW   .get(0),
                                                addDriversW.get(0),
                                                busesW     .get(0)};

                                tm.addRow(row);
                            }
                        }
                    }
                }
            }
        }
        
        repaint();
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        _contentPanel = new javax.swing.JPanel();
        _saveButton = new javax.swing.JButton();
        _coachTourOrders = new javax.swing.JComboBox();
        _coachTourOrdersLabel = new javax.swing.JLabel();
        _coachTourLabel = new javax.swing.JLabel();
        _coachTourPane = new javax.swing.JScrollPane();
        _coachTourTable = new javax.swing.JTable();
        _resetButton = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        _titleLabel = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        _contentPanel.setLayout(new java.awt.GridBagLayout());

        _saveButton.setText(Msg.getString("manage.coachtour.save"));
        _saveButton.setEnabled(false);
        _saveButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                saveAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        _contentPanel.add(_saveButton, gridBagConstraints);

        _coachTourOrders.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                coachTourOrdersAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        _contentPanel.add(_coachTourOrders, gridBagConstraints);

        _coachTourOrdersLabel.setText(Msg.getString("admin.coachtour.orders"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        _contentPanel.add(_coachTourOrdersLabel, gridBagConstraints);

        _coachTourLabel.setText(Msg.getString("admin.coachtour.tours"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        _contentPanel.add(_coachTourLabel, gridBagConstraints);

        _coachTourTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                Msg.getString("offer.coachtour.schedule"),
                Msg.getString("reserve.coachtour.driver"),
                Msg.getString("reserve.coachtour.additionaldriver"),
                Msg.getString("reserve.coachtour.bus")
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true, true, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        _coachTourPane.setViewportView(_coachTourTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        _contentPanel.add(_coachTourPane, gridBagConstraints);

        _resetButton.setText(Msg.getString("offer.coachtour.reset"));
        _resetButton.setEnabled(false);
        _resetButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                resetAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        _contentPanel.add(_resetButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        _contentPanel.add(filler1, gridBagConstraints);

        add(_contentPanel, java.awt.BorderLayout.CENTER);

        _titleLabel.setFont(new java.awt.Font("sansserif", 0, 18)); // NOI18N
        _titleLabel.setText(Msg.getString("manage.coachtour.title"));
        add(_titleLabel, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void saveAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_saveAction
    {//GEN-HEADEREND:event_saveAction
        DefaultTableModel tm = (DefaultTableModel) _coachTourTable.getModel();
        
        int index = _coachTourOrders.getSelectedIndex();
        //minus 1 because of the "pleas-select"
        index = index - 1;
        if(index >= 0)
        {
            //Daten Auslesen und zusammenfassen für den Controller
            CoachTourOrderDI cto = _reserveableOrders.get(index);
            @SuppressWarnings("unchecked") List<CoachTourScheduleDI> lcts = (List<CoachTourScheduleDI>) cto.getSchedules();
            
            List<CoachTourScheduleDI> trips = new LinkedList<>();
            for(CoachTourScheduleDI cts : lcts)
            {
                for(int i = 0; i < cts.getNumberOfBusses(); i++)
                {
                    trips.add(cts);
                }
            }
            
            List<DriverDI> drivers    = new LinkedList<>();
            List<DriverDI> addDrivers = new LinkedList<>();
            List<BusDI>    buses      = new LinkedList<>();
            
            for(int i = 0; i < tm.getRowCount(); i++)
            {
                try {
                    drivers.add(((GuiDriverWrapper) tm.getValueAt(i, 1)).getDriver());
                } catch (ClassCastException c) {
                    drivers.add(((DriverDI) tm.getValueAt(i, 1)));
                }
                if (tm.getValueAt(i, 2) != null) {
                    try {
                        addDrivers.add(((GuiDriverWrapper) tm.getValueAt(i, 2)).getDriver());
                    } catch (ClassCastException c) {
                        addDrivers.add(((DriverDI) tm.getValueAt(i, 2)));
                    }
                } else {
                    addDrivers.add(null);
                }
                try {
                    buses.add(((GuiBusWrapper) tm.getValueAt(i, 3)).getBus());
                } catch (ClassCastException c) {
                    buses.add(((BusDI) tm.getValueAt(i, 3)));
                }
            }

            try {
                _CTGC.manageCoachTour(cto, trips, drivers, addDrivers, buses);
                _parent.enableNavi();
            } catch (PeriodException ex)
            {
                _parent.setErrorMsg(ex.getBundleMessage());
                Logger.getLogger(CoachTourManagePanel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UncompatibleConvertionException uce)
            {
                uce.printStackTrace();
                _parent.setErrorMsg(uce.getBundleMessage());
            } catch (NotUpdateableException nue) {
                _parent.setErrorMsg(nue.getBundleMessage());
            }
        }
    }//GEN-LAST:event_saveAction

    private void coachTourOrdersAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_coachTourOrdersAction
    {//GEN-HEADEREND:event_coachTourOrdersAction
        if(!_init)
        {
            validate();
            updateGUI();
            if(_coachTourOrders.getSelectedIndex() > 0)
            {
                disableNavi();
            }
            else
            {
                enableNavi();
            }
        }
    }//GEN-LAST:event_coachTourOrdersAction

    private void resetAction(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetAction
        enableNavi();
    }//GEN-LAST:event_resetAction


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel _coachTourLabel;
    private javax.swing.JComboBox _coachTourOrders;
    private javax.swing.JLabel _coachTourOrdersLabel;
    private javax.swing.JScrollPane _coachTourPane;
    private javax.swing.JTable _coachTourTable;
    private javax.swing.JPanel _contentPanel;
    private javax.swing.JButton _resetButton;
    private javax.swing.JButton _saveButton;
    private javax.swing.JLabel _titleLabel;
    private javax.swing.Box.Filler filler1;
    // End of variables declaration//GEN-END:variables

    public void setParent(CoachTourPanel p)
    {
        _parent = p;
    }
    private void enableNavi()
    {
        if (_parent != null)
        {
            _parent.enableNavi();
            enableComponents(false);
        }
    }
    private void disableNavi()
    {
        if (_parent != null && !_init)
        {
            _parent.disableNavi();
            enableComponents(true);
        }
    }

    private void enableComponents(boolean b)
    {
        _resetButton.setEnabled(b);
        _saveButton.setEnabled(b);
    }
}
