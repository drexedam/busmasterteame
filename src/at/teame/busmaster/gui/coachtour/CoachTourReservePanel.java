/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.gui.coachtour;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.bundles.Msg;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.guidata.bus.GuiBusWrapper;
import at.teame.busmaster.domain.guidata.driver.GuiDriverWrapper;
import at.teame.busmaster.gui.ConfigLoading;
import at.teame.busmaster.gui.guicontroller.CoachTourGuiController;

/**
 *
 * @author Morent Jochen <jochen.morent@students.fhv.at>
 */
public class CoachTourReservePanel extends javax.swing.JPanel
{
    private CoachTourGuiController _CTGC = CoachTourGuiController.getInstance();
    private List<CoachTourOrderDI> _reserveableOrders;
    private boolean _changedComboBox = true;
    private CoachTourPanel _parent;
    private boolean        _init;
    
    /**
     * Creates new form CoachTourReserve
     */
    public CoachTourReservePanel()
    {
        initComponents();
    }

    @Override
    public void setVisible(boolean aFlag)
    {
        if(aFlag)
        {
            if(_CTGC != null)
            {
                try
                {
                    _reserveableOrders = (List<CoachTourOrderDI>) _CTGC.getReservableOrders();
                } catch (PeriodException ex)
                {
                    _parent.setErrorMsg(ex.getBundleMessage());
                }
            }
            initGUI();
        }
        super.setVisible(aFlag);
    }
    
    public void initGUI()
    {
        _init = true;
        if(_CTGC != null && _coachTourOrders != null)
        {
            _fillingOrders = true;
            _coachTourOrders.removeAllItems();
            
            _coachTourOrders.addItem(Msg.getString("manageoffer.coachtour.selectorder"));
            
            if(_coachTourOrders != null && !_reserveableOrders.isEmpty())
            {
                for(CoachTourOrderDI cto : _reserveableOrders)
                {
                    CoachTourCustomerDI ctc = cto.getCustomer();
                    //noinspection unchecked
                    _coachTourOrders.addItem(cto.getSpecification() + "; " +
                                             ctc.getLastName() + " " +
                                             ctc.getFirstName());
                }
            }
            _fillingOrders   = false;
            _changedComboBox = false;
        }
        updateGUI();
        _init = false;
    }
    
    private boolean _fillingOrders = false;
    public void updateGUI()
    {
        if(_fillingOrders)
        {
            return;
        }
        //ComboBox leeren und neu befüllen
        if(_coachTourOrders != null && _changedComboBox)
        {
            _fillingOrders = true;
            _coachTourOrders.removeAllItems();
            
            _coachTourOrders.addItem(Msg.getString("manageoffer.coachtour.selectorder"));
            
            if(_reserveableOrders != null && !_reserveableOrders.isEmpty())
            {
                for(CoachTourOrderDI cto : _reserveableOrders)
                {
                    CoachTourCustomerDI ctc = cto.getCustomer();
                    //noinspection unchecked
                    _coachTourOrders.addItem(cto.getSpecification() + "; " +
                                             ctc.getLastName() + " " +
                                             ctc.getFirstName());
                }
            }
            
            _fillingOrders   = false;
            _changedComboBox = false;
        }
        //Tabelle leeren und neu befüllen
        if(_coachTourTable != null)
        {
            DefaultTableModel tm = (DefaultTableModel) _coachTourTable.getModel();
            
            //clear Table
            for(int i = tm.getRowCount()-1; i >= 0; i--)
            {
                tm.removeRow(i);
            }
            
            if(_reserveableOrders != null && !_reserveableOrders.isEmpty() &&
               _coachTourOrders != null)
            {
                int index = _coachTourOrders.getSelectedIndex();
                if(index > 0)
                {
                    //minus 1 because of the "pleas-select"
                    CoachTourOrderDI cto = _reserveableOrders.get(index - 1);
                    List<CoachTourScheduleDI> lcts = (List<CoachTourScheduleDI>) cto.getSchedules();
                    
                    //Data loading
                    List<DriverDI> drivers    = (List<DriverDI>) _CTGC.getAllDrivers(cto);
                    List<DriverDI> addDrivers = new LinkedList<>();
                    addDrivers.add(null);
                    addDrivers.addAll(drivers);
                    List<BusDI>    buses      = (List<BusDI>)    _CTGC.getAllBusses (cto);

                    //Wrappers
                    List<GuiDriverWrapper> driversW    = new LinkedList<>();
                    List<GuiDriverWrapper> addDriversW = new LinkedList<>();
                    List<GuiBusWrapper>    busesW      = new LinkedList<>();

                    for(DriverDI d : drivers)
                    {
                        driversW.add(new GuiDriverWrapper(d, cto));
                    }
                    for(DriverDI d : addDrivers)
                    {
                        addDriversW.add(new GuiDriverWrapper(d, cto));
                    }
                    for(BusDI b : buses)
                    {
                        busesW.add(new GuiBusWrapper(b, cto));
                    }

                    JComboBox<GuiDriverWrapper> driver    = new JComboBox<>();
                    JComboBox<GuiDriverWrapper> addDriver = new JComboBox<>();
                    JComboBox<GuiBusWrapper>    bus       = new JComboBox<>();


                    for(GuiDriverWrapper d : driversW)
                    {
                        driver.addItem(d);
                    }
                    for(GuiDriverWrapper d : addDriversW)
                    {
                        addDriver.addItem(d);
                    }
                    for(GuiBusWrapper b : busesW)
                    {
                        bus.addItem(b);
                    }

                    TableColumnModel tcm = _coachTourTable.getColumnModel();
                    tcm.getColumn(1).setCellEditor(new DefaultCellEditor(driver));
                    tcm.getColumn(2).setCellEditor(new DefaultCellEditor(addDriver));
                    tcm.getColumn(3).setCellEditor(new DefaultCellEditor(bus));
                    
                    if(!lcts.isEmpty())
                    {
                        for(CoachTourScheduleDI cts : lcts)
                        {
                            for(int i = 0; i < cts.getNumberOfBusses(); i++)
                            {
                                Object[] row = {cts.getSpecification(),
                                                driversW   .get(0),
                                                addDriversW.get(0),
                                                busesW     .get(0)};

                                tm.addRow(row);
                            }
                        }
                    }
                }
            }
        }
        
        repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        _contentPanel = new javax.swing.JPanel();
        _reserveButton = new javax.swing.JButton();
        _coachTourOrders = new javax.swing.JComboBox();
        _coachTourOrdersLabel = new javax.swing.JLabel();
        _coachTourLabel = new javax.swing.JLabel();
        _coachTourPane = new javax.swing.JScrollPane();
        _coachTourTable = new javax.swing.JTable();
        _resetButton = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        _titleLabel = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        _contentPanel.setLayout(new java.awt.GridBagLayout());

        _reserveButton.setText(Msg.getString("reserve.coachtour.reserve"));
        _reserveButton.setEnabled(false);
        _reserveButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                reserveAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        _contentPanel.add(_reserveButton, gridBagConstraints);

        _coachTourOrders.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                coachTourOrdersAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        _contentPanel.add(_coachTourOrders, gridBagConstraints);

        _coachTourOrdersLabel.setText(Msg.getString("admin.coachtour.orders"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        _contentPanel.add(_coachTourOrdersLabel, gridBagConstraints);

        _coachTourLabel.setText(Msg.getString("admin.coachtour.tours"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        _contentPanel.add(_coachTourLabel, gridBagConstraints);

        _coachTourTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                Msg.getString("offer.coachtour.schedule"),
                Msg.getString("reserve.coachtour.driver"),
                Msg.getString("reserve.coachtour.additionaldriver"),
                Msg.getString("reserve.coachtour.bus")
            }
        )
        {
            Class[] types = new Class []
            {
                String.class, Object.class, Object.class, Object.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true, true, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        _coachTourTable.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusGained(java.awt.event.FocusEvent evt)
            {
                _coachTourTableFocusGained(evt);
            }
        });
        _coachTourPane.setViewportView(_coachTourTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        _contentPanel.add(_coachTourPane, gridBagConstraints);

        _resetButton.setText(Msg.getString("offer.coachtour.reset"));
        _resetButton.setEnabled(false);
        _resetButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                _resetButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        _contentPanel.add(_resetButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        _contentPanel.add(filler1, gridBagConstraints);

        add(_contentPanel, java.awt.BorderLayout.CENTER);

        _titleLabel.setFont(new java.awt.Font("sansserif", 0, 18)); // NOI18N
        _titleLabel.setText(Msg.getString("reserve.coachtour.title"));
        add(_titleLabel, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void coachTourOrdersAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_coachTourOrdersAction
    {//GEN-HEADEREND:event_coachTourOrdersAction
        if(!_init)
        {
            validate();
            updateGUI();
            if(_coachTourOrders.getSelectedIndex() > 0)
            {
                disableNavi();
            }
            else
            {
                enableNavi();
            }
        }
    }//GEN-LAST:event_coachTourOrdersAction

    private void reserveAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_reserveAction
    {//GEN-HEADEREND:event_reserveAction
        DefaultTableModel tm = (DefaultTableModel) _coachTourTable.getModel();
        
        int index = _coachTourOrders.getSelectedIndex();
        //minus 1 because of the "pleas-select"
        index = index - 1;
        if(index >= 0)
        {
            //Daten Auslesen und zusammenfassen für den Controller
            CoachTourOrderDI cto = _reserveableOrders.get(index);
            List<CoachTourScheduleDI> lcts = (List<CoachTourScheduleDI>) cto.getSchedules();
            
            List<CoachTourScheduleDI> trips = new LinkedList<>();
            for(CoachTourScheduleDI cts : lcts)
            {
                for(int i = 0; i < cts.getNumberOfBusses(); i++)
                {
                    trips.add(cts);
                }
            }
            
            List<DriverDI> drivers    = new LinkedList<>();
            List<DriverDI> addDrivers = new LinkedList<>();
            List<BusDI>    buses      = new LinkedList<>();
            
            for(int i = 0; i < tm.getRowCount(); i++)
            {
                drivers   .add(((GuiDriverWrapper) tm.getValueAt(i, 1)).getDriver());
                addDrivers.add(((GuiDriverWrapper) tm.getValueAt(i, 2)).getDriver());
                buses     .add(((GuiBusWrapper)    tm.getValueAt(i, 3)).getBus());
            }
            
            try
            {
                _CTGC.reserveCoachTour(cto, trips, drivers, addDrivers, buses);
            }catch (PeriodException | UncompatibleConvertionException | DBException ex)
            {
                _parent.setErrorMsg(ex.getBundleMessage());
                Logger.getLogger(CoachTourReservePanel.class.getName()).log(Level.SEVERE, null, ex);
            }
            _reserveableOrders.remove(index);
            _changedComboBox = true;
            enableNavi();
            this.validate();
            this.updateGUI();
        }
        
    }//GEN-LAST:event_reserveAction

    private void _resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event__resetButtonActionPerformed
        _coachTourOrders.setSelectedIndex(0);
        this.validate();
        this.updateGUI();
        enableNavi();
    }//GEN-LAST:event__resetButtonActionPerformed

    private void _coachTourTableFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event__coachTourTableFocusGained
        disableNavi();
    }//GEN-LAST:event__coachTourTableFocusGained


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel _coachTourLabel;
    private javax.swing.JComboBox _coachTourOrders;
    private javax.swing.JLabel _coachTourOrdersLabel;
    private javax.swing.JScrollPane _coachTourPane;
    private javax.swing.JTable _coachTourTable;
    private javax.swing.JPanel _contentPanel;
    private javax.swing.JButton _reserveButton;
    private javax.swing.JButton _resetButton;
    private javax.swing.JLabel _titleLabel;
    private javax.swing.Box.Filler filler1;
    // End of variables declaration//GEN-END:variables

    
    public void setParent(CoachTourPanel p)
    {
        _parent = p;
    }
    private void enableNavi()
    {
        if (_parent != null)
        {
            _parent.enableNavi();
            enableComponents(false);
        }
    }
    private void disableNavi()
    {
        if (_parent != null && !_init)
        {
            _parent.disableNavi();
            enableComponents(true);
        }
    }

    private void enableComponents(boolean b)
    {
        _resetButton.setEnabled(b);
        _reserveButton.setEnabled(b);
    }
}
