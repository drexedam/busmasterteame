/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.gui.bus_module;

import at.teame.busmaster.bundles.Msg;
import at.teame.busmaster.gui.MainGUI;
import java.awt.CardLayout;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;

/**
 *
 * @author Morent Jochen <jochen.morent@students.fhv.at>
 */
public class BusModulePanel extends javax.swing.JPanel
{
    private MainGUI _main;

    /**
     * Creates new form CoachTourPanel
     */
    public BusModulePanel()
    {
        initComponents();
        setBorder(BorderFactory.createEmptyBorder(10,10,1,1));

    }

    @Override
    public void setVisible(boolean aFlag)
    {
        CardLayout cl = (CardLayout) _content.getLayout();
        
        cl.first(_content);
        
        super.setVisible(aFlag);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        _content = new javax.swing.JPanel();
        _busCreate = new at.teame.busmaster.gui.bus_module.BusCreatePanel();
        _busChange = new at.teame.busmaster.gui.bus_module.BusChangePanel();
        _busDelete = new at.teame.busmaster.gui.bus_module.BusDeletePanel();
        _busTypeCreate = new at.teame.busmaster.gui.bus_module.BusTypeCreatePanel();
        _busTypeDelete = new at.teame.busmaster.gui.bus_module.BusTypeDeletePanel();
        _moduleCreate = new at.teame.busmaster.gui.bus_module.ModuleCreatePanel();
        _moduleDelete = new at.teame.busmaster.gui.bus_module.ModuleDeletePanel();
        _moduleTypeCreate = new at.teame.busmaster.gui.bus_module.ModuleTypeCreatePanel();
        _moduleTypeDelete = new at.teame.busmaster.gui.bus_module.ModuleTypeDeletePanel();
        _naviWrapper = new javax.swing.JPanel();
        _navi = new at.teame.busmaster.gui.bus_module.BusModuleNaviPanel();

        _content.setLayout(new java.awt.CardLayout());
        _content.add(_busCreate, "crBus");
        _busCreate.setParent(this);
        _content.add(_busChange, "chBus");
        _busChange.setParent(this);
        _content.add(_busDelete, "deBus");
        _content.add(_busTypeCreate, "crBusType");
        _busTypeCreate.setParent(this);
        _content.add(_busTypeDelete, "deBusType");
        _content.add(_moduleCreate, "crModule");
        _moduleCreate.setParent(this);
        _content.add(_moduleDelete, "deModule");
        _moduleDelete.setParent(this);
        _content.add(_moduleTypeCreate, "crModuleType");
        _moduleTypeCreate.setParent(this);
        _content.add(_moduleTypeDelete, "deModuleType");
        _moduleTypeDelete.setParent(this);

        _naviWrapper.setLayout(new java.awt.BorderLayout());
        _naviWrapper.add(_navi, java.awt.BorderLayout.CENTER);
        _navi.setParent(this);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(_naviWrapper, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(_content, javax.swing.GroupLayout.DEFAULT_SIZE, 618, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(_content, javax.swing.GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE)
            .addComponent(_naviWrapper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private at.teame.busmaster.gui.bus_module.BusChangePanel _busChange;
    private at.teame.busmaster.gui.bus_module.BusCreatePanel _busCreate;
    private at.teame.busmaster.gui.bus_module.BusDeletePanel _busDelete;
    private at.teame.busmaster.gui.bus_module.BusTypeCreatePanel _busTypeCreate;
    private at.teame.busmaster.gui.bus_module.BusTypeDeletePanel _busTypeDelete;
    private javax.swing.JPanel _content;
    private at.teame.busmaster.gui.bus_module.ModuleCreatePanel _moduleCreate;
    private at.teame.busmaster.gui.bus_module.ModuleDeletePanel _moduleDelete;
    private at.teame.busmaster.gui.bus_module.ModuleTypeCreatePanel _moduleTypeCreate;
    private at.teame.busmaster.gui.bus_module.ModuleTypeDeletePanel _moduleTypeDelete;
    private at.teame.busmaster.gui.bus_module.BusModuleNaviPanel _navi;
    private javax.swing.JPanel _naviWrapper;
    // End of variables declaration//GEN-END:variables

    public void setMain(MainGUI main)
    {
        _main = main;
    }
    public void setView(String coachTour)
    {
        CardLayout cards = (CardLayout) _content.getLayout();
        cards.show(_content, coachTour);
    }
    public void back()
    {
        if(_main != null)
        {
            _main.setView("welcome");
        }
    }
    
    public void disableNavi() {
        _navi.disableNavi();
    }
    public void enableNavi() {
        _navi.enableNavi();
    }
    
    public void setErrorMsg(String bundelKey)
    {
        JOptionPane.showMessageDialog(null, Msg.getString(bundelKey));
    }
}
