/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.gui;

/**
 * @author  Morent Jochen <jochen.morent@students.fhv.at>
 * @org     FHV <fhv.at>
 * @project BusMaster-Team-E
 * @date    23.04.2014
 */
public enum ConfigLoading
{
    DO_LOADING, LET_IT_BE;
    
    
    private static ConfigLoading setting = LET_IT_BE;
    
    public static void setTo_DO_LOADING()
    {
        setting = DO_LOADING;
    }
    public static void setTo_LET_IT_BE()
    {
        setting = LET_IT_BE;
    }
    public static ConfigLoading getSetting()
    {
        return setting;
    }
}
