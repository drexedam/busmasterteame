/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.gui.publictransport;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.bundles.Msg;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.ContactDetailsTypeDI;
import at.teame.busmaster.gui.guicontroller.CircleDefinitonGuiController;
import at.teame.busmaster.gui.guicontroller.wrapper.BusWrapper;
import at.teame.busmaster.gui.guicontroller.wrapper.DayTypeWrapper;
import at.teame.busmaster.gui.guicontroller.wrapper.PublicBusTransportWrapper;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Morent Jochen <jochen.morent@students.fhv.at>
 */
public class PublicTransportCircelDefPanel extends javax.swing.JPanel
{
    private final CircleDefinitonGuiController _CDGC        = CircleDefinitonGuiController.getInstance();
    private List<DayTypeWrapper>            _dayTypes       = new LinkedList<>();
    private List<BusWrapper>                _buses          = new LinkedList<>();
    private List<PublicBusTransportWrapper> _selectablePBTs = new LinkedList<>();
    private List<PublicBusTransportWrapper> _selectedPBTs   = new LinkedList<>();
    private PublicTransportPanel            _parent;
    
    /**
     * Creates new form PublicTransportCircelDefPanel
     */
    public PublicTransportCircelDefPanel()
    {
        initComponents();
        dayTypChangedAction(null);
        _selectableSorter = new TableRowSorter(_selectableTable.getModel());
        _selectableTable.setRowSorter(_selectableSorter);
    }

    @Override
    public void setVisible(boolean aFlag)
    {
        if(aFlag)
        {
            if(_CDGC != null)
            {
                _buses = _CDGC.getAllBusses();
                _dayTypes = _CDGC.getAllDayTypes();
            }
            initGUI();
        }
        super.setVisible(aFlag);
    }
    
    public void initGUI()
    {
        if(_CDGC != null && _daytyp != null)
        {
            _daytyp.removeAllItems();
            for(DayTypeWrapper dtw : _dayTypes)
            {
                _daytyp.addItem(dtw);
            }
        }
        if(_CDGC != null && _defaultBus != null)
        {
            _defaultBus.removeAllItems();
            _defaultBus.addItem(new BusWrapper(null));
            for(BusWrapper bw : _buses)
            {
                _defaultBus.addItem(bw);
            }
        }
        updateGUI();
    }

    public void updateGUI()
    {
        if(_selectableTable != null)
        {
            DefaultTableModel tm = (DefaultTableModel) _selectableTable.getModel();
            
            //clear Table
            for(int i = tm.getRowCount()-1; i >= 0; i--)
            {
                tm.removeRow(i);
            }
            
            for(PublicBusTransportWrapper pbtw : _selectablePBTs)
            {
                //"Routenummer", "Starttime", "Endtime", "Startstation", "Endstation"
                
                Object[] row = {pbtw.getNumber(),
                                pbtw.getStart(),
                                pbtw.getEnd(),
                                pbtw.getStartStaion(),
                                pbtw.getEndStaion()};
                tm.addRow(row);
            }
        }
        
        if(_selectedTable != null)
        {
            DefaultTableModel tm = (DefaultTableModel) _selectedTable.getModel();
            
            //clear Table
            for(int i = tm.getRowCount()-1; i >= 0; i--)
            {
                tm.removeRow(i);
            }
            
            for(PublicBusTransportWrapper pbtw : _selectedPBTs)
            {
                //"Routenummer", "Starttime", "Endtime", "Startstation", "Endstation"
                
                Object[] row = {pbtw.getNumber(),
                                pbtw.getStart(),
                                pbtw.getEnd(),
                                pbtw.getStartStaion(),
                                pbtw.getEndStaion()};
                tm.addRow(row);
            }
        }
        
        
        super.repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        _titlePanel = new javax.swing.JPanel();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        _titleLabel = new javax.swing.JLabel();
        _contentPanel = new javax.swing.JPanel();
        _generalInfoLabel = new javax.swing.JLabel();
        _specificationLabel = new javax.swing.JLabel();
        _specification = new javax.swing.JTextField();
        _daytypLabel = new javax.swing.JLabel();
        _daytyp = new javax.swing.JComboBox();
        _defaultBusLabel = new javax.swing.JLabel();
        _defaultBus = new javax.swing.JComboBox();
        _publicBusTransportLabel = new javax.swing.JLabel();
        _selectableLabel = new javax.swing.JLabel();
        _selectableScrollPane = new javax.swing.JScrollPane();
        _selectableTable = new javax.swing.JTable();
        _addButton = new javax.swing.JButton();
        _selectedLabel = new javax.swing.JLabel();
        _selectedScrollPane = new javax.swing.JScrollPane();
        _selectedTable = new javax.swing.JTable();
        _remButton = new javax.swing.JButton();
        _moveUpButton = new javax.swing.JButton();
        _moveDownButton = new javax.swing.JButton();
        filler3 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10), new java.awt.Dimension(32767, 10));
        filler4 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10), new java.awt.Dimension(32767, 10));
        filler5 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10), new java.awt.Dimension(32767, 10));
        filler6 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10), new java.awt.Dimension(32767, 10));
        filler7 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10), new java.awt.Dimension(32767, 10));
        filler8 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10), new java.awt.Dimension(32767, 10));
        filler9 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10), new java.awt.Dimension(32767, 10));
        filler10 = new javax.swing.Box.Filler(new java.awt.Dimension(10, 0), new java.awt.Dimension(10, 0), new java.awt.Dimension(10, 32767));
        filler11 = new javax.swing.Box.Filler(new java.awt.Dimension(10, 0), new java.awt.Dimension(10, 0), new java.awt.Dimension(10, 32767));
        filter = new javax.swing.JTextField();
        _filterLabel = new javax.swing.JLabel();
        _buttonsPanel = new javax.swing.JPanel();
        _saveButton = new javax.swing.JButton();
        _cancelButton = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));

        setLayout(new java.awt.BorderLayout());

        _titlePanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        _titlePanel.add(filler2, gridBagConstraints);

        _titleLabel.setFont(new java.awt.Font("sansserif", 0, 18)); // NOI18N
        _titleLabel.setText(Msg.getString("circle.pts.title"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        _titlePanel.add(_titleLabel, gridBagConstraints);

        add(_titlePanel, java.awt.BorderLayout.NORTH);

        _contentPanel.setLayout(new java.awt.GridBagLayout());

        _generalInfoLabel.setText(Msg.getString("circle.pts.gi"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        _contentPanel.add(_generalInfoLabel, gridBagConstraints);

        _specificationLabel.setText(Msg.getString("circle.pts.specification"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        _contentPanel.add(_specificationLabel, gridBagConstraints);

        _specification.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                specificationEditAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        _contentPanel.add(_specification, gridBagConstraints);

        _daytypLabel.setText(Msg.getString("circle.pts.daytyp"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        _contentPanel.add(_daytypLabel, gridBagConstraints);

        if(_CDGC != null)
        {
            _dayTypes = (List<DayTypeWrapper>) _CDGC.getAllDayTypes();

            for(DayTypeWrapper dt : _dayTypes)
            {
                _daytyp.addItem(dt);
            }
        }
        _daytyp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dayTypChangedAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        _contentPanel.add(_daytyp, gridBagConstraints);

        _defaultBusLabel.setText(Msg.getString("circle.pts.bus"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        _contentPanel.add(_defaultBusLabel, gridBagConstraints);

        if(_CDGC != null)
        {
            _buses = (List<BusWrapper>) _CDGC.getAllBusses();
            _defaultBus.addItem(new BusWrapper(null));

            for(BusWrapper b : _buses)
            {
                _defaultBus.addItem(b);
            }
        }
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        _contentPanel.add(_defaultBus, gridBagConstraints);

        _publicBusTransportLabel.setText(Msg.getString("circle.pts.pbt"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        _contentPanel.add(_publicBusTransportLabel, gridBagConstraints);

        _selectableLabel.setText(Msg.getString("circle.pts.selectable"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        _contentPanel.add(_selectableLabel, gridBagConstraints);

        _selectableTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                Msg.getString("circle.pts.routenumber"), Msg.getString("circle.pts.starttime"), Msg.getString("circle.pts.endtime"), Msg.getString("circle.pts.startstation"), Msg.getString("circle.pts.endstation")
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        _selectableScrollPane.setViewportView(_selectableTable);
        ListSelectionModel lsml = _selectableTable.getSelectionModel();
        lsml.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (_selectableTable.getSelectedRowCount() > 1) {
                    _addButton.setText(Msg.getString("circle.pts.addmulti"));
                } else {
                    _addButton.setText(Msg.getString("circle.pts.add"));
                    if (_selectableTable.getSelectedRowCount() == 0) {
                        _addButton.setEnabled(false);
                    } else {
                        _addButton.setEnabled(true);
                    }
                }
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        _contentPanel.add(_selectableScrollPane, gridBagConstraints);

        _addButton.setText(Msg.getString("circle.pts.add"));
        _addButton.setEnabled(false);
        _addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        _contentPanel.add(_addButton, gridBagConstraints);

        _selectedLabel.setText(Msg.getString("circle.pts.selected"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        _contentPanel.add(_selectedLabel, gridBagConstraints);

        _selectedTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                Msg.getString("circle.pts.routenumber"), Msg.getString("circle.pts.starttime"), Msg.getString("circle.pts.endtime"), Msg.getString("circle.pts.startstation"), Msg.getString("circle.pts.endstation")
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        _selectedScrollPane.setViewportView(_selectedTable);
        ListSelectionModel lsm = _selectedTable.getSelectionModel();
        lsm.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (_selectedTable.getSelectedRowCount() == 0) {
                    _remButton.setEnabled(false);
                    _moveDownButton.setEnabled(false);
                    _moveUpButton.setEnabled(false);
                } else {
                    _remButton.setEnabled(true);
                    _moveDownButton.setEnabled(true);
                    _moveUpButton.setEnabled(true);
                }
                int j = _selectedTable.getSelectedRow();
                int scroll = 0;
                if (j != -1) {
                    long i = getStartInMillisec(_selectedPBTs, j);
                    for (int k = 0; k < _selectableTable.getRowCount() && i != getStartInMillisec(_selectablePBTs, k); k++) scroll++;
                }
                _selectableTable.scrollRectToVisible(_selectableTable.getCellRect(scroll,0, true));
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        _contentPanel.add(_selectedScrollPane, gridBagConstraints);

        _remButton.setText(Msg.getString("circle.pts.remove"));
        _remButton.setEnabled(false);
        _remButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        _contentPanel.add(_remButton, gridBagConstraints);

        _moveUpButton.setText(Msg.getString("circle.pts.up"));
        _moveUpButton.setEnabled(false);
        _moveUpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveUpAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        _contentPanel.add(_moveUpButton, gridBagConstraints);

        _moveDownButton.setText(Msg.getString("circle.pts.down"));
        _moveDownButton.setEnabled(false);
        _moveDownButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveDownAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        _contentPanel.add(_moveDownButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 17;
        gridBagConstraints.gridwidth = 8;
        _contentPanel.add(filler3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.gridwidth = 8;
        _contentPanel.add(filler4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 8;
        _contentPanel.add(filler5, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 8;
        _contentPanel.add(filler6, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 8;
        _contentPanel.add(filler7, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 8;
        _contentPanel.add(filler8, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 8;
        _contentPanel.add(filler9, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 18;
        _contentPanel.add(filler10, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 18;
        _contentPanel.add(filler11, gridBagConstraints);

        filter.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                filterCaretUpdate(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        _contentPanel.add(filter, gridBagConstraints);

        _filterLabel.setText(Msg.getString("circle.pts.filter"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        _contentPanel.add(_filterLabel, gridBagConstraints);

        add(_contentPanel, java.awt.BorderLayout.CENTER);

        _buttonsPanel.setLayout(new java.awt.GridBagLayout());

        _saveButton.setText(Msg.getString("circle.pts.save"));
        _saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        _buttonsPanel.add(_saveButton, gridBagConstraints);

        _cancelButton.setText(Msg.getString("circle.pts.cancel"));
        _cancelButton.setEnabled(false);
        _cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        _buttonsPanel.add(_cancelButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        _buttonsPanel.add(filler1, gridBagConstraints);

        add(_buttonsPanel, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents

    private void addAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_addAction
    {//GEN-HEADEREND:event_addAction
        if (!_cancelButton.isEnabled()) {
            _cancelButton.setEnabled(true);
            _parent.disableNavi();
        }
        DefaultTableModel dtm = (DefaultTableModel) _selectedTable.getModel();
        List<PublicBusTransportWrapper> toRemove = new LinkedList<>();
        for (int j = 0; j < _selectableTable.getRowCount(); j++) {
            if (_selectableTable.isRowSelected(j)) {  
                toRemove.add(_selectablePBTs.get(j));
                if (_selectedTable.getSelectedRow() != -1) {
                    _selectedPBTs.add(_selectedTable.getSelectedRow()+1, _selectablePBTs.get(j));
                } else {
                    _selectedPBTs.add(_selectablePBTs.get(j));
                }
            }
        }
        _selectablePBTs.removeAll(toRemove);
        this.updateGUI();
        this.validate();
        this.updateUI();
    }//GEN-LAST:event_addAction

    private void removeAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_removeAction
    {//GEN-HEADEREND:event_removeAction
        for(int i = _selectedTable.getRowCount()-1; i >= 0; i--)
        {
            if(_selectedTable.isRowSelected(i))
            {
                PublicBusTransportWrapper pbtw = _selectedPBTs.get(i);
                _selectedPBTs.remove(pbtw);
                int index = 0;
                while (getStartInMillisec(_selectablePBTs, index) < pbtw.getPublicBusTransport().getStart().getTime()) index ++;
                System.out.println(index);
                _selectablePBTs.add(index, pbtw);
            }
        }
        this.updateGUI();
        this.validate();
        this.updateUI();
    }//GEN-LAST:event_removeAction

    private void moveUpAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_moveUpAction
    {//GEN-HEADEREND:event_moveUpAction
        ListSelectionModel model = _selectedTable.getSelectionModel();
        
        for(int i = 1; i < _selectedTable.getRowCount(); i++)
        {
            if( _selectedTable.isRowSelected(i  ) &&
               !_selectedTable.isRowSelected(i-1)    )
            {
                swapDest(i-1, i);
                this.validate();
                this.updateGUI();
                model.addSelectionInterval(i-1, i-1);
            }
        }      
    }//GEN-LAST:event_moveUpAction

    private void moveDownAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_moveDownAction
    {//GEN-HEADEREND:event_moveDownAction
        ListSelectionModel model = _selectedTable.getSelectionModel();
        
        for(int i = _selectedTable.getRowCount()-2; i >= 0; i--)
        {
            if( _selectedTable.isRowSelected(i  ) &&
               !_selectedTable.isRowSelected(i+1)    )
            {
                swapDest(i+1, i);
                this.validate();
                this.updateGUI();
                model.addSelectionInterval(i+1, i+1);
            }
        }
    }//GEN-LAST:event_moveDownAction

    private void cancelAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cancelAction
    {//GEN-HEADEREND:event_cancelAction
        _parent.enableNavi();
        _cancelButton.setEnabled(false);
        this.clear();
        this.validate();
        this.updateUI();
    }//GEN-LAST:event_cancelAction

    private void saveAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_saveAction
    {//GEN-HEADEREND:event_saveAction
        if (_CDGC != null && _selectedPBTs.size() > 0 && _specification.getText().length() > 0) {
            try {
            _CDGC.saveCircleDefinition((BusWrapper)_defaultBus.getSelectedItem(), (DayTypeWrapper)_daytyp.getSelectedItem(), _selectedPBTs, _specification.getText());
            } catch (PeriodException | UncompatibleConvertionException e) {
                JOptionPane.showMessageDialog(null, Msg.getString(e.getBundleMessage()));
            }
            this.clear();
            _parent.enableNavi();
            this.validate();
            this.updateGUI();
            this.updateUI();
        } else {
            JOptionPane.showMessageDialog(null, Msg.getString("offer.coachtour.mandatoryfailure"), Msg.getString("offer.coachtour.mandatoryfailure"), JOptionPane.OK_OPTION);
        }
    }//GEN-LAST:event_saveAction

    private void dayTypChangedAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_dayTypChangedAction
    {//GEN-HEADEREND:event_dayTypChangedAction
        if (_selectedPBTs.isEmpty()) {
            DayTypeWrapper daytype = (DayTypeWrapper) _daytyp.getSelectedItem();
            if (daytype != null && _CDGC != null)
            {
                _selectablePBTs.clear();
                _selectableTable.removeAll();
                List<PublicBusTransportWrapper> pbts = _CDGC.getPublicBusTransports(daytype);
                for (PublicBusTransportWrapper pbtw : pbts) {
                    _selectablePBTs.add(pbtw);
                }
                updateGUI();
            }
            for(BusWrapper bw : _buses)
            {
                bw.setDayType(daytype);
            }
            updateGUI();
        } else {
            if (JOptionPane.showConfirmDialog(null, "Reset", "Do you want to clear the lists?", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
                _selectedPBTs.clear();
                _selectedTable.removeAll();
                dayTypChangedAction(evt);
            }
        }
    }//GEN-LAST:event_dayTypChangedAction

    private void specificationEditAction(java.awt.event.FocusEvent evt)//GEN-FIRST:event_specificationEditAction
    {//GEN-HEADEREND:event_specificationEditAction
        if (_specification.getText().length() > 0) {
            _parent.disableNavi();
            _cancelButton.setEnabled(true);
        }
    }//GEN-LAST:event_specificationEditAction

    private void filterCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_filterCaretUpdate
        String filtertext = filter.getText();
        if (filtertext.length() == 0) {
            _selectableSorter.setRowFilter(null);
        } else {
            _selectableSorter.setRowFilter(RowFilter.regexFilter(filtertext));
        }
    }//GEN-LAST:event_filterCaretUpdate

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton _addButton;
    private javax.swing.JPanel _buttonsPanel;
    private javax.swing.JButton _cancelButton;
    private javax.swing.JPanel _contentPanel;
    private javax.swing.JComboBox _daytyp;
    private javax.swing.JLabel _daytypLabel;
    private javax.swing.JComboBox _defaultBus;
    private javax.swing.JLabel _defaultBusLabel;
    private javax.swing.JLabel _filterLabel;
    private javax.swing.JLabel _generalInfoLabel;
    private javax.swing.JButton _moveDownButton;
    private javax.swing.JButton _moveUpButton;
    private javax.swing.JLabel _publicBusTransportLabel;
    private javax.swing.JButton _remButton;
    private javax.swing.JButton _saveButton;
    private javax.swing.JLabel _selectableLabel;
    private javax.swing.JScrollPane _selectableScrollPane;
    private javax.swing.JTable _selectableTable;
    private javax.swing.JLabel _selectedLabel;
    private javax.swing.JScrollPane _selectedScrollPane;
    private javax.swing.JTable _selectedTable;
    private javax.swing.JTextField _specification;
    private javax.swing.JLabel _specificationLabel;
    private javax.swing.JLabel _titleLabel;
    private javax.swing.JPanel _titlePanel;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler10;
    private javax.swing.Box.Filler filler11;
    private javax.swing.Box.Filler filler2;
    private javax.swing.Box.Filler filler3;
    private javax.swing.Box.Filler filler4;
    private javax.swing.Box.Filler filler5;
    private javax.swing.Box.Filler filler6;
    private javax.swing.Box.Filler filler7;
    private javax.swing.Box.Filler filler8;
    private javax.swing.Box.Filler filler9;
    private javax.swing.JTextField filter;
    // End of variables declaration//GEN-END:variables
   
    private final TableRowSorter _selectableSorter;
    
    public void setParent(PublicTransportPanel p)
    {
        _parent = p;
    }

    private void clear() {
        _specification.setText("");
        _selectableTable.removeAll();
        _selectedTable.removeAll();
        _selectablePBTs.clear();
        _selectedPBTs.clear();
        _daytyp.setSelectedIndex(0);
        _defaultBus.setSelectedIndex(0);
    }
    
    private void swapDest(int i, int j)
    {
        PublicBusTransportWrapper temp = _selectedPBTs.get(i);
        _selectedPBTs.remove(temp);
        _selectedPBTs.add(j, temp);
    }
    
    private long getStartInMillisec(List<PublicBusTransportWrapper> list, int row) {
        if (list.size() > 0) {
            return (list.get(row).getPublicBusTransport().getStart().getTime());
        } else {
            return -1;
        }
    }
}
