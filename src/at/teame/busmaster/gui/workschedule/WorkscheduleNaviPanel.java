/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.gui.workschedule;

import at.teame.busmaster.bundles.Msg;

/**
 *
 * @author Morent Jochen <jochen.morent@students.fhv.at>
 */
public class WorkscheduleNaviPanel extends javax.swing.JPanel
{
    private WorkschedulePanel _parent;
    
    /**
     * Creates new form CoachTourNavi
     */
    public WorkscheduleNaviPanel()
    {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        _naviTitleLabel = new javax.swing.JLabel();
        _backButton = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));

        setLayout(new java.awt.GridBagLayout());

        _naviTitleLabel.setFont(new java.awt.Font("sansserif", 0, 24)); // NOI18N
        _naviTitleLabel.setText(Msg.getString("navi.pts.title"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        add(_naviTitleLabel, gridBagConstraints);

        _backButton.setText(Msg.getString("navi.coachtour.back"));
        _backButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                backAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 52;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.1;
        add(_backButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 0.1;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void backAction(java.awt.event.ActionEvent evt)//GEN-FIRST:event_backAction
    {//GEN-HEADEREND:event_backAction
        if(_parent != null)
        {
            _parent.back();
        }
    }//GEN-LAST:event_backAction


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton _backButton;
    private javax.swing.JLabel _naviTitleLabel;
    private javax.swing.Box.Filler filler1;
    // End of variables declaration//GEN-END:variables

    public void setParent(WorkschedulePanel p)
    {
        _parent = p;
    }
    
    public void disableNavi() {
        _backButton.setEnabled(false);
    }
    public void enableNavi() {
        _backButton.setEnabled(true);
    }
}
