package at.teame.busmaster.controller.pbtParts;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.Controller;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteSectionDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.StationDI;
import at.teame.busmaster.domain.persistencedata.publicbustransport.Route;
import at.teame.busmaster.domain.persistencedata.publicbustransport.RouteSection;
import at.teame.busmaster.domain.persistencedata.publicbustransport.Station;
import at.teame.busmaster.persistence.publicbustransport.RouteDAO;
import at.teame.busmaster.persistence.publicbustransport.RouteSectionDAO;
import at.teame.busmaster.persistence.publicbustransport.StationDAO;

public class StationAndRouteController extends Controller{
	
	private StationDAO _stationDAO= StationDAO.getInstance();
	private RouteDAO _routeDAO = RouteDAO.getInstance();
	private RouteSectionDAO _sectionDAO = RouteSectionDAO.getInstance();
	
	/**
	 * 
	 * @return the list of all stations
	 */
	public List<? extends StationDI> getAllStations(){
		return _stationDAO.getAllRealStations();
	}
	
	/**
	 * 
	 * @return the list of all routes
	 */
	public List<? extends RouteDI> getAllRoutes(){
		return _routeDAO.getAllRealRoutes();
	}
	/**
	 * 
	 * @return the list of all route sections
	 */
	public List<? extends RouteSectionDI>getAllRouteSections(){
		return _sectionDAO.getAllRealRouteSections();
	}
	/**
	 * 
	 * @param station the station to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createStation(StationDI station) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_stationDAO.saveStation((Station) station.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving station", "exception.DBException.savingStation", e);
		}
	}
	/**
	 * 
	 * @param station the station to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeStation(StationDI station) throws PeriodException, UncompatibleConvertionException{
		_stationDAO.removeStation((Station) station.convertToDomain());
	}
	/**
	 * 
	 * @param route the route to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createRoute(RouteDI route) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_routeDAO.saveRoute((Route) route.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving route", "exception.DBException.savingRoute", e);
		}
	}
	/**
	 * 
	 * @param route the route to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeRoute(RouteDI route) throws PeriodException, UncompatibleConvertionException{
		_routeDAO.removeRoute((Route) route.convertToDomain());
	}
	/**
	 * 
	 * @param section the route section to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createRouteSection(RouteSectionDI section) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_sectionDAO.saveRouteSection((RouteSection) section.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving route section", "exception.DBException.savingSection", e);
		}
	}
	/**
	 * 
	 * @param section the route section to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeRouteSection(RouteSectionDI section) throws PeriodException, UncompatibleConvertionException{
		_sectionDAO.removeRouteSection((RouteSection) section.convertToDomain());
	}
}
