package at.teame.busmaster.controller.pbtParts;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.Controller;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.publicbustransport.PublicBusTransportDI;
import at.teame.busmaster.domain.persistencedata.publicbustransport.PublicBusTransport;
import at.teame.busmaster.persistence.publicbustransport.PublicBusTransportDAO;

public class PublicBusTransportController extends Controller{
	
	private PublicBusTransportDAO _transportDAO = PublicBusTransportDAO.getInstance();
	
	/**
	 * 
	 * @return the list of all public bus transports
	 */
	public List<? extends PublicBusTransportDI> getAllPublicBusTransports(){
		return _transportDAO.getAllTransports();
	}
	/**
	 * 
	 * @param transport the public bus transport to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createPublicBusTransport(PublicBusTransportDI transport) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_transportDAO.savePublicBusTransport((PublicBusTransport) transport.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving public bus transport", "exception.DBException.savingPublicBusTransport", e);
		}
	}
	/**
	 * 
	 * @param transport the public bus transport to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removePublicBusTransport(PublicBusTransportDI transport) throws PeriodException, UncompatibleConvertionException{
		_transportDAO.removePublicBusTransport((PublicBusTransport) transport.convertToDomain());
	}
}
