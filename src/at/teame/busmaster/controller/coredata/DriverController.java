package at.teame.busmaster.controller.coredata;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverTypeDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverUnavailabilityDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverUnavailabilityTypeDI;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.DriverType;
import at.teame.busmaster.domain.persistencedata.driver.DriverUnavailability;
import at.teame.busmaster.domain.persistencedata.driver.DriverUnavailabilityType;
import at.teame.busmaster.persistence.driver.DriverDAO;
import at.teame.busmaster.persistence.driver.DriverTypeDAO;
import at.teame.busmaster.persistence.driver.DriverUnavailabilityDAO;
import at.teame.busmaster.persistence.driver.DriverUnavailabilityTypeDAO;

public class DriverController extends ContactDetailsTypeController{
	
	DriverDAO _driverDAO= DriverDAO.getInstance();
	DriverTypeDAO _driverTypeDAO = DriverTypeDAO.getInstance();
	DriverUnavailabilityDAO _unavailabilityDAO = DriverUnavailabilityDAO.getInstance();
	DriverUnavailabilityTypeDAO _unavailabilityTypeDAO = DriverUnavailabilityTypeDAO.getInstance();
	
	/**
	 * 
	 * @return the list of all drivers
	 */
	public List<? extends DriverDI> getAllDrivers(){
		return _driverDAO.getAllDrivers();
	}
	/**
	 * 
	 * @return the list of all driver types
	 */
	public List<? extends DriverTypeDI> getAllDriverTypes(){
		return _driverTypeDAO.getAllDriverTypes();
	}
	
	/**
	 * 
	 * @param driver for which the unavailabilities will return
	 * @return the list of all driver unavailabilities
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public List<? extends DriverUnavailabilityDI> getAllDriverUnavailabilities(DriverDI driver) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			return _unavailabilityDAO.getUnavailable((Driver)driver.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during getting all driverunavailabilities", "exception.DBException.getDriverUnavailabilities", e);
		}
	}
	/**
	 * 
	 * @return list of all driver unavailability types
	 */
	public List<? extends DriverUnavailabilityTypeDI> getAllDriverUnavailabilityTypes(){
		return _unavailabilityTypeDAO.getAllDriverUnavailableTypes();
	}
	/**
	 * 
	 * @param driver the driver to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createDriver(DriverDI driver) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_driverDAO.saveDriver((Driver)driver.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving driver", "exception.DBException.savingDriver", e);
		}
	}	
	/**
	 * 
	 * @param driver the driver to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeDriver(DriverDI driver) throws PeriodException, UncompatibleConvertionException{
		_driverDAO.removeDriver((Driver)driver.convertToDomain());
	}
	/**
	 * 
	 * @param type the driver type to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createDriverType(DriverTypeDI type) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_driverTypeDAO.saveDriverType((DriverType)type.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving driver type", "exception.DBException.savingDriverType", e);
		}
	}
	/**
	 * 
	 * @param type the driver type to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeDriverType(DriverTypeDI type) throws PeriodException, UncompatibleConvertionException{
		_driverTypeDAO.removeDriverType((DriverType)type.convertToDomain());
	}
	/**
	 * 
	 * @param unavailability the driver unavailability to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createDriverUnavailability(DriverUnavailabilityDI unavailability) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_unavailabilityDAO.saveDriverUnavailable((DriverUnavailability)unavailability.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving driver unavailability", "exception.DBException.savingDriverUnavailability", e);
		}
	}
	/**
	 * 
	 * @param unavailability the driver unavailability to delete
	 * @throws UncompatibleConvertionException 
	 * @throws PeriodException 
	 */
	public void removeDriverUnavailability(DriverUnavailabilityDI unavailability) throws PeriodException, UncompatibleConvertionException{
		_unavailabilityDAO.removeDriverUnavailability((DriverUnavailability)unavailability.convertToDomain());
	}
	/**
	 * 
	 * @param unavailabilityType the driver unavailability type to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createDriverUnavailabilityType(DriverUnavailabilityTypeDI unavailabilityType) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_unavailabilityTypeDAO.saveDriverUnavailableType((DriverUnavailabilityType)unavailabilityType.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving driver unavailability type", "exception.DBException.savingDriverUnavailabilityType", e);
		}
	}
	/**
	 * 
	 * @param unavailabilityType the driver unavailability type to delete
	 * @throws UncompatibleConvertionException 
	 * @throws PeriodException 
	 */
	public void removeDriverUnavailabilityType(DriverUnavailabilityTypeDI unavailabilityType) throws PeriodException, UncompatibleConvertionException{
		_unavailabilityTypeDAO.removeDriverUnavailabilityType((DriverUnavailabilityType)unavailabilityType.convertToDomain());
	}
}
