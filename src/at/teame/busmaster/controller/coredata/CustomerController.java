package at.teame.busmaster.controller.coredata;

import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourCustomer;
import at.teame.busmaster.persistence.coachtour.CoachTourCustomerDAO;

public class CustomerController extends ContactDetailsTypeController {
	private CoachTourCustomerDAO _customerDAO = CoachTourCustomerDAO.getInstance();
	/**
	 * 
	 * @return the list of all customers
	 */
	public List<? extends CoachTourCustomerDI> getAllCustomer(){
		return _customerDAO.getAllCustomers();
	}
	/**
	 * 
	 * @param customer the customer to save
         * @throws at.teame.busmaster.domain.UncompatibleConvertionException
         * @throws at.gruppe2.exceptions.PeriodException
	 */
	public void saveCustomer(CoachTourCustomerDI customer) throws UncompatibleConvertionException, PeriodException{
		_customerDAO.saveCustomer((CoachTourCustomer)customer.convertToDomain());		
	}
	/**
	 * 
	 * @param customer the customer to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeCustomer(CoachTourCustomerDI customer) throws PeriodException, UncompatibleConvertionException{
		_customerDAO.removeCustomer((CoachTourCustomer)customer.convertToDomain());
	}
}
