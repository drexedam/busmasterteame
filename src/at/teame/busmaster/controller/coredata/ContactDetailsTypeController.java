package at.teame.busmaster.controller.coredata;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.Controller;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.ContactDetailsTypeDI;
import at.teame.busmaster.domain.persistencedata.contactdetails.ContactDetailsType;
import at.teame.busmaster.persistence.StateDAO;
import at.teame.busmaster.persistence.contactdetails.ContactDetailsTypeDAO;
import java.util.List;
import org.hibernate.HibernateException;

public class ContactDetailsTypeController extends Controller{

	private ContactDetailsTypeDAO _contactDAO = ContactDetailsTypeDAO.getInstance();
        private StateDAO _stateDAO = StateDAO.getInstance();
	/**
	 * 
	 * @return the list of all contact details types
	 */
	public List<? extends ContactDetailsTypeDI> getAllContactDetailsTypes(){
		return _contactDAO.getAllContactTypes();
	}
	/**
	 * 
	 * @param type the contact type to save
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 * @throws DBException
	 */
	public void createContactDetailsType(ContactDetailsTypeDI type) throws PeriodException, UncompatibleConvertionException, DBException{
		try{
			_contactDAO.saveContactType((ContactDetailsType)type.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving contact details type", "exception.DBException.savingContactDetailsType", e);
		}
	}
	/**
	 * 
	 * @param type the contact type to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeContactDetailsType(ContactDetailsTypeDI type) throws PeriodException, UncompatibleConvertionException{
		_contactDAO.removeContactType((ContactDetailsType)type.convertToDomain());
	}
        /**
         * 
         * @return the list of all states
         */
        public List<? extends StateDI> getAllStates(){
            return _stateDAO.getAllStates();
        }
}
