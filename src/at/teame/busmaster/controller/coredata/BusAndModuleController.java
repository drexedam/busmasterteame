package at.teame.busmaster.controller.coredata;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.Controller;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.bus.BusTypeDI;
import at.teame.busmaster.domain.domaininterface.bus.BusUnavailabilityDI;
import at.teame.busmaster.domain.domaininterface.bus.BusUnavailabilityTypeDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleTypeDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.bus.BusType;
import at.teame.busmaster.domain.persistencedata.bus.BusUnavailability;
import at.teame.busmaster.domain.persistencedata.bus.BusUnavailabilityType;
import at.teame.busmaster.domain.persistencedata.module.Module;
import at.teame.busmaster.domain.persistencedata.module.ModuleType;
import at.teame.busmaster.persistence.bus.BusDAO;
import at.teame.busmaster.persistence.bus.BusTypeDAO;
import at.teame.busmaster.persistence.bus.BusUnavailabilityDAO;
import at.teame.busmaster.persistence.bus.BusUnavailabilityTypeDAO;
import at.teame.busmaster.persistence.module.ModuleDAO;
import at.teame.busmaster.persistence.module.ModuleTypeDAO;

public class BusAndModuleController extends Controller {
	
	private ModuleDAO _moduleDAO = ModuleDAO.getInstance();
	private BusDAO _busDAO = BusDAO.getInstance();
	private BusTypeDAO _busTypeDAO = BusTypeDAO.getInstance();
	private ModuleTypeDAO _moduleTypeDAO = ModuleTypeDAO.getInstance();
	private BusUnavailabilityDAO _unavailabilityDAO = BusUnavailabilityDAO.getInstance();
	private BusUnavailabilityTypeDAO _unavailabilityTypeDAO = BusUnavailabilityTypeDAO.getInstance();
	
	/**
	 * 
	 * @return the list of all modules
	 */
	public List<? extends ModuleDI> getAllModules(){
		return _moduleDAO.getAllModules();
	}
	/**
	 * 
	 * @return the list of all modules which are not part of an Bus
	 */
    public List<? extends ModuleDI> getAllModulesNotInBus(){
		return _moduleDAO.getAllModulesNotInBus();
	}
	/**
	 * 
	 * @return the list of all buses
	 */
	public List<? extends BusDI> getAllBuses(){
		return _busDAO.getAllBusses();
	}
	/**
	 * 
	 * @return the list of all bus types
	 */
	public List<? extends BusTypeDI> getAllBusTypes(){
		return _busTypeDAO.getAllBusTypes();
	}
	/**
	 * 
	 * @return the list of all modules types
	 */
	public List<? extends ModuleTypeDI> getAllModuleTypes(){
		return _moduleTypeDAO.getAllModulTypes();
	}
	/**
	 * 
	 * @param bus for which the unavailabilities will return
	 * @return the list of all bus unavailabilities
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public List<? extends BusUnavailabilityDI> getAllBusUnavaiabilities(BusDI bus) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			return _unavailabilityDAO.getBusUnavailable((Bus)bus.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during getting all busunavailabilities", "exception.DBException.getBusUnavailabilities", e);
		}
	}
	/**
	 * 
	 * @return the list of all bus unavailability types
	 */
	public List<? extends BusUnavailabilityTypeDI> getAllBusUnavailabilityTypes(){
		return _unavailabilityTypeDAO.getAllBusUnavailableTypes();
	}
	/**
	 * 
	 * @param busSrc the bus to save
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createBus(BusDI busSrc) throws PeriodException, UncompatibleConvertionException{
		_busDAO.saveBus((Bus)busSrc.convertToDomain());
	}
	/**
	 * 
	 * @param bus the bus to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeBus(BusDI bus) throws PeriodException, UncompatibleConvertionException{
		_busDAO.removeBus((Bus)bus.convertToDomain());
	}
	/**
	 * 
	 * @param moduleSrc the module to save
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createModule(ModuleDI moduleSrc) throws PeriodException, UncompatibleConvertionException{
		_moduleDAO.saveModule((Module)moduleSrc.convertToDomain());
	}
	/**
	 * 
	 * @param module the module to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeModule(ModuleDI module) throws PeriodException, UncompatibleConvertionException{
		_moduleDAO.removeModule((Module)module.convertToDomain());
	}
	/**
	 * 
	 * @param busTypeSrc the bus type to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createBusType(BusTypeDI busTypeSrc) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_busTypeDAO.saveBusType((BusType)busTypeSrc.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving bustype", "exception.DBException.savingBusType", e);
		}
	}
	/**
	 * 
	 * @param busType the bus type to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeBusType(BusTypeDI busType) throws PeriodException, UncompatibleConvertionException{
		_busTypeDAO.removeBusType((BusType) busType.convertToDomain());
	}
	/**
	 * 
	 * @param moduleTypeSrc the module type to save
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 * @throws DBException
	 */
	public void createModuleType(ModuleTypeDI moduleTypeSrc) throws PeriodException, UncompatibleConvertionException, DBException {
		try{
		_moduleTypeDAO.saveModuleType((ModuleType)moduleTypeSrc.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving moduletype", "exception.DBException.savingModuleType", e);
		}	
	}
	/**
	 * 
	 * @param moduleType the module type to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeModuleType(ModuleTypeDI moduleType) throws PeriodException, UncompatibleConvertionException{
		_moduleTypeDAO.removeModuleType((ModuleType)moduleType.convertToDomain());
	}
	/**
	 * 
	 * @param busUnavailabilitySrc the bus unavailability to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createBusUnavailability(BusUnavailabilityDI busUnavailabilitySrc) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_unavailabilityDAO.saveBusUnvailable((BusUnavailability)busUnavailabilitySrc.convertToDomain());
			}
			catch(HibernateException e){
				throw new DBException("Problem during saving busunavailability", "exception.DBException.savingBusUnavailability", e);
			}
	}
	/**
	 * 
	 * @param unavailability the bus unavailability to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeBusUnavailability(BusUnavailabilityDI unavailability) throws PeriodException, UncompatibleConvertionException{
		_unavailabilityDAO.removeBusUnavailability((BusUnavailability)unavailability.convertToDomain());
	}
	/**
	 * 
	 * @param busUnavailabilityTypeSrc the bus unavailability type to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void createBusUnavailabilityType(BusUnavailabilityTypeDI busUnavailabilityTypeSrc) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_unavailabilityTypeDAO.saveBusUnavailableTyp((BusUnavailabilityType)busUnavailabilityTypeSrc.convertToDomain());
			}
			catch(HibernateException e){
				throw new DBException("Problem during saving busunavailabilitytype", "exception.DBException.savingBusUnavailabilityType", e);
			}
	}
	/**
	 * 
	 * @param unavailabilityType the bus unavailability type to delete
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void removeBusUnavailabilityType(BusUnavailabilityTypeDI unavailabilityType) throws PeriodException, UncompatibleConvertionException{
		_unavailabilityTypeDAO.removeBusUnavailabilityType((BusUnavailabilityType)unavailabilityType.convertToDomain());
	}
}
