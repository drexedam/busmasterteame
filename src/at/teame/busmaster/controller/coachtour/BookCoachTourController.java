package at.teame.busmaster.controller.coachtour;

import java.util.Date;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourOrder;

/**
 * 
 * @author Team E
 * @version 0.3
 */
public class BookCoachTourController extends CoachTourController{
	
	/**
	 * 
	 * @return a list of all bookable orders existing
	 * @throws PeriodException
	 */
	public List<? extends CoachTourOrderDI> getBookableCoachTourOrders() throws PeriodException{
		return getFutureCoachTourOrders(_orderDAO.getAllBookableOrders());
	}
	
	/**
	 * 
	 * @param toBook the order to book
	 * @throws UncompatibleConvertionException
	 */
	public void bookCoachTourOrder(CoachTourOrderDI toBook) throws UncompatibleConvertionException {
		if(!(toBook instanceof CoachTourOrder)) {
            throw new UncompatibleConvertionException("toBook is not of type CoachTourOrder", "exception.UncompatibleConvertionException.order");
		} else {
            CoachTourOrder order = (CoachTourOrder) toBook;
            order.setBookingDate(new Date());

            _orderDAO.saveOrder(order);
        }
	}

	/**
     * 
     * @param toBook the order to book
     * @return if all drivers and busses are available
     * @throws PeriodException
     */
	public boolean isBookable(CoachTourOrderDI toBook) throws PeriodException {
		for(CoachTourDI t: toBook.getTours()){
			if(!t.getBus().isAvailable(t.getSchedule().getStart(), t.getSchedule().getEnd(), toBook))
				return false;
			if(!t.getDriver().isAvailable(t.getSchedule().getStart(), t.getSchedule().getEnd(), toBook))
				return false;
			if(t.getAdditionalDriver()!=null){
				if(!t.getAdditionalDriver().isAvailable(t.getSchedule().getStart(), t.getSchedule().getEnd(), toBook))
					return false;
			}
		}
		return true;
	}
}
