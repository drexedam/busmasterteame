package at.teame.busmaster.controller.coachtour;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.HibernateException;

import at.teame.busmaster.controller.Controller;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourOrder;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourSchedule;
import at.teame.busmaster.persistence.StateDAO;
import at.teame.busmaster.persistence.bus.BusDAO;
import at.teame.busmaster.persistence.coachtour.CoachTourCustomerDAO;
import at.teame.busmaster.persistence.coachtour.CoachTourDAO;
import at.teame.busmaster.persistence.coachtour.CoachTourOrderDAO;
import at.teame.busmaster.persistence.driver.DriverDAO;
import at.teame.busmaster.persistence.module.ModuleDAO;

/**
 * @author Team E
 * @version 0.3
 */
public class CoachTourController extends Controller{
	
	private static final long WEEK_IN_MILLISEC = 604800000;
	
	protected CoachTourCustomerDAO _customerDAO = CoachTourCustomerDAO.getInstance();
	protected ModuleDAO _moduleDAO = ModuleDAO.getInstance();
	protected StateDAO _stateDAO = StateDAO.getInstance();
	protected CoachTourOrderDAO _orderDAO = CoachTourOrderDAO.getInstance();
	protected CoachTourDAO _tourDAO = CoachTourDAO.getInstance();
	protected DriverDAO _driverDAO = DriverDAO.getInstance();
	protected BusDAO _busDAO  = BusDAO.getInstance();
    /**
     *
     * @return the list of all Customers
     * @throws DBException if a problem while connecting or getting data from the database occurs
     */
	public List<? extends CoachTourCustomerDI> getAllCustomers() throws DBException{
		List<? extends CoachTourCustomerDI> result;
		try {
			result = _customerDAO.getAllCustomers();
		} catch (HibernateException e) {
			throw new DBException("Problem during getting all customers", "exception.DBException.getCustomers", e);
		}
		
		return result;
	}

    /**
     *
     * @return the list of all modules
     * @throws DBException if a problem while connecting or getting data from the database occurs
     */
	public List<? extends ModuleDI> getAllModules() throws DBException{
		List<? extends ModuleDI> result;
		
		try {
			result = _moduleDAO.getAllModules();
		} catch (HibernateException e) {
			throw new DBException("Problem during getting all modules", "exception.DBException.getModules", e);
		}
		return result;
	}

    /**
     *
     * @return the list of all States
     * @throws DBException if a problem while connecting or getting data from the database occurs
     */
	public List<? extends StateDI> getAllStates() throws DBException{
		List<? extends StateDI> result;
		try {
			result = _stateDAO.getAllStates();
		} catch (HibernateException e) {
			throw new DBException("Problem during getting all states", "exception.DBException.getStates", e);
		}
		return result;
	}

    /**
     *
     * @return list of all coach tour orders
     * @throws DBException if a problem while connecting or getting data from the database occurs
     */
	public List<? extends CoachTourOrderDI> getAllCoachTourOrders() throws DBException {
		List<? extends CoachTourOrderDI> result;
		try {
			result =  _orderDAO.getAllOrders();
		} catch (HibernateException e) {
			throw new DBException("Problem during getting all Coachtourorders", "exceptionDBException.getOrders", e);
		}
		return result;
	}

    /**
     *
     * @return a list of all coach tours that are not reserved but will happen in a weeks time
     */
	public List<? extends CoachTourOrderDI> reminder(){
		List<CoachTourOrderDI> result=new LinkedList<>();
		
		for(CoachTourOrderDI ct : getFutureCoachTourOrders(_orderDAO.getAllChangeableAndReservableOrders())) {
                    boolean added = false;
                    for (CoachTourScheduleDI cts : ct.getSchedules()) {
                        if(!added && cts.getStart().getTime()<= (new Date().getTime()+(WEEK_IN_MILLISEC*2))) {
                        		result.add(ct);
                                added = true;
                        }
                    }
                    
		}
		return result;
	}

    /**
     *
     * @param orders the list of orders to sort
     * @return a new list that contains only orders that have not started yet
     */
	protected List<? extends CoachTourOrderDI> getFutureCoachTourOrders(List<CoachTourOrder> orders) {
		List<CoachTourOrderDI> order = new LinkedList<>();
		Date current = new Date();
		for(CoachTourOrder cto : orders) {
			boolean alreadyStarted = false;
			for(CoachTourSchedule cts : cto.getSchedules()) {
				if(cts.getStart().before(current)) {
					alreadyStarted = true;
				}
			}
			if(!alreadyStarted) {
				order.add(cto);
			}
			
		}
		
		return order;
	}

}
