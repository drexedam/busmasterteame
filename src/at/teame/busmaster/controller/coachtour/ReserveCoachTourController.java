package at.teame.busmaster.controller.coachtour;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTour;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourOrder;
import at.teame.busmaster.domain.persistencedata.driver.Driver;

/**
 * 
 * @author Team E
 * @version 0.3
 */
public class ReserveCoachTourController extends CoachTourController {
	/**
	 * 
	 * @return a list with all reserveable orders existing
	 * @throws PeriodException
	 */
	public List<? extends CoachTourOrderDI> getReservableCoachTourOrders() throws PeriodException{
		return getFutureCoachTourOrders(_orderDAO.getAllChangeableAndReservableOrders());
	}
	
	/**
	 * 
	 * @param toReserve the order to reserve
	 * @param tours the tours to the order to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void reserveCoachTourOrder(CoachTourOrderDI toReserve, List<CoachTourDI> tours) throws DBException, PeriodException, UncompatibleConvertionException{
		if(!(toReserve instanceof CoachTourOrder)) {
			throw new UncompatibleConvertionException("toReserve not of type CoachTourOrder", "exception.UncompatibleConvertionException.order");
		} else {

            CoachTourOrder order = (CoachTourOrder) toReserve;

            order.setReservationDate(new Date());
            try {
                _orderDAO.saveOrder(order);

                for (CoachTourDI coachTourDI : tours) {
                    _tourDAO.saveCoachTour((CoachTour) coachTourDI.convertToDomain());
                }
            } catch (HibernateException e) {
                throw new DBException("Error while saving order and tours", "exception.DBException.savingOrderAndTours", e);
            }
        }
	}

	/**
	 * 
	 * @param order for which the drivers will be sorted
	 * @return a list of best economical sorted drivers
	 */
	public List<? extends DriverDI> getBusdriver(CoachTourOrderDI order){
		List<Driver> drivers = _driverDAO.getAllDrivers();
		//sort the list of drivers
		Collections.sort(drivers, new DriverComperator(order.getSchedules()));
		
		return drivers;
	}
	
	/**
	 * 
	 * @param order for which the busses will be sorted
	 * @return a list of best economical sorted busses
	 */
	public List<? extends BusDI> getBusses(CoachTourOrderDI order){
		List<Bus> busses = _busDAO.getAllBusses();
		//sort the list of busses
		Collections.sort(busses, new BusComperator(order.getSchedules()));
		
		return busses;
	}

    private class DriverComperator implements Comparator<Driver>{
		
		private List<? extends CoachTourScheduleDI> _schedules;
		
		public DriverComperator(List<? extends CoachTourScheduleDI> schedules){
			_schedules = schedules;
		}
		
		@Override
		public int compare(Driver o2, Driver o1) {
			try {
				boolean o1Avail = true;
				boolean o2Avail = true;
				
				//checks for both drivers if they are for all schedules available
				for (CoachTourScheduleDI schedule : _schedules) {
					if(!o1.isAvailable(schedule.getStart(), schedule.getEnd()))
						o1Avail=false;
					if(!o2.isAvailable(schedule.getStart(), schedule.getEnd()))
						o2Avail=false;
				}
								
				if(o1Avail && !o2Avail)
					return 1;
				if(!o1Avail && o2Avail)
					return -1;
				
				//compares the fare price of the drivers
				if(o1.getType().getFare()>o2.getType().getFare())
					return 1;
				if(o1.getType().getFare()<o2.getType().getFare())
					return -1;
				//the difference between the standing price of the drivers
				return o1.getType().getStandingPrice()-o2.getType().getStandingPrice();
					
				
			} catch (PeriodException e) {
				e.printStackTrace();
			}
			return -1;
		}
	}

	private class BusComperator implements Comparator<Bus>{
		
		private List<? extends CoachTourScheduleDI> _schedules;
		
		public BusComperator(List<? extends CoachTourScheduleDI> schedules){
			_schedules = schedules;
		}
	
		@Override
		public int compare(Bus o2, Bus o1) {
			try {
				boolean o1Avail = true;
				boolean o2Avail = true;
				
				//checks for both busses if they are for all schedules available
				for (CoachTourScheduleDI schedule : _schedules) {
					if(!o1.isAvailable(schedule.getStart(), schedule.getEnd()))
						o1Avail=false;
					if(!o2.isAvailable(schedule.getStart(), schedule.getEnd()))
						o2Avail=false;
				}
								
				if(o1Avail && !o2Avail)
					return 1;
				if(!o1Avail && o2Avail)
					return -1;
				
				//compares the base price of the busses
				if(o1.getType().getBasePrice()>o2.getType().getBasePrice())
					return 1;
				if(o1.getType().getBasePrice()<o2.getType().getBasePrice())
					return -1;
				
				//the difference between the kilometer price of the busses
				return o1.getType().getKMPrice()-o2.getType().getKMPrice();
				
			} catch (PeriodException e) {
				e.printStackTrace();
			}
			return -1;
		}
	}
}
