package at.teame.busmaster.controller.coachtour;

import java.util.LinkedList;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.persistencedata.coachtour.AdditionalExpense;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourOrder;
import at.teame.busmaster.domain.persistencedata.module.Module;
import at.teame.busmaster.domain.persistencedata.offer.Offer;
import at.teame.busmaster.persistence.bus.BusTypeDAO;
import at.teame.busmaster.persistence.driver.DriverTypeDAO;

/**
 * 
 * @author Team E
 * @version 0.3
 */
public class CreateCoachTourOfferController extends CoachTourController {

	/**
	 * 
	 * @param charges the additional charges to calculate with
	 * @param modules the modules to calculate with
	 * @param drivers the number of drivers
	 * @param traveltime estimated time on the road
	 * @param standingtime estimated time of waiting
	 * @param busses the number of busses
	 * @param distance the estimated distance to the destination
	 * @return a double with the calculated price
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
    public float calculatePrice(List<String[]> charges, List<List<ModuleDI>> modules, int drivers, int traveltime, int standingtime, int busses, int distance) throws PeriodException, UncompatibleConvertionException {
        List<AdditionalExpense> charg = new LinkedList<>();
        //parse the parameter to an additional expense object 
        for (String[] s : charges) {
            charg.add(new AdditionalExpense(s[0], Integer.parseInt(s[1])));
        }
        List<List<Module>> module = new LinkedList<>();
        //adds the modules from a List<List<>> to a List<>
        for (List<ModuleDI> m : modules) {
            List<Module> mo = new LinkedList<>();
            for (ModuleDI moduledi : m) {
                mo.add((Module)moduledi.convertToDomain());
            }
            module.add(mo);
        }
        Offer offer = new Offer(charg, module);
        return (((float)offer.getPrice(BusTypeDAO.getInstance().getAllBusTypes(), DriverTypeDAO.getInstance().getAllDriverTypes(), drivers, traveltime, standingtime, busses, distance))/100);
    }
	
    /**
     * 
     * @param orderSrc the order to save
     * @throws PeriodException
     * @throws UncompatibleConvertionException
     */
    public void createOrder(CoachTourOrderDI orderSrc) throws PeriodException, UncompatibleConvertionException
    {
        CoachTourOrder order = (CoachTourOrder) orderSrc.convertToDomain();
       _orderDAO.saveOrder(order);
    }
    
	
}
