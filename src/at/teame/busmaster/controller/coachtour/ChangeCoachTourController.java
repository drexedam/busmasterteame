package at.teame.busmaster.controller.coachtour;

import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;

/**
 * 
 * @author Team E
 * @version 0.3
 */
public class ChangeCoachTourController extends CoachTourController {
	
	/**
	 * 
	 * @return a list of all changeable orders existing
	 * @throws PeriodException
	 */
	public List<? extends CoachTourOrderDI> getChangeableCoachTourOrders() throws PeriodException{
		return getFutureCoachTourOrders(_orderDAO.getAllChangeableAndReservableOrders());
	}
	
	/**
	 * 
	 * @return a list of all manageable orders existing
	 * @throws PeriodException
	 */
	public List<? extends CoachTourOrderDI> getManageableCoachTourOrders() throws PeriodException{
		return getFutureCoachTourOrders(_orderDAO.getAllManageableOrders());
	}
	
}
