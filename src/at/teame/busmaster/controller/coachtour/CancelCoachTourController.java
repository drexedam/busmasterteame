package at.teame.busmaster.controller.coachtour;

import java.util.Date;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourOrder;

/**
 * 
 * @author Team E
 * @version 0.3
 */
public class CancelCoachTourController extends CoachTourController {
	
	/**
	 * 
	 * @return a list of all cancelable orders existing
	 * @throws PeriodException
	 */
	public List<? extends CoachTourOrderDI> getCancelableCoachTourOrders() throws PeriodException{
		return getFutureCoachTourOrders(_orderDAO.getAllCancelableOrders());
	}
	
	/**
	 * 
	 * @param toCancle the order to cancel
	 * @throws UncompatibleConvertionException
	 */
	public void cancleCoachTour(CoachTourOrderDI toCancle) throws UncompatibleConvertionException {
		if(!(toCancle instanceof CoachTourOrder)) {
			throw new UncompatibleConvertionException("Could not convert to CoachTourOrder", "exception.UncompatibleConvertionException.coachTourOrder");
		}
		
		CoachTourOrder order = (CoachTourOrder) toCancle;
		order.setCancelDate(new Date());
		
		_orderDAO.saveOrder(order);
	}
}
