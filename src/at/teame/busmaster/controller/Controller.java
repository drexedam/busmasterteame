package at.teame.busmaster.controller;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.Updateable;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.persistence.AnyDAO;

public class Controller {
	
	private AnyDAO _dangerousDAO = AnyDAO.getInstance();
	
	/**
    *
    * @param toUpdate defines the DI object that should be updated
    * @param guiObj holds the object with the new information to update
    * @throws UncompatibleConvertionException if a problem while connecting or getting data from the database occurs
    * @throws NotUpdateableException if the DI is not updatable
    * @throws PeriodException if the start date is later than the end date
    */
	public void update(DI toUpdate, DI guiObj) throws UncompatibleConvertionException, NotUpdateableException, PeriodException {
		if(!(toUpdate instanceof Updateable)) {
			throw new NotUpdateableException("You can not update this object", "exception.NotUpdateableException.update");
		}
		
		((Updateable) toUpdate).update(guiObj);
		
		_dangerousDAO.saveANY(toUpdate);
	}
}
