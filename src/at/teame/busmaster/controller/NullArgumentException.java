package at.teame.busmaster.controller;

import at.gruppe2.exceptions.BusmasterException;

public class NullArgumentException extends Exception implements
        BusmasterException {

	private static final long serialVersionUID = 1L;
	private String _bundleMsg;
	
	public NullArgumentException(String message, String bundleMessage) {
		super(message);
		_bundleMsg = bundleMessage;
	}
	
	public NullArgumentException(String bundleMessage) {
		_bundleMsg = bundleMessage;
	}
	
	@Override
	public String getBundleMessage() {
		return _bundleMsg;
	}

}
