/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.controller;

/**
 * 
 * @author Team E
 * @version 0.3
 */
import at.teame.busmaster.domain.domaininterface.user.UserDI;
import at.teame.busmaster.domain.persistencedata.user.User;
import at.teame.busmaster.persistence.user.UserDAO;

/**
 *
 * @author Patrick
 */
public class LoginController {
    private UserDAO _userDAO = UserDAO.getInstance();
    
    /**
     * 
     * @param username the name to compare for login
     * @param password the password to compare for login
     * @return if login was successful return true; default return value is false
     */
    public boolean login (String username, String password) {
        boolean success = false;

        User u = _userDAO.getUser(username, password);
        success = u != null;
        
        return success;
    }
    /**
     * 
     * @param username the name to compare for login
     * @param password the password to compare for login
     * @return the user if username and password matches with one
     */
    public UserDI driverLogin(String username, String password){
    	return _userDAO.getUser(username, password);
    }
}
