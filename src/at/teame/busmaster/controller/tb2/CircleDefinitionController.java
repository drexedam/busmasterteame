package at.teame.busmaster.controller.tb2;

import java.util.LinkedList;
import java.util.List;

import at.gruppe2.dao.bus.IBusDAO;
import at.gruppe2.dao.publicbustransport.IPublicBusTransportDAO;
import at.gruppe2.dao.schedule.IDayTypeDAO;
import at.gruppe2.domain.IFactory;
import at.gruppe2.domain.bus.IBus;
import at.gruppe2.domain.publicbustransport.IPublicBusTransport;
import at.gruppe2.domain.schedule.ICircleDefinition;
import at.gruppe2.domain.schedule.IDayType;
import at.gruppe2.exceptions.PeriodException;
import at.gruppe2.exceptions.UnsupportedClassException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.guidata.tb2.CircleDefinitionDummy;
import at.teame.busmaster.persistence.Factory;
import at.teame.busmaster.persistence.bus.BusDAO;
import at.teame.busmaster.persistence.publicbustransport.PublicBusTransportDAO;
import at.teame.busmaster.persistence.schedule.CircleDefinitionDAO;
import at.teame.busmaster.persistence.schedule.DayTypeDAO;

public class CircleDefinitionController {
	private IBusDAO _busDAO = BusDAO.getInstance();
	private IDayTypeDAO _dayTypeDAO= DayTypeDAO.getInstance();
	private IPublicBusTransportDAO _transportDAO = PublicBusTransportDAO.getInstance();
	private IFactory _fac = new Factory();
	
	public List<? extends IBus> getAllBusses(){
		return _busDAO.getAllBusses();
	}
	
	public List<? extends IDayType> getAllDayTypes(){
		return _dayTypeDAO.getAllDayTypes();
	}
	
	public List<? extends IPublicBusTransport> getPublicBusTransports(IDayType dayType){
		List<IPublicBusTransport> transports= new LinkedList<>();
		for(IPublicBusTransport t :_transportDAO.getAllTransports()){
			if(t.getDayType().equals(dayType))
				transports.add(t);
		}
		return transports;
	}
	
	public void saveCircleDefinition(CircleDefinitionDummy circleDefinition) throws PeriodException, UncompatibleConvertionException{
		ICircleDefinition circleDef = null;
		try {
			circleDef = (ICircleDefinition) _fac.createEmpty(ICircleDefinition.class);
		} catch (UnsupportedClassException e) {
			// one of the exchanged interfaces. It need to be supported...
			e.printStackTrace();
		}
		circleDef.setBus(circleDefinition.getBus());
		circleDef.setDayType(circleDefinition.getDayType());
		circleDef.setSpecification(circleDefinition.getSpecification());
		circleDef.setTransports(circleDefinition.getPublicTransports());

		for(IPublicBusTransport trans :circleDefinition.getPublicTransports()){
			trans.setCircleDefinition(circleDef);
		}
        IBus b = circleDefinition.getBus();
        if (b != null) {
            b.addCircleDefinition(circleDef);
        }
        CircleDefinitionDAO.getInstance().saveCircleDefinition(circleDef);
	}
}
