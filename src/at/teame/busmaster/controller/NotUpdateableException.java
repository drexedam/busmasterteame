package at.teame.busmaster.controller;

import at.gruppe2.exceptions.BusmasterException;

public class NotUpdateableException extends Exception implements
        BusmasterException {
	private static final long serialVersionUID = 1L;

	private String _bundleMsg;
	
	public NotUpdateableException(String message, String bundleMessage) {
		super(message);
		_bundleMsg = bundleMessage;
	}
	
	public NotUpdateableException(String bundleMessage) {
		_bundleMsg = bundleMessage;
	}
	
	@Override
	public String getBundleMessage() {
		return _bundleMsg;
	}

}
