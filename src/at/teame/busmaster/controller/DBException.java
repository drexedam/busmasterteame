package at.teame.busmaster.controller;

import at.gruppe2.exceptions.BusmasterException;

public class DBException extends Exception implements BusmasterException {

	private static final long serialVersionUID = 1L;
	private String _bundleMsg;
	
	public DBException(String message, String bundleMessage, Throwable reason) {
		super(message, reason);
		_bundleMsg = bundleMessage;
	}
	
	public DBException(String message, String bundleMessage) {
		super(message);
		_bundleMsg = bundleMessage;
	}
	
	public DBException(String bundleMessage) {
		_bundleMsg = bundleMessage;
	}
	
	@Override
	public String getBundleMessage() {
		return _bundleMsg;
	}

}
