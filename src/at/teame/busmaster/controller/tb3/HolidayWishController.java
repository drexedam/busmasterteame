package at.teame.busmaster.controller.tb3;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.HolidayWishDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTour;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.HolidayWish;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftSchedule;
import at.teame.busmaster.persistence.coachtour.CoachTourDAO;
import at.teame.busmaster.persistence.driver.DriverDAO;
import at.teame.busmaster.persistence.driver.HolidayWishDAO;
import at.teame.busmaster.persistence.schedule.DriverShiftScheduleDAO;

public class HolidayWishController {
	
	private HolidayWishDAO _wishDAO = HolidayWishDAO.getInstance();
	
	/**
	 * 
	 * @param driver for whom the wishes will return
	 * @return a list of all holiday wishes
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public List<? extends HolidayWishDI> getAllHolidayWishes(DriverDI driver) throws DBException, PeriodException, UncompatibleConvertionException{
		try{	
			return _wishDAO.getWishes((Driver)driver.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during getting all holiday wishes", "exception.DBException.getHolidayWishes", e);
		}
	}
	/**
	 * 
	 * @param wish to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void saveHolidayWish(HolidayWishDI wish) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_wishDAO.saveWish((HolidayWish)wish.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving holiday wish", "exception.DBException.savingHolidayWish", e);
		}
	}
	/**
	 * 
	 * @param id of the wish to delete
	 */
	public void removeHolidayWish(int id){
		_wishDAO.removeWish(id);
	}
	/**
	 * 
	 * @param start of the holiday wish
	 * @param end of the holiday wish
	 * @return if enough resources are available at this time period
	 */
	public boolean enoughResourcesAvailable(Date start, Date end) {
		if(start.before(new Date()) || end.before(new Date())) {
			return false;
		}
		
		if(end.before(start)) {
			return false;
		}
		
		
		int dif=(int) ((end.getTime()-start.getTime())/(1000*60*60*24));
		
		int freeDrivers;
		int neededDrivers;
		List<CoachTour> tours = CoachTourDAO.getInstance().getAllTours();
		List<Driver> ds = DriverDAO.getInstance().getAllDrivers();
		List<DriverShiftSchedule> shifts = DriverShiftScheduleDAO.getInstance().getAllDriverShifts();
		
		for(int i=0; i<=dif; i++){
				neededDrivers=1;
				freeDrivers=0;
				boolean free = true;
				Date check = new Date(start.getTime()+(i*1000*60*60*24));
				
				for(Driver d : ds) {
				
			
					List<HolidayWish> wishes = HolidayWishDAO.getInstance().getWishes(d);
			
			
					for(HolidayWish w : wishes){
						if(w.getStart().before(check) && w.getEnd().after(check)) {
							free = false;
						}
					}
					if(free)
						freeDrivers++;
				}
				
				for(CoachTour t : tours){
					if(t.getSchedule().getStart().before(check)&& t.getSchedule().getEnd().after(check))
						neededDrivers += t.getAdditionalDriver() == null ? 1 : 2;
				}
				
				for(DriverShiftSchedule s : shifts){
					if(s.getDate().getTime() == check.getTime()){
						neededDrivers++;
					}
				}
				
				if(freeDrivers<=neededDrivers)
					return false;
			}
		
		return true;
	}
}
