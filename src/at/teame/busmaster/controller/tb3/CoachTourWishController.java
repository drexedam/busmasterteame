package at.teame.busmaster.controller.tb3;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.CoachTourWishDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.persistencedata.driver.CoachTourWish;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.persistence.coachtour.CoachTourScheduleDAO;
import at.teame.busmaster.persistence.driver.CoachTourWishDAO;

public class CoachTourWishController {
	
    private static CoachTourWishController _instance = new CoachTourWishController();
    public static CoachTourWishController getInstance()
    {
        return _instance;
    }
    
    public CoachTourWishController() {}
    
	private CoachTourWishDAO _wishDAO = CoachTourWishDAO.getInstance();
	private CoachTourScheduleDAO _scheduleDAO = CoachTourScheduleDAO.getInstance();
	
	/**
	 * 
	 * @param driver for whom the coach tour wishes will return
	 * @return a list of all coach tour wishes
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public List<? extends CoachTourWishDI> getAllCoachtourWishes(DriverDI driver) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			return _wishDAO.getWishes((Driver)driver.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during getting all coach tour wishes", "exception.DBException.getCoachtourWishes", e);
		}
	}
	/**
	 * 
	 * @return a list of all schedules which are not planed yet
	 */
	public List<? extends CoachTourScheduleDI> getAllNotPlanedSchedules(){
		return _scheduleDAO.getNotPlanedSchedules();
	}
	/**
	 * 
	 * @param wish to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void saveCoachtourWish(CoachTourWishDI wish) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_wishDAO.saveWish((CoachTourWish)wish.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving coach tour wish", "exception.DBException.savingCoachtourWish", e);
		}
	}
	/**
	 * 
	 * @param id of the wish to delete
	 */
	public void removeCoachtourWish(int id){
		_wishDAO.removeWish(id);
	}

    public CoachTourScheduleDI getCoachTourScheduleByID(int id)
    {
        return _scheduleDAO.getScheduleByID(id);
    }
}
