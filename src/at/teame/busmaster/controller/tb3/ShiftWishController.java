package at.teame.busmaster.controller.tb3;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.ShiftWishDI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftScheduleDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTour;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.HolidayWish;
import at.teame.busmaster.domain.persistencedata.driver.ShiftWish;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftSchedule;
import at.teame.busmaster.persistence.coachtour.CoachTourDAO;
import at.teame.busmaster.persistence.driver.DriverDAO;
import at.teame.busmaster.persistence.driver.HolidayWishDAO;
import at.teame.busmaster.persistence.driver.ShiftWishDAO;
import at.teame.busmaster.persistence.schedule.DriverShiftScheduleDAO;

public class ShiftWishController {
	
	private ShiftWishDAO _wishDAO = ShiftWishDAO.getInstance();
	private DriverShiftScheduleDAO _scheduleDAO = DriverShiftScheduleDAO.getInstance();
	
	/**
	 * 
	 * @param driver for whom all wishes are loaded
	 * @return a list of all shift wishes
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public List<? extends ShiftWishDI> getAllShiftWishes(DriverDI driver) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			return _wishDAO.getWishes((Driver)driver.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during getting all shift wishes", "exception.DBException.getShiftWishes", e);
		}
	}
	
	/**
	 * 
	 * @param driver for whom all shifts are loaded
	 * @param start of week
	 * @param end of week
	 * @return list of all shifts
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public List<? extends DriverShiftScheduleDI> getAllSchedules(DriverDI driver, Date start, Date end) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			List<DriverShiftScheduleDI> res = new LinkedList<>();
			List<ShiftWish> sw = _wishDAO.getWishes((Driver)driver.convertToDomain());
			boolean contains = false;
			for(DriverShiftScheduleDI dss : _scheduleDAO.getShifts((Driver) driver.convertToDomain(), start, end)) {
				contains = false;
				for(ShiftWish s : sw) {
					if(dss.equals(s.getShift())) {
						contains = true;
						break;
					}
				}
				if(!contains)
					res.add(dss);
			}
			return res; 
		}
		catch(HibernateException e){
			throw new DBException("Problem during getting all shifts", "exception.DBException.getShifts", e);
		}
	}
	/**
	 * 
	 * @param wish to save
	 * @throws DBException
	 * @throws PeriodException
	 * @throws UncompatibleConvertionException
	 */
	public void saveShiftWish(ShiftWishDI wish) throws DBException, PeriodException, UncompatibleConvertionException{
		try{
			_wishDAO.saveWish((ShiftWish)wish.convertToDomain());
		}
		catch(HibernateException e){
			throw new DBException("Problem during saving shift wish", "exception.DBException.savingShiftWish", e);
		}
	}
	/**
	 * 
	 * @param id of the wish to delete
	 */
	public void removeShiftWish(int id){
		_wishDAO.removeWish(id);
	}
	
	public boolean isPossible(DriverShiftScheduleDI temp){
		
		
		
		
		int freeDrivers;
		int neededDrivers;
		List<CoachTour> tours = CoachTourDAO.getInstance().getAllTours();
		List<Driver> ds = DriverDAO.getInstance().getAllDrivers();
		List<DriverShiftSchedule> shifts = DriverShiftScheduleDAO.getInstance().getAllDriverShifts();
		
			neededDrivers=1;
			freeDrivers=0;
			boolean free = true;
			Date check = temp.getDate();
			
			for(Driver d : ds) {
		
				List<HolidayWish> wishes = HolidayWishDAO.getInstance().getWishes(d);
		
		
				for(HolidayWish w : wishes){
					if(w.getStart().before(check) && w.getEnd().after(check)) {
						free = false;
					}
				}
				if(free)
					freeDrivers++;
			}
			
			for(CoachTour t : tours){
				if(t.getSchedule().getStart().getTime() == check.getTime())
					neededDrivers += t.getAdditionalDriver() == null ? 1 : 2;
			}
			
			for(DriverShiftSchedule s : shifts){
				if(s.getDate().getTime() == check.getTime()){
					neededDrivers++;
				}
			}
			
			if(freeDrivers<=neededDrivers)
				return false;
		
		return true;
	}
	
	public DriverShiftScheduleDI getShiftByID(int id) {
		return (DriverShiftScheduleDI) _scheduleDAO.getDriverShiftSchedule(id);
	}
}
