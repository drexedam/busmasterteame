package at.teame.busmaster.persistence;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

@SuppressWarnings("deprecation")
public abstract class Persistence {

	protected Session _session;
	protected Transaction _tx;
	protected Query _query;
	protected Criteria _criteria;
        protected static SessionFactory _sessionFactory;
        private static ServiceRegistry _serviceRegistry;

    public Persistence()
    {
        openSession();
    }
	
	protected void openSession(){
		if(_session==null){
            if(_sessionFactory == null)
            {
                Configuration configuration = new Configuration();
                configuration.configure();
                ServiceRegistryBuilder srb = new ServiceRegistryBuilder();
                srb.applySettings(configuration.getProperties());
                _serviceRegistry = srb.build();
                _sessionFactory = configuration.buildSessionFactory(_serviceRegistry);
            }
            
            _session = _sessionFactory.openSession();
		}
	}
    
	public void closeSession(){
		_session.close();
        _session = null;
	}

}
