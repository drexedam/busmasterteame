package at.teame.busmaster.persistence;

import at.gruppe2.domain.IFactory;
import at.gruppe2.domain.bus.IBus;
import at.gruppe2.domain.bus.IBusType;
import at.gruppe2.domain.bus.IBusUnavailability;
import at.gruppe2.domain.bus.IBusUnavailabilityType;
import at.gruppe2.domain.driver.IDriver;
import at.gruppe2.domain.driver.IDriverType;
import at.gruppe2.domain.driver.IDriverUnavailability;
import at.gruppe2.domain.driver.IDriverUnavailabilityType;
import at.gruppe2.domain.publicbustransport.IPublicBusTransport;
import at.gruppe2.domain.publicbustransport.IRoute;
import at.gruppe2.domain.publicbustransport.IRouteSection;
import at.gruppe2.domain.publicbustransport.IStation;
import at.gruppe2.domain.schedule.ICircleDefinition;
import at.gruppe2.domain.schedule.ICircleSchedule;
import at.gruppe2.domain.schedule.IDayType;
import at.gruppe2.domain.schedule.IDriverShiftDefinition;
import at.gruppe2.domain.schedule.IDriverShiftSchedule;
import at.gruppe2.exceptions.UnsupportedClassException;
import at.teame.busmaster.bundles.Msg;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.bus.BusType;
import at.teame.busmaster.domain.persistencedata.bus.BusUnavailability;
import at.teame.busmaster.domain.persistencedata.bus.BusUnavailabilityType;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.DriverType;
import at.teame.busmaster.domain.persistencedata.driver.DriverUnavailability;
import at.teame.busmaster.domain.persistencedata.driver.DriverUnavailabilityType;
import at.teame.busmaster.domain.persistencedata.publicbustransport.PublicBusTransport;
import at.teame.busmaster.domain.persistencedata.publicbustransport.Route;
import at.teame.busmaster.domain.persistencedata.publicbustransport.RouteSection;
import at.teame.busmaster.domain.persistencedata.publicbustransport.Station;
import at.teame.busmaster.domain.persistencedata.schedule.CircleDefinition;
import at.teame.busmaster.domain.persistencedata.schedule.CircleSchedule;
import at.teame.busmaster.domain.persistencedata.schedule.DayType;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftDefinition;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftSchedule;

public class Factory implements IFactory {

	@Override
	public Object createEmpty(@SuppressWarnings("rawtypes") Class c) throws UnsupportedClassException {
		if(c == IBus.class)
		{
			return new Bus();
		} else if(c == ICircleDefinition.class) {
			return new CircleDefinition();
		} else if(c == IBusType.class) {
			return new BusType();
		} else if(c == IBusUnavailability.class) {
			return new BusUnavailability();
		} else if(c == IBusUnavailabilityType.class) {
			return new BusUnavailabilityType();
		} else if(c == IDriver.class) {
			return new Driver();
		} else if(c == IDriverType.class) {
			return new DriverType();
		} else if(c == IDriverUnavailability.class) {
			return new DriverUnavailability();
		} else if(c == IDriverUnavailabilityType.class) {
			return new DriverUnavailabilityType();
		} else if(c == IPublicBusTransport.class) {
			return new PublicBusTransport();
		} else if(c == IRoute.class) {
			return new Route();
		} else if(c == IRouteSection.class) {
			return new RouteSection();
		} else if(c == IStation.class) {
			return new Station();
		} else if (c == ICircleSchedule.class) {
			return new CircleSchedule();
		} else if (c == IDayType.class) {
			return new DayType();
		} else if (c == IDriverShiftDefinition.class) {
			return new DriverShiftDefinition();
		} else if (c == IDriverShiftSchedule.class) {
			return new DriverShiftSchedule();
		} 
		
		Object[] param = {c};
		throw new UnsupportedClassException(c + " is not supported by this factory.", Msg.getString("exception.Factory.initialize", param));

	}

}
