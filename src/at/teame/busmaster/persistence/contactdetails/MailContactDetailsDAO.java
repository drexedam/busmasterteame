package at.teame.busmaster.persistence.contactdetails;

import java.util.List;

import at.teame.busmaster.domain.persistencedata.contactdetails.MailContactDetails;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class MailContactDetailsDAO extends Persistence{
	
	private static final MailContactDetailsDAO _instance = new MailContactDetailsDAO();
	
	private MailContactDetailsDAO()
    {
        super();
    }
	
	public static MailContactDetailsDAO getInstance() {
		return _instance;
	}
	
	public void saveMailContact(MailContactDetails mailContact)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(mailContact);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<MailContactDetails> getAllMailContacts()throws HibernateException{
		List<MailContactDetails> contacts = null;
        _query= _session.createQuery("FROM MailContactDetails");

        contacts = _query.list();
			
		return contacts;
	}

}
