package at.teame.busmaster.persistence.contactdetails;

import java.util.List;

import org.hibernate.HibernateException;

import at.teame.busmaster.domain.persistencedata.contactdetails.ContactDetailsType;
import at.teame.busmaster.persistence.Persistence;

public class ContactDetailsTypeDAO extends Persistence{
	
	private static final ContactDetailsTypeDAO _instance = new ContactDetailsTypeDAO();
	
	private ContactDetailsTypeDAO()
    {
        super();
    }
	
	public static ContactDetailsTypeDAO getInstance() {
		return _instance;
	}
	
	public void saveContactType(ContactDetailsType type)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(type);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<ContactDetailsType> getAllContactTypes()throws HibernateException{
		List<ContactDetailsType> types = null;
        _query= _session.createQuery("FROM ContactDetailsType");
        types = _query.list();
		return types;
	}
	
	public void removeContactType(ContactDetailsType type){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(type);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
}
