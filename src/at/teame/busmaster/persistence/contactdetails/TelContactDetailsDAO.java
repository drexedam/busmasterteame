package at.teame.busmaster.persistence.contactdetails;

import java.util.List;

import at.teame.busmaster.domain.persistencedata.contactdetails.TelContactDetails;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class TelContactDetailsDAO extends Persistence{
	
	private static final TelContactDetailsDAO _instance = new TelContactDetailsDAO();
	
	private TelContactDetailsDAO()
    {
        super();
    }
	
	public static TelContactDetailsDAO getInstance() {
		return _instance;
	}
	
	public void savePhoneContact(TelContactDetails phoneContact)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(phoneContact);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<TelContactDetails> getAllPhoneContacts()throws HibernateException{
		List<TelContactDetails> contacts = null;
		_query= _session.createQuery("FROM TelContactDetails");
			
        contacts = _query.list();
        
		return contacts;
	}
}
