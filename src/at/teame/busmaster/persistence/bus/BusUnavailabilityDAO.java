package at.teame.busmaster.persistence.bus;

import java.util.List;

import at.gruppe2.dao.bus.IBusUnavailabilityDAO;
import at.gruppe2.domain.bus.IBus;
import at.gruppe2.domain.bus.IBusUnavailability;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.bus.BusUnavailability;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class BusUnavailabilityDAO extends Persistence implements IBusUnavailabilityDAO{
	
	private static final BusUnavailabilityDAO _instance = new BusUnavailabilityDAO();
	
	private BusUnavailabilityDAO()
    {
        super();
    }
	
	public static BusUnavailabilityDAO getInstance() {
		return _instance;
	}
	
	public void saveBusUnvailable(BusUnavailability busUnavailable)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(busUnavailable);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<BusUnavailability> getBusUnavailable(Bus bus)throws HibernateException{
		List<BusUnavailability> notAvailable;
		
		notAvailable = _session.createCriteria(BusUnavailability.class).list();
		
		return notAvailable;
	}
	
	public void removeBusUnavailability(BusUnavailability busUnavailability){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(busUnavailability);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@Override
	public void saveBusUnvailable(IBusUnavailability busUnavailable)
			throws HibernateException {
		saveBusUnvailable((BusUnavailability) busUnavailable);
	}

	@Override
	public List<? extends IBusUnavailability> getBusUnavailable(IBus bus)
			throws HibernateException {
		return getBusUnavailable((Bus) bus);
	}


}
