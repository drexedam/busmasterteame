package at.teame.busmaster.persistence.bus;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.dao.bus.IBusDAO;
import at.gruppe2.domain.bus.IBus;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.persistence.Persistence;

public class BusDAO extends Persistence implements IBusDAO {
	private static final BusDAO _instance = new BusDAO();
	
	protected BusDAO()
    {
        super();
    }
	
	public static BusDAO getInstance() {
		return _instance;
	}
	
	public void saveBus(Bus bus)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(bus);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	public void removeBus(Bus bus){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(bus);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
	
	@SuppressWarnings("unchecked")
	public List<Bus> getAllBusses()throws HibernateException{
		List<Bus> busses;
		
		busses = _session.createCriteria(Bus.class).list();
		
		return busses;
	}

	@Override
	public void saveBus(IBus bus) throws HibernateException {
		saveBus((Bus) bus);
	}
}
