package at.teame.busmaster.persistence.bus;

import java.util.List;

import at.gruppe2.dao.bus.IBusTypeDAO;
import at.gruppe2.domain.bus.IBusType;
import at.teame.busmaster.domain.persistencedata.bus.BusType;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class BusTypeDAO extends Persistence implements IBusTypeDAO{
	
	private static final BusTypeDAO _instance = new BusTypeDAO();
	
	private BusTypeDAO()
    {
        super();
    }
	
	public static BusTypeDAO getInstance() {
		return _instance;
	}
	
	public void saveBusType(BusType busType)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(busType);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<BusType> getAllBusTypes()throws HibernateException{
		List<BusType> types;
		
		types= _session.createCriteria(BusType.class).list();
		
		return types;
	}
	
	public void removeBusType(BusType busType){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(busType);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }	
	}

	@Override
	public void saveBusType(IBusType busType) throws HibernateException {
		saveBusType((BusType) busType);
	}


}
