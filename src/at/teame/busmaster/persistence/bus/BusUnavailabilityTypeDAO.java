package at.teame.busmaster.persistence.bus;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.dao.bus.IBusUnavailabilityTypeDAO;
import at.gruppe2.domain.bus.IBusUnavailabilityType;
import at.teame.busmaster.domain.persistencedata.bus.BusUnavailabilityType;
import at.teame.busmaster.persistence.Persistence;

public class BusUnavailabilityTypeDAO extends Persistence implements IBusUnavailabilityTypeDAO {
	
	private static final BusUnavailabilityTypeDAO _instance = new BusUnavailabilityTypeDAO();
	
	private BusUnavailabilityTypeDAO()
    {
        super();
    }
	
	public static BusUnavailabilityTypeDAO getInstance() {
		return _instance;
	}
	
	public void saveBusUnavailableTyp(BusUnavailabilityType busType)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(busType);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<BusUnavailabilityType> getAllBusUnavailableTypes()throws HibernateException{
		List<BusUnavailabilityType> types;
		
		types = _session.createCriteria(BusUnavailabilityType.class).list();
		
		return types;
	}
	
	public void removeBusUnavailabilityType(BusUnavailabilityType busUnavailabilityType){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(busUnavailabilityType);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@Override
	public void saveBusUnavailableTyp(IBusUnavailabilityType busType)
			throws HibernateException {
		saveBusUnavailableTyp((BusUnavailabilityType) busType);
	}

}
