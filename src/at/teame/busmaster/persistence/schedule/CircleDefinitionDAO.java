package at.teame.busmaster.persistence.schedule;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.dao.schedule.ICircleDefinitionDAO;
import at.gruppe2.domain.schedule.ICircleDefinition;
import at.teame.busmaster.domain.persistencedata.schedule.CircleDefinition;
import at.teame.busmaster.persistence.Persistence;

public class CircleDefinitionDAO extends Persistence implements ICircleDefinitionDAO {
	
	private static final CircleDefinitionDAO _instance = new CircleDefinitionDAO();
	
	private CircleDefinitionDAO()
    {
        super();
    }
	
	public static CircleDefinitionDAO getInstance() {
		return _instance;
	}
	
	public void saveCircleDefinition(CircleDefinition definition)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.merge(definition);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
    @Override
	public List<CircleDefinition> getAllCircleDefinitions()throws HibernateException{
		List<CircleDefinition> def;
		
		def = _session.createCriteria(CircleDefinition.class).list();
		
		return def;
	}

	@Override
	public void saveCircleDefinition(ICircleDefinition definition)
			throws HibernateException {
		saveCircleDefinition((CircleDefinition)definition);
	}

	@Override
	public ICircleDefinition getCircleDefinition(int id)
			throws HibernateException {
		return (ICircleDefinition) _session.load(CircleDefinition.class, id);
	}

}
