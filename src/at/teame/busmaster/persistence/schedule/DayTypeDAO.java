package at.teame.busmaster.persistence.schedule;

import java.util.List;

import at.gruppe2.dao.schedule.IDayTypeDAO;
import at.gruppe2.domain.schedule.IDayType;
import at.teame.busmaster.domain.persistencedata.schedule.DayType;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class DayTypeDAO extends Persistence implements IDayTypeDAO{
	
	private static final DayTypeDAO _instance = new DayTypeDAO();
	
	private DayTypeDAO()
    {
        super();
    }
	
	public static DayTypeDAO getInstance() {
		return _instance;
	}

	public void saveDayType(DayType dayType)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(dayType);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
    @Override
	public List<DayType> getAllDayTypes()throws HibernateException {
		List<DayType> types;
		
		types = _session.createCriteria(DayType.class).list();

		return types;
	}

	@Override
	public void saveDayType(IDayType dayType) throws HibernateException {
		saveDayType((DayType) dayType);
	}

	@Override
	public void removeDayType(IDayType dayType) throws HibernateException {
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(dayType);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
		
	}
}
