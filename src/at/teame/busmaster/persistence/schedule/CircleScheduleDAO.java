package at.teame.busmaster.persistence.schedule;

import java.util.List;

import at.gruppe2.dao.schedule.ICircleScheduleDAO;
import at.gruppe2.domain.schedule.ICircleSchedule;
import at.teame.busmaster.domain.persistencedata.schedule.CircleSchedule;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class CircleScheduleDAO extends Persistence implements ICircleScheduleDAO {
	
	private static final CircleScheduleDAO _instance = new CircleScheduleDAO();
	
	private CircleScheduleDAO()
    {
        super();
    }
	
	public static CircleScheduleDAO getInstance() {
		return _instance;
	}

	public void saveCirculation(CircleSchedule circulation)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(circulation);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
    @Override
	public List<CircleSchedule> getAllCirculations()throws HibernateException{
		List<CircleSchedule> circ;
		
		circ = _session.createCriteria(CircleSchedule.class).list();
		
		return circ;
	}

	@Override
	public void saveCirculation(ICircleSchedule circulation)
			throws HibernateException {
		saveCirculation((CircleSchedule) circulation);
	}

}
