package at.teame.busmaster.persistence.schedule;

import java.util.List;

import at.gruppe2.dao.schedule.IDriverShiftDefinitionDAO;
import at.gruppe2.domain.schedule.IDriverShiftDefinition;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftDefinition;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class DriverShiftDefinitionDAO extends Persistence implements IDriverShiftDefinitionDAO {
	
	private static final DriverShiftDefinitionDAO _instance = new DriverShiftDefinitionDAO();
	
	private DriverShiftDefinitionDAO()
    {
        super();
    }
	
	public static DriverShiftDefinitionDAO getInstance() {
		return _instance;
	}
	
	public void saveDriverShiftDefinition(DriverShiftDefinition driverShiftDef)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.merge(driverShiftDef);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
    @Override
	public List<DriverShiftDefinition> getAllDriverShiftDefinition()throws HibernateException{
		List<DriverShiftDefinition> shiftDef;
		
		shiftDef = _session.createCriteria(DriverShiftDefinition.class).list();
		
		return shiftDef;
	}

	@Override
	public void saveDriverShiftDefinition(IDriverShiftDefinition driverShiftDef)
			throws HibernateException {
		saveDriverShiftDefinition((DriverShiftDefinition) driverShiftDef);
	}

	@Override
	public IDriverShiftDefinition getDriverShiftDefinition(int id)
			throws HibernateException {
		return (IDriverShiftDefinition) _session.load(DriverShiftDefinition.class, id);
	}

	@Override
	public void removeDriverShiftDefinition(
			IDriverShiftDefinition driverShiftDef) throws HibernateException {
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(driverShiftDef);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

}
