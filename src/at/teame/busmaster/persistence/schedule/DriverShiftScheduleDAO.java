package at.teame.busmaster.persistence.schedule;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import at.gruppe2.dao.schedule.IDriverShiftScheduleDAO;
import at.gruppe2.domain.schedule.IDriverShiftSchedule;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftSchedule;
import at.teame.busmaster.persistence.Persistence;

public class DriverShiftScheduleDAO extends Persistence implements IDriverShiftScheduleDAO{
	private static final DriverShiftScheduleDAO _instance = new DriverShiftScheduleDAO();
	
	private DriverShiftScheduleDAO()
    {
        super();
    }
	
	public static DriverShiftScheduleDAO getInstance() {
		return _instance;
	}
	
	public void saveDriverShift(DriverShiftSchedule driverShift)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(driverShift);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
    @Override
	public List<DriverShiftSchedule> getAllDriverShifts()throws HibernateException{
		List<DriverShiftSchedule> shifts;
		
		shifts = _session.createCriteria(DriverShiftSchedule.class).list();
		
		return shifts;
	}

	@Override
	public void saveDriverShift(IDriverShiftSchedule driverShift)
			throws HibernateException {
		saveDriverShift((DriverShiftSchedule) driverShift);
	}

	@Override
	public IDriverShiftSchedule getDriverShiftSchedule(int id)
			throws HibernateException {
		return (IDriverShiftSchedule) _session.load(DriverShiftSchedule.class, id);
	}

	@Override
	public void removeDriverShiftSchedule(IDriverShiftSchedule driverShift)
			throws HibernateException {
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(driverShift);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<DriverShiftSchedule> getShifts(Driver d, Date start, Date end)throws HibernateException{
		List<DriverShiftSchedule> shifts = null;
		
        Query q =_session.getNamedQuery("DriverShiftSchedule.getByDriverAndDate");
        q.setParameter("driver", d);
        q.setParameter("start", start);
        q.setParameter("end", end);
        
        shifts = q.list();
		
		return shifts;
	}
}
