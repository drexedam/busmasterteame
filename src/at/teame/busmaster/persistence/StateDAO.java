package at.teame.busmaster.persistence;

import at.teame.busmaster.domain.domaininterface.StateDI;
import java.util.List;
import at.teame.busmaster.domain.persistencedata.State;
import org.hibernate.HibernateException;

public class StateDAO extends Persistence{
	
	private static final StateDAO _instance = new StateDAO();
		
	private StateDAO(){}
	
	public static StateDAO getInstance() {
		return _instance;
	}
	
	public void saveState(StateDI state)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(state);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<StateDI> getAllStates()throws HibernateException{
		List<StateDI> countries;

		countries = _session.createCriteria(State.class).list();
		
		return countries;
	}
}
