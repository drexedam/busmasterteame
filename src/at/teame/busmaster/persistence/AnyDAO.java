package at.teame.busmaster.persistence;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import org.hibernate.HibernateException;

public class AnyDAO extends Persistence{
	
	private static final AnyDAO _instance = new AnyDAO();
		
	private AnyDAO()
    {
        super();
    }
	
	public static AnyDAO getInstance() {
		return _instance;
	}
	
    public void saveANY(DI o) throws PeriodException, UncompatibleConvertionException
    {
        try
        {
            Object newO = _session.merge(o.convertToDomain());
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(newO);
            _tx.commit();
        } catch(HibernateException e)
        {
            e.printStackTrace();
            if(_tx != null)
            {
                _tx.rollback();
            }
        } finally
        {
            _tx = null;
        }
    }
}
