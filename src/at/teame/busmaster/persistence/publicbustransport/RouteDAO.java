package at.teame.busmaster.persistence.publicbustransport;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.dao.publicbustransport.IRouteDAO;
import at.gruppe2.domain.publicbustransport.IRoute;
import at.teame.busmaster.domain.persistencedata.publicbustransport.Route;
import at.teame.busmaster.persistence.Persistence;

public class RouteDAO extends Persistence implements IRouteDAO {

	private static final RouteDAO _instance = new RouteDAO();
	
	protected RouteDAO()
    {
        super();
    }
	
	public static RouteDAO getInstance() {
		return _instance;
	}
	
	public void saveRoute(Route route) throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(route);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
	
	@Override
	public void saveRoute(IRoute route) throws HibernateException {
		saveRoute((Route)route);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<? extends IRoute> getAllRoutes() throws HibernateException {
		List<Route> routes;
		
		routes= _session.createCriteria(Route.class).list();
		
		return routes;
	}

	public List<Route> getAllRealRoutes()throws HibernateException{
		List<Route> routes = new LinkedList<>();
		
		for(IRoute r : getAllRoutes()){
			routes.add((Route)r);
		}
		
		return routes;
	}
	
	public void removeRoute(Route route){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(route);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
}
