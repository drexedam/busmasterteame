package at.teame.busmaster.persistence.publicbustransport;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.dao.publicbustransport.IRouteSectionDAO;
import at.gruppe2.domain.publicbustransport.IRouteSection;
import at.teame.busmaster.domain.persistencedata.publicbustransport.RouteSection;
import at.teame.busmaster.persistence.Persistence;

public class RouteSectionDAO extends Persistence implements IRouteSectionDAO{
	private static final RouteSectionDAO _instance = new RouteSectionDAO();
	
	protected RouteSectionDAO()
    {
        super();
    }
	
	public static RouteSectionDAO getInstance() {
		return _instance;
	}
	
	public void saveRouteSection(RouteSection routeSection) throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(routeSection);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
	
	
	@Override
	public void saveRouteSection(IRouteSection routeSection) throws HibernateException {
		saveRouteSection((RouteSection)routeSection);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<? extends IRouteSection> getAllRouteSections()
			throws HibernateException {
		List<RouteSection> sections;
		
		sections = _session.createCriteria(RouteSection.class).list();
		
		return sections;
	}
	
	public List<RouteSection> getAllRealRouteSections()throws HibernateException{
		List<RouteSection> sections = new LinkedList<>();
		
		for(IRouteSection s : getAllRouteSections()){
			sections.add((RouteSection)s);
		}
		
		return sections;
	}
	
	public void removeRouteSection(RouteSection section){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(section);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

}
