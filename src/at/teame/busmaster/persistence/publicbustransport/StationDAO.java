package at.teame.busmaster.persistence.publicbustransport;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.dao.publicbustransport.IStationDAO;
import at.gruppe2.domain.publicbustransport.IStation;
import at.teame.busmaster.domain.persistencedata.publicbustransport.Station;
import at.teame.busmaster.persistence.Persistence;

public class StationDAO extends Persistence implements IStationDAO {
	private static final StationDAO _instance = new StationDAO();
	
	protected StationDAO()
    {
        super();
    }
	
	public static StationDAO getInstance() {
		return _instance;
	}
	
	public void saveStation(Station station)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(station);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
	
	@Override
	public void saveStation(IStation station) throws HibernateException {
		saveStation((Station)station);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<? extends IStation> getAllStations() throws HibernateException {
		List<Station> stations;
		
		stations = _session.createCriteria(Station.class).list();
		
		return stations;
	}
	
	public List<Station> getAllRealStations()throws HibernateException{
		List<Station> stations = new LinkedList<>();
		
		for(IStation s : getAllStations()){
			stations.add((Station)s);
		}
		
		return stations;
	}
	
	public void removeStation(Station station){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(station);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
}
