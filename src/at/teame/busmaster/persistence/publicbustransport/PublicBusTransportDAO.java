package at.teame.busmaster.persistence.publicbustransport;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.dao.publicbustransport.IPublicBusTransportDAO;
import at.gruppe2.domain.publicbustransport.IPublicBusTransport;
import at.teame.busmaster.domain.persistencedata.publicbustransport.PublicBusTransport;
import at.teame.busmaster.persistence.Persistence;

public class PublicBusTransportDAO extends Persistence implements IPublicBusTransportDAO {
	
	private static final PublicBusTransportDAO _instance = new PublicBusTransportDAO();
	
	private PublicBusTransportDAO()
    {
        super();
    }
	
	public static PublicBusTransportDAO getInstance() {
		return _instance;
	}
	
	public void savePublicBusTransport(PublicBusTransport transport)throws HibernateException{
        try
        {
            PublicBusTransport toUpdate = (PublicBusTransport)_session.merge(transport);
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(toUpdate);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<PublicBusTransport> getAllTransports()throws HibernateException{
		List<PublicBusTransport> route;
		
		route = _session.createCriteria(PublicBusTransport.class).list();
		
		return route;
	}

	@Override
	public void savePublicBusTransport(IPublicBusTransport transport)
			throws HibernateException {
		savePublicBusTransport((PublicBusTransport)transport);
	}

	@Override
	public IPublicBusTransport getPublicBusTransport(int id)
			throws HibernateException {
		return (IPublicBusTransport) _session.load(PublicBusTransport.class, id);
	}
	
	public void removePublicBusTransport(PublicBusTransport transport){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(transport);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

}
