package at.teame.busmaster.persistence.coachtour;

import java.util.List;

import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourAdress;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class CoachTourAddressDAO extends Persistence{
	
	private static final CoachTourAddressDAO _instance = new CoachTourAddressDAO();
	
	private CoachTourAddressDAO()
    {
        super();
    }
	
	public static CoachTourAddressDAO getInstance() {
		return _instance;
	}
	
	public void saveAddress(CoachTourAdress address)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(address);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<CoachTourAdress> getAllAddresses()throws HibernateException{
		List<CoachTourAdress> addresses;
		
		addresses = _session.createCriteria(CoachTourAdress.class).list();
		
		return addresses;
	}

}
