package at.teame.busmaster.persistence.coachtour;

import java.util.List;

import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourOrder;
import at.teame.busmaster.persistence.Persistence;

import org.hibernate.HibernateException;

public class CoachTourOrderDAO extends Persistence{
	
	private static final CoachTourOrderDAO _instance = new CoachTourOrderDAO();
	
	private CoachTourOrderDAO()
    {
        super();
    }
	
	public static CoachTourOrderDAO getInstance() {
		return _instance;
	}
	
	public void saveOrder(CoachTourOrder order)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(order);
            _tx.commit();
            _sessionFactory.getCache().evictAllRegions(); //Delete cache
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<CoachTourOrder> getAllOrders()throws HibernateException{
		List<CoachTourOrder> offers;
		
		offers = _session.createCriteria(CoachTourOrder.class).list();
		
		return offers;
	}
	
	@SuppressWarnings("unchecked")
	public List<CoachTourOrder> getAllCancelableOrders(){
		List<CoachTourOrder> orders;
		
		orders = _session.getNamedQuery("Order.NotCanceled").list();
		
		return orders;
	}
	
	@SuppressWarnings("unchecked")
	public List<CoachTourOrder> getAllChangeableAndReservableOrders(){
		List<CoachTourOrder> orders;
		
		orders = _session.getNamedQuery("Order.NotCanceledAndReserved").list();
		
		return orders;
	}
	
	@SuppressWarnings("unchecked")
	public List<CoachTourOrder> getAllBookableOrders(){
		List<CoachTourOrder> orders;
		
		orders = _session.getNamedQuery("Order.NotCanceledButReservedAndNotBooked").list();
		
		return orders;
	}

	@SuppressWarnings("unchecked")
	public List<CoachTourOrder> getAllManageableOrders(){
		List<CoachTourOrder> orders;
		
        orders = _session.getNamedQuery("Order.Manageable").list();
		
		return orders;
	}
}
