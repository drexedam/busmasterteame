package at.teame.busmaster.persistence.coachtour;

import java.util.List;

import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourOrder;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourSchedule;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class CoachTourScheduleDAO extends Persistence{
	
	private static final CoachTourScheduleDAO _instance = new CoachTourScheduleDAO();
	
	private CoachTourScheduleDAO()
    {
        super();
    }
	
	public static CoachTourScheduleDAO getInstance() {
		return _instance;
	}
	
	public void saveSchedule(CoachTourSchedule schedule)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(schedule);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	public List<CoachTourSchedule> getAllSchedules(CoachTourOrder order)throws HibernateException{
		List<CoachTourSchedule> plans = null;
		
		_session.createCriteria(CoachTourSchedule.class).list();

		return plans;
	}
	
	@SuppressWarnings("unchecked")
	public List<CoachTourSchedule> getNotPlanedSchedules(){
		List<CoachTourSchedule> orders;
		
		orders = _session.getNamedQuery("CoachTourSchedule.NotPlaned").list();
		
		return orders;
	}

    public CoachTourSchedule getScheduleByID(int id)
    {
        return (CoachTourSchedule)_session.getNamedQuery("CoachTourSchedule.getByID").setParameter("id", id).uniqueResult();
    }

}
