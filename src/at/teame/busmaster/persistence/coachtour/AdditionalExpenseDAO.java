package at.teame.busmaster.persistence.coachtour;

import java.util.List;

import at.teame.busmaster.domain.persistencedata.coachtour.AdditionalExpense;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourOrder;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class AdditionalExpenseDAO extends Persistence{
	
	private static final AdditionalExpenseDAO _instance = new AdditionalExpenseDAO();
	
	private AdditionalExpenseDAO()
    {
        super();
    }
	
	public static AdditionalExpenseDAO getInstance() {
		return _instance;
	}	
	
	public void saveExpense(AdditionalExpense expense)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(expense);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<AdditionalExpense> getAllExpenses(CoachTourOrder order)throws HibernateException{
		List<AdditionalExpense> costs = null;
		
        costs = _session.createCriteria(AdditionalExpense.class).list();
		
		return costs;
	}

}
