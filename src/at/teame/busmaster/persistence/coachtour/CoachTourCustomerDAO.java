package at.teame.busmaster.persistence.coachtour;

import java.util.List;

import org.hibernate.HibernateException;

import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourCustomer;
import at.teame.busmaster.persistence.Persistence;

public class CoachTourCustomerDAO extends Persistence{
	
	private static final CoachTourCustomerDAO _instance = new CoachTourCustomerDAO();
	
	private CoachTourCustomerDAO()
    {
        super();
    }
	
	public static CoachTourCustomerDAO getInstance() {
		return _instance;
	}
	
	public void saveCustomer(CoachTourCustomerDI customer)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(customer);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<CoachTourCustomerDI> getAllCustomers()throws HibernateException{
		List<CoachTourCustomerDI> customers;
		
		customers = _session.createCriteria(CoachTourCustomer.class).list();
		
		return customers;
	}
	
	public void removeCustomer(CoachTourCustomer customer){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(customer);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

}
