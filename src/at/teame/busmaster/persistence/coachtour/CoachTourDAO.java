package at.teame.busmaster.persistence.coachtour;

import java.util.List;

import at.teame.busmaster.domain.persistencedata.coachtour.CoachTour;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;


public class CoachTourDAO extends Persistence{
	private static final CoachTourDAO _instance = new CoachTourDAO();
	
	private CoachTourDAO()
    {
        super();
    }
	
	public static CoachTourDAO getInstance() {
		return _instance;
	}
	
	public void saveCoachTour(CoachTour tour)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(tour);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<CoachTour> getAllTours()throws HibernateException{
		List<CoachTour> charters;
		
		charters = _session.createCriteria(CoachTour.class).list();
		
		return charters;
	}

}
