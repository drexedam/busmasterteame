package at.teame.busmaster.persistence.user;

import at.teame.busmaster.domain.domaininterface.user.UserDI;
import at.teame.busmaster.domain.persistencedata.user.User;
import at.teame.busmaster.persistence.Persistence;

import org.hibernate.HibernateException;

import java.util.List;
import org.hibernate.Query;

public class UserDAO extends Persistence {
	
	private static final UserDAO _instance = new UserDAO();
	
	private UserDAO()
    {
        super();
    }
	
	public static UserDAO getInstance() {
		return _instance;
	}
	
	public void saveUser(User user)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(user);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	
	public User getUser(String username, String password)throws HibernateException{
		Query q =_session.getNamedQuery("User.getByUserName");
        q.setParameter("userName", username);
        q.setParameter("password", password);
        
        List<User> users = q.list();
        
        if(users != null && !users.isEmpty())
        {
            return users.get(0);
        }
		
		return null;
	}

    @SuppressWarnings("unchecked")
	public List<User> getAllUsers () {
        List<User> users;

        users = _session.createCriteria(User.class).list();

        return users;
    }
}
