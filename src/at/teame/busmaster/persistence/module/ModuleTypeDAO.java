package at.teame.busmaster.persistence.module;

import java.util.List;

import at.teame.busmaster.domain.persistencedata.module.ModuleType;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;

public class ModuleTypeDAO extends Persistence{
	
private static final ModuleTypeDAO _instance = new ModuleTypeDAO();
	
	private ModuleTypeDAO()
    {
        super();
    }
	
	public static ModuleTypeDAO getInstance() {
		return _instance;
	}
	
	public void saveModuleType(ModuleType type)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(type);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<ModuleType> getAllModulTypes()throws HibernateException{
		List<ModuleType> types = null;
        
        _query= _session.createQuery("FROM ModuleType");

        types = _query.list();
			
		return types;
	}
	
	public void removeModuleType(ModuleType type){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(type);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

}
