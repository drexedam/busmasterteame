package at.teame.busmaster.persistence.module;

import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import java.util.List;

import at.teame.busmaster.domain.persistencedata.module.Module;
import at.teame.busmaster.persistence.Persistence;
import org.hibernate.HibernateException;
import org.hibernate.Query;

public class ModuleDAO extends Persistence{
	
private static final ModuleDAO _instance = new ModuleDAO();
	
	private ModuleDAO()
    {
        super();
    }
	
	public static ModuleDAO getInstance() {
		return _instance;
	}
	
	public void saveModule(ModuleDI module)throws HibernateException{
        try
        {
            Object o = _session.merge(module);
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(o);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<ModuleDI> getAllModules()throws HibernateException{
		List<ModuleDI> modules;
		
		modules = _session.createCriteria(Module.class).list();
		
		return modules;
	}

	@SuppressWarnings("unchecked")
	public List<ModuleDI> getAllModulesNotInBus()throws HibernateException{
		
        Query q = _session.getNamedQuery("Modules.getModulesNotInBus");
        return q.list();
	}

	public void removeModule(Module module){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(module);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
}
