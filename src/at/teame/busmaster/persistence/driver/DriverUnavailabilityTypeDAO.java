package at.teame.busmaster.persistence.driver;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.dao.driver.IDriverUnavailabilityTypeDAO;
import at.gruppe2.domain.driver.IDriverUnavailabilityType;
import at.teame.busmaster.domain.persistencedata.driver.DriverUnavailabilityType;
import at.teame.busmaster.persistence.Persistence;

public class DriverUnavailabilityTypeDAO extends Persistence implements IDriverUnavailabilityTypeDAO{

	private static final DriverUnavailabilityTypeDAO _instance = new DriverUnavailabilityTypeDAO();
	
	private DriverUnavailabilityTypeDAO()
    {
        super();
    }
	
	public static DriverUnavailabilityTypeDAO getInstance() {
		return _instance;
	}
	
	public void saveDriverUnavailableType(DriverUnavailabilityType duaTyp)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(duaTyp);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<DriverUnavailabilityType> getAllDriverUnavailableTypes()throws HibernateException{
		List<DriverUnavailabilityType> types = null;
        _query= _session.createQuery("FROM DriverUnavailabilityType");

        types = _query.list();
			
		return types;
	}
	
	public void removeDriverUnavailabilityType(DriverUnavailabilityType type){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(type);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@Override
	public void saveDriverUnavailableType(IDriverUnavailabilityType duaTyp)
			throws HibernateException {
		saveDriverUnavailableType((DriverUnavailabilityType) duaTyp);
	}
}
