package at.teame.busmaster.persistence.driver;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.dao.driver.IDriverDAO;
import at.gruppe2.domain.driver.IDriver;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.persistence.Persistence;

public class DriverDAO extends Persistence implements IDriverDAO{
	
	private static final DriverDAO _instance = new DriverDAO();
	
	private DriverDAO()
    {
        super();
    }
	
	public static DriverDAO getInstance() {
		return _instance;
	}

	public void saveDriver(Driver driver)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(driver);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<Driver> getAllDrivers()throws HibernateException{
		List<Driver> drivers;
		
		drivers = _session.createCriteria(Driver.class).list();
		
		return drivers;
	}

	public void removeDriver(Driver driver){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(driver);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
	
	@Override
	public void saveDriver(IDriver driver) throws HibernateException {
		saveDriver((Driver) driver);
	}

	@Override
	public IDriver getDriver(int id) throws HibernateException {
		return (IDriver) _session.load(Driver.class, id);
	}
}
