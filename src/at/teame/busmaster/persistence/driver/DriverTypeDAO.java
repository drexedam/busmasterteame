package at.teame.busmaster.persistence.driver;

import java.util.List;

import org.hibernate.HibernateException;

import at.gruppe2.dao.driver.IDriverTypeDAO;
import at.gruppe2.domain.driver.IDriverType;
import at.teame.busmaster.domain.persistencedata.driver.DriverType;
import at.teame.busmaster.persistence.Persistence;

public class DriverTypeDAO extends Persistence implements IDriverTypeDAO{
	
	private static final DriverTypeDAO _instance = new DriverTypeDAO();
	
	private DriverTypeDAO()
    {
        super();
    }
	
	public static DriverTypeDAO getInstance() {
		return _instance;
	}
	
	public void saveDriverType(DriverType driverType)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(driverType);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<DriverType> getAllDriverTypes()throws HibernateException{
		List<DriverType> driverTypes = null;
		
		openSession();
        _query= _session.createQuery("FROM DriverType");

        driverTypes = _query.list();
		
		return driverTypes;
	}
	
	public void removeDriverType(DriverType driverType){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(driverType);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@Override
	public void saveDriverType(IDriverType driverType)
			throws HibernateException {
		saveDriverType((DriverType) driverType); 
	}
}
