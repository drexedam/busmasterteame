package at.teame.busmaster.persistence.driver;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.HolidayWish;
import at.teame.busmaster.persistence.Persistence;

public class HolidayWishDAO extends Persistence {

	private static final HolidayWishDAO _instance = new HolidayWishDAO();
	
	private HolidayWishDAO() {
		super();
	}
	
	public static HolidayWishDAO getInstance() {
		return _instance;
	}
	
	public void saveWish(HolidayWish wish)throws HibernateException{
        try
        {
        	Object newO = _session.merge(wish);
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(newO);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
	
	public void removeWish(int id) {

            Query q = _session.getNamedQuery("HolidayWish.deleteByID");
            q.setParameter("id", id);
            q.executeUpdate();
        
	}

	public void removeWish(HolidayWish wish){
        try
        {
        	
            _tx = _session.beginTransaction();
            _session.delete(wish);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
	
	@SuppressWarnings("unchecked")
	public List<HolidayWish> getWishes(Driver d)throws HibernateException{
		List<HolidayWish> wishes = null;
		
        Query q =_session.getNamedQuery("HolidayWish.getByDriver");
        q.setParameter("driver", d);
        
        wishes = q.list();
		
		return wishes;
	}
	
}
