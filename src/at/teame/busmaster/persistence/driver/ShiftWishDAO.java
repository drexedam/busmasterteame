package at.teame.busmaster.persistence.driver;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.ShiftWish;
import at.teame.busmaster.persistence.Persistence;

public class ShiftWishDAO extends Persistence {

private static final ShiftWishDAO _instance = new ShiftWishDAO();
	
	private ShiftWishDAO() {
		super();
	}
	
	public static ShiftWishDAO getInstance() {
		return _instance;
	}
	
	public void saveWish(ShiftWish wish)throws HibernateException{
        try
        {
        	Object newO = _session.merge(wish);
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(newO);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
	
	public void removeWish(int id) {

            Query q = _session.getNamedQuery("ShiftWish.deleteByID");
            q.setParameter("id", id);
            q.executeUpdate();
        
	}

	public void removeWish(ShiftWish wish){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(wish);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}
	
	@SuppressWarnings("unchecked")
	public List<ShiftWish> getWishes(Driver d)throws HibernateException{
		List<ShiftWish> wishes = null;
		
        Query q =_session.getNamedQuery("ShiftWish.getByDriver");
        q.setParameter("driver", d);
        
        wishes = q.list();
		
		return wishes;
	}
	
}
