package at.teame.busmaster.persistence.driver;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import at.teame.busmaster.domain.persistencedata.coachtour.CoachTour;
import at.teame.busmaster.domain.persistencedata.driver.CoachTourWish;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.persistence.Persistence;

public class CoachTourWishDAO extends Persistence
{

    private static final CoachTourWishDAO _instance = new CoachTourWishDAO();

    private CoachTourWishDAO()
    {
        super();
    }

    public static CoachTourWishDAO getInstance()
    {
        return _instance;
    }

    public void saveWish(CoachTourWish wish) throws HibernateException
    {
        try
        {
            _tx = _session.beginTransaction();
            _session.merge(wish);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
    }

    public void removeWish(int id)
    {
        Query q = _session.getNamedQuery("CoachTourWish.deleteByID");
        q.setParameter("id", id);
        q.executeUpdate();
    }

    public void removeWish(CoachTourWish wish)
    {
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(wish);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CoachTourWish> getWishes(Driver d) throws HibernateException
    {
        List<CoachTourWish> wishes = null;

        Query q = _session.getNamedQuery("CoachTourWish.getByDriver");
        q.setParameter("driver", d);

        wishes = q.list();

        return wishes;
    }

    @SuppressWarnings("unchecked")
    public List<CoachTourWish> getWishes(CoachTour ct) throws HibernateException
    {
        List<CoachTourWish> wishes = null;

        Query q = _session.getNamedQuery("CoachTourWish.getByCoachTour");
        q.setParameter("coachTour", ct);

        wishes = q.list();

        return wishes;
    }

}
