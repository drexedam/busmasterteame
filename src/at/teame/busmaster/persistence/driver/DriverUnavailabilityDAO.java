package at.teame.busmaster.persistence.driver;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import at.gruppe2.dao.driver.IDriverUnavailabilityDAO;
import at.gruppe2.domain.driver.IDriver;
import at.gruppe2.domain.driver.IDriverUnavailability;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.DriverUnavailability;
import at.teame.busmaster.persistence.Persistence;

public class DriverUnavailabilityDAO extends Persistence implements IDriverUnavailabilityDAO {
	
	private static final DriverUnavailabilityDAO _instance = new DriverUnavailabilityDAO();
	
	private DriverUnavailabilityDAO()
    {
        super();
    }
	
	public static DriverUnavailabilityDAO getInstance() {
		return _instance;
	}
	
	public void saveDriverUnavailable(DriverUnavailability driverUnavailable)throws HibernateException{
        try
        {
            _tx = _session.beginTransaction();
            _session.saveOrUpdate(driverUnavailable);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<DriverUnavailability> getUnavailable(Driver driver)throws HibernateException{
		List<DriverUnavailability> notAvailable=null;
        _criteria = _session.createCriteria(DriverUnavailability.class);
        _criteria.add(Restrictions.eq("_driver", driver));
        notAvailable = _criteria.list();
			
		return notAvailable;
	}
	
	public void removeDriverUnavailability(DriverUnavailability unavailability){
        try
        {
            _tx = _session.beginTransaction();
            _session.delete(unavailability);
            _tx.commit();
        } catch (HibernateException e)
        {
            _tx.rollback();
            throw e;
        }
	}

	@Override
	public void saveDriverUnavailable(IDriverUnavailability driverUnavailable)
			throws HibernateException {
		saveDriverUnavailable((DriverUnavailability) driverUnavailable);
		
	}

	@Override
	public List<? extends IDriverUnavailability> getUnavailable(IDriver driver)
			throws HibernateException {
		return getUnavailable((Driver) driver);
	}

}
