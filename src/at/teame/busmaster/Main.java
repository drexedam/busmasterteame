/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster;

import at.busmaster.teamf.application.controller.DriverShiftControllerImp;
import java.awt.Dimension;
import java.util.List;

import at.teame.busmaster.bundles.LanguageLocale;
import at.teame.busmaster.bundles.Msg;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.gui.ConfigLoading;
import at.teame.busmaster.gui.Login;
import at.teame.busmaster.gui.MainGUI;
import at.teame.busmaster.gui.Reminder;
import at.teame.busmaster.gui.guicontroller.CoachTourGuiController;
import at.teame.busmaster.persistence.Factory;
import at.teame.busmaster.persistence.bus.BusDAO;
import at.teame.busmaster.persistence.driver.DriverDAO;
import at.teame.busmaster.persistence.publicbustransport.PublicBusTransportDAO;
import at.teame.busmaster.persistence.schedule.CircleDefinitionDAO;
import at.teame.busmaster.persistence.schedule.DriverShiftDefinitionDAO;
import at.teame.busmaster.persistence.schedule.DriverShiftScheduleDAO;

/**
 *
 * @author Patrick
 */
public class Main {
    private static MainGUI _gui;
    private static Login _login;
    
    public static void main(String[] args) {
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | javax.swing.UnsupportedLookAndFeelException | IllegalAccessException | InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(MainGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        DriverShiftControllerImp.getNewInstance(null, new Factory(),
                                                DriverShiftScheduleDAO.getInstance(),
                                                DriverShiftDefinitionDAO.getInstance(),
                                                CircleDefinitionDAO.getInstance(),
                                                BusDAO.getInstance(), DriverDAO.getInstance(),
                                                PublicBusTransportDAO.getInstance(), null);
        ConfigLoading.setTo_DO_LOADING();
        _login = new Login();
        _login.setVisible(true);
    }
    
    public static void createMainGui(int locale) {
        if (locale == 0) {
            Msg.switchLanguage(LanguageLocale.getLanguageLocale(LanguageLocale.english));
        } else {
            Msg.switchLanguage(LanguageLocale.getLanguageLocale(LanguageLocale.deutsch));
        }
        _gui = new MainGUI();
        _gui.setSize(new Dimension(1024, 600));
        _gui.setVisible(true);
        @SuppressWarnings("unchecked")
		List<CoachTourOrderDI> toReserve = (List<CoachTourOrderDI>) CoachTourGuiController.getInstance().reminder();
        if (toReserve.size() > 0) {
            Reminder r = new Reminder();
            r.setData(toReserve);
            r.setVisible(true);
        }
        _login.dispose();
    }
}
