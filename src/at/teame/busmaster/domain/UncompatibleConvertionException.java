package at.teame.busmaster.domain;

import at.gruppe2.exceptions.BusmasterException;

public class UncompatibleConvertionException extends Exception implements
        BusmasterException {

		private static final long serialVersionUID = 1L;
		private String _bundleMsg;
		
		public UncompatibleConvertionException(String message, String bundleMessage, Throwable cause) {
			super(message, cause);
			_bundleMsg = bundleMessage;
		}
		
		public UncompatibleConvertionException(String message, String bundleMessage) {
			super(message);
			_bundleMsg = bundleMessage;
		}
		
		public UncompatibleConvertionException(String bundleMessage) {
			_bundleMsg = bundleMessage;
		}
		
		@Override
		public String getBundleMessage() {
			return _bundleMsg;
		}

}
