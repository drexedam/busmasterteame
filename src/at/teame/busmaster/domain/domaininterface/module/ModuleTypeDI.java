package at.teame.busmaster.domain.domaininterface.module;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface ModuleTypeDI extends DI{
    /**
     * 
     * @return description of the type
     */
	public String getSpecification();
    /**
     * 
     * @return required seats in the bus
     */
	public int getRequiredSpace();
    /**
     * 
     * @return the price
     */
	public int getPrice();
}
