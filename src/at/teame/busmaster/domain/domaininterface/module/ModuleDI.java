package at.teame.busmaster.domain.domaininterface.module;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface ModuleDI extends DI{
    /**
     * 
     * @return the bus this module is in now
     */
	public BusDI getBus();
    /**
     * 
     * @return the type
     */
	public ModuleTypeDI getType();
    /**
     * 
     * @return the price
     */
	public int getPrice();
    /**
     * 
     * @return the required space
     */
	public int getRequiredSpace();
}
