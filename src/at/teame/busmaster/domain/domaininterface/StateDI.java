package at.teame.busmaster.domain.domaininterface;

/**
 * @author Team E
 * @version 0.3
 */
public interface StateDI extends DI{
    /**
     * 
     * @return name of the state
     */
	public String getSpecification();
}
