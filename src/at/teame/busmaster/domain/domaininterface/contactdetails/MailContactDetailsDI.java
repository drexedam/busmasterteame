package at.teame.busmaster.domain.domaininterface.contactdetails;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface MailContactDetailsDI extends DI{
    /**
     * 
     * @return the address
     */
	public String getAdress();
    /**
     * 
     * @return the driver (nullable)
     */
	public DriverDI getDriver();
    /**
     * 
     * @return the customer (nullable)
     */
	public CoachTourCustomerDI getCoachTourCustomer();
    /**
     * 
     * @return the type of contact
     */
	public ContactDetailsTypeDI getType();
}
