package at.teame.busmaster.domain.domaininterface.contactdetails;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface ContactDetailsTypeDI extends DI{
    /**
     * 
     * @return description of type
     */
	public String getSpecification();
}
