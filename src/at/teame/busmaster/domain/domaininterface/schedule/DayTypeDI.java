package at.teame.busmaster.domain.domaininterface.schedule;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface DayTypeDI extends DI{
    /**
     * 
     * @return the name
     */
	public String getSpecification();
    /**
     * 
     * @return a description
     */
	public String getDescription();
}
