package at.teame.busmaster.domain.domaininterface.schedule;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.PublicBusTransportDI;
import java.util.List;

/**
 * @author Team E
 * @version 0.3
 */
public interface CircleDefinitionDI extends DI{
	/**
     * 
     * @return the day type this is planned for
     */
    public DayTypeDI getDayType();
	/**
     * 
     * @return description
     */
    public String getSpecification();
    /**
     * 
     * @return the default used bus
     */
    public BusDI getDefaultBus();
    /**
     * 
     * @return List of all public transprotations
     */
    public List<? extends PublicBusTransportDI> getPublicBusTransports();

}
