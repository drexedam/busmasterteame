package at.teame.busmaster.domain.domaininterface.schedule;

import java.util.Date;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface DriverShiftScheduleDI extends DI{
    /**
     * 
     * @return the date
     */
	public Date getDate();
    /**
     * 
     * @return the definition
     */
	public DriverShiftDefinitionDI getDefinition();
	/**
     * 
     * @return the driver
     */
    public DriverDI getDriver();
    
    /**
     * 
     * @return the id
     */
    public int getID();
}
