package at.teame.busmaster.domain.domaininterface.schedule;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface DriverShiftDefinitionDI extends DI{
    /**
     * 
     * @return the day type on which this is planed
     */
	public DayTypeDI getDayType();
    /**
     * 
     * @return description of the definition
     */
	public String getSpecification();
}
