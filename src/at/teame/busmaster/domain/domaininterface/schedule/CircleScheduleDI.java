package at.teame.busmaster.domain.domaininterface.schedule;

import java.util.Date;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface CircleScheduleDI extends DI{
    /**
     * 
     * @return the date
     */
	public Date getDate();
    /**
     * 
     * @return the definition
     */
	public CircleDefinitionDI getDefinition();
	/**
     * 
     * @return the bus
     */
    public BusDI getBus();
}
