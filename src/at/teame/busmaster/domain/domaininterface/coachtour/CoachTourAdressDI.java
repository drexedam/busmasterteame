package at.teame.busmaster.domain.domaininterface.coachtour;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.StateDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface CoachTourAdressDI extends DI{
    /**
     * 
     * @return the street
     */
	public String getStreet();
    /**
     * 
     * @return the post code
     */
	public String getZIP();
    /**
     * 
     * @return the house number
     */
	public String getHouseNumber();
    /**
     * 
     * @return the city
     */
	public String getCity();
    /**
     * 
     * @return the state
     */
	public StateDI getState();
}
