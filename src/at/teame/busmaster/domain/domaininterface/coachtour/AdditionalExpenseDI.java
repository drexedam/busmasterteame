package at.teame.busmaster.domain.domaininterface.coachtour;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface AdditionalExpenseDI extends DI{
    /**
     * 
     * @return short discription
     */
	public String getSpecification();
    /**
     * 
     * @return the price
     */
	public int getPrice();
    /**
     * 
     * @return the referanced order
     */
	public CoachTourOrderDI getOrder();
}
