package at.teame.busmaster.domain.domaininterface.coachtour;

import java.util.Date;
import java.util.List;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface CoachTourOrderDI extends DI{
    /**
     * 
     * @return the number of persons
     */
	public int getNumberOfPersons();
    /**
     * 
     * @return the date on that this order was created
     */
	public Date getCreationDate();
    /**
     * 
     * @return the date on that this order was reservat
     */
	public Date getReservationDate();
    /**
     * 
     * @return the date on that this order was canceled
     */
	public Date getCancelDate();
    /**
     * 
     * @return the date on that this order was booked
     */
	public Date getBookingDate();
    /**
     * 
     * @return short text discribing the order
     */
	public String getSpecification();
    /**
     * 
     * @return referenced customer of this order
     */
	public CoachTourCustomerDI getCustomer();
    /**
     * 
     * @return the list of schedules of this order
     */
	public List<? extends CoachTourScheduleDI> getSchedules();
    /**
     * 
     * @return the list of additional expenses
     */
	public List<? extends AdditionalExpenseDI> getExpenses();
	
    /**
     * 
     * @return the list of all tours of all schedules
     */
	public List<? extends CoachTourDI> getTours();
	
}
