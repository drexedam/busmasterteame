package at.teame.busmaster.domain.domaininterface.coachtour;

import java.util.Date;
import java.util.List;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface CoachTourScheduleDI extends DI{
    /**
     * 
     * @return the database ID
     */
	public int getID();
    /**
     * 
     * @return the distance in km
     */
	public int getDistance();
    /**
     * 
     * @return the travel time
     */
	public int getTravelTime();
    /**
     * 
     * @return the standing time
     */
	public int getStandingTime();
    /**
     * 
     * @return the number of drivers needed for this schedule
     */
	public int getNumberOfDrivers();
    /**
     * 
     * @return the number of busses needed for this schedule
     */
	public int getNumberOfBusses();
    /**
     * 
     * @return the start date
     */
	public Date getStart();
    /**
     * 
     * @return the end date
     */
	public Date getEnd();
    /**
     * 
     * @return short text to discripe the schedule
     */
	public String getSpecification();
	
    /**
     * 
     * @return the list of modules needed for the schedule
     */
	public List<? extends ModuleDI> getModules();
    /**
     * 
     * @return the list of addresses. The first address is the start all following ones are destinations.
     */
	public List<?extends CoachTourAdressDI> getAdresses();
    /**
     * 
     * @return the connected Order
     */
	public CoachTourOrderDI getOrder();
	
    /**
     * 
     * @return the list of Tours. Each tour represents a combination of a bus, driver and maybe a additional driver.
     */
	public List<? extends CoachTourDI> getTours();
}
