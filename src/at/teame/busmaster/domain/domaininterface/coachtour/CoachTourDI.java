package at.teame.busmaster.domain.domaininterface.coachtour;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface CoachTourDI extends DI{
    /**
     * 
     * @return main driver of this tour
     */
	public DriverDI getDriver();
    /**
     * 
     * @return additional driver of this tour
     */
	public DriverDI getAdditionalDriver();
    /**
     * 
     * @return used bus of this tour
     */
	public BusDI getBus();
    /**
     * 
     * @return referenced schedule
     */
	public CoachTourScheduleDI getSchedule();
}
