package at.teame.busmaster.domain.domaininterface.coachtour;

import java.util.List;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.MailContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.TelContactDetailsDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface CoachTourCustomerDI extends DI{
    /**
     * 
     * @return first name of the customer
     */
	public String getFirstName();
    /**
     * 
     * @return last name of the customer
     */
	public String getLastName();
    /**
     * 
     * @return name of the juristic person
     */
	public String getJurPerson();
    /**
     * 
     * @return the street (part of the address)
     */
	public String getStreet();
    /**
     * 
     * @return the house number (part of the address)
     */
	public String getHouseNumber();
    /**
     * 
     * @return the post code (part of the address
     */
	public String getZIP();
    /**
     * 
     * @return the city (part of the address)
     */
	public String getCity();
    /**
     * 
     * @return the state (part of the address)
     */
	public StateDI getState();
    /**
     * 
     * @return list of all orders this customer is referanced by
     */
	public List<? extends CoachTourOrderDI> getOrders();
    /**
     * 
     * @return list of all mailcontacts this customer is referanced by
     */
    public List<? extends MailContactDetailsDI> getMail();
    /**
     * 
     * @return list of all telcontacts this customer is referanced by
     */
    public List<? extends TelContactDetailsDI> getTel();

}
