package at.teame.busmaster.domain.domaininterface.user;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface UserDI extends DI{
    /**
     * 
     * @return the user name
     */
	public String getUserName();
    /**
     * 
     * @return the password (plain)
     */
	public String getPassword();
    /**
     * 
     * @return the permition
     */
	public String getPermition();
	/**
	 * 
	 * @return the driver
	 */
	public DriverDI getDriver();
}
