package at.teame.busmaster.domain.domaininterface;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;

/**
 * @author Team E
 * @version 0.3
 */
public interface DI
{
    /**
     * 
     * @return an object with the same values which can be persisted
     * @throws PeriodException if there is a twist in the dates
     * @throws UncompatibleConvertionException if there are errors in the data
     */
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException;
}
