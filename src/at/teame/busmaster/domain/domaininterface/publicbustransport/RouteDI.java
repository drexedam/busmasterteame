package at.teame.busmaster.domain.domaininterface.publicbustransport;

import java.util.List;

import at.teame.busmaster.domain.domaininterface.DI;

public interface RouteDI extends DI{
	public String getNumber();
	public String getSpecification();
	public List<? extends RouteSectionDI> getSections();
}
