package at.teame.busmaster.domain.domaininterface.publicbustransport;

import java.util.Date;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.schedule.CircleDefinitionDI;
import at.teame.busmaster.domain.domaininterface.schedule.DayTypeDI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftDefinitionDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface PublicBusTransportDI extends DI{
    /**
     * 
     * @return start time
     */
	public Date getStart();
    /**
     * 
     * @return the circle definition this is part of
     */
	public CircleDefinitionDI getCircleDefinition();
    /**
     * 
     * @return the driver shift definition this is part of
     */
	public DriverShiftDefinitionDI getShiftDefinition();
	
	public DayTypeDI getDayType();
	
	public RouteDI getRoute();
}
