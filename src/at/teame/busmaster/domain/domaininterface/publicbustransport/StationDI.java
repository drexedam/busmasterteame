package at.teame.busmaster.domain.domaininterface.publicbustransport;

import at.teame.busmaster.domain.domaininterface.DI;

public interface StationDI extends DI{
	public String getNumber();
	public String getName();
	public String getShortDesc();
}
