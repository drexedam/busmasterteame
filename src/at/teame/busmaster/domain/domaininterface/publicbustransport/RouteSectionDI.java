package at.teame.busmaster.domain.domaininterface.publicbustransport;

import at.teame.busmaster.domain.domaininterface.DI;


public interface RouteSectionDI extends DI{

	public int getDistance();
	public int getDrivingTime();
	public StationDI getStart();
	public StationDI getEnd();
}
