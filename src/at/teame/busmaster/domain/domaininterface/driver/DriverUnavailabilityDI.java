package at.teame.busmaster.domain.domaininterface.driver;

import java.util.Date;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface DriverUnavailabilityDI extends DI{
    /**
     * 
     * @return start date of this unavailability
     */
	public Date getStart();
    /**
     * 
     * @return end date of this unavailability
     */
	public Date getEnd();
    /**
     * 
     * @return type of unavailability
     */
	public DriverUnavailabilityTypeDI getType();
    /**
     * 
     * @return driver effected by this unavailability
     */
	public DriverDI getDriver();
}
