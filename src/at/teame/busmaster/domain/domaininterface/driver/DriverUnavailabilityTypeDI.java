package at.teame.busmaster.domain.domaininterface.driver;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface DriverUnavailabilityTypeDI extends DI{
    /**
     * 
     * @return description of type
     */
	public String getSpecification();
}
