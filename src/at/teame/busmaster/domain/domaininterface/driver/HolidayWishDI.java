package at.teame.busmaster.domain.domaininterface.driver;

import java.util.Date;

import at.teame.busmaster.domain.domaininterface.DI;

public interface HolidayWishDI extends DI {

	public Date getStart();
	public Date getEnd();
	public DriverDI getDriver();
	public int getId();
}
