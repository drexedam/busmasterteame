package at.teame.busmaster.domain.domaininterface.driver;

import java.util.Date;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface DriverDI extends DI{
    /**
     * 
     * @return first name of the driver
     */
	public String getFirstName();
    /**
     * 
     * @return last name of the driver
     */
	public String getLastName();
    /**
     * 
     * @return the street (part of the address)
     */
	public String getStreet();
    /**
     * 
     * @return the house number (part of the address)
     */
	public String getHouseNumber();
    /**
     * 
     * @return the post code (part of the address
     */
	public String getZIP();
    /**
     * 
     * @return the city (part of the address)
     */
	public String getCity();
    /**
     * 
     * @return the state (part of the address)
     */
	public StateDI getState();
    /**
     * 
     * @return type of driver
     */
	public DriverTypeDI getType();
    /**
     * 
     * @param start start date of the period
     * @param end end date of the period
     * @return if or if not the driver is available in the given time period
     * @throws PeriodException thrown if start and end date are changed
     */
	public boolean isAvailable(Date start, Date end) throws PeriodException;
	/**
	 * 
	 * @param start date of the period
	 * @param end date of the period
	 * @param order to ignore
	 * @return
	 * @throws PeriodException
	 */
	public boolean isAvailable(Date start, Date end, CoachTourOrderDI order) throws PeriodException;
	/**
     * 
     * @return the list of tours the driver is planed for or was part of
     */
	public List<? extends CoachTourDI> getCoachTours();
}
