/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.domain.domaininterface.driver;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;

/**
 * @author  Morent Jochen <jochen.morent@students.fhv.at>
 * @org     FHV <fhv.at>
 * @project BusMaster-Team-E
 * @date    30.05.2014
 */
public interface CoachTourWishDI extends DI
{
    /**
     * 
     * @return the corresponding Driver
     */
    public int getID();
    /**
     * 
     * @return the corresponding Driver
     */
    public DriverDI getDriver();
    /**
     * 
     * @return the corresponding CoachTour
     */
    public CoachTourScheduleDI getCoachTourSchedule();
}
