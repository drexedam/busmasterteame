package at.teame.busmaster.domain.domaininterface.driver;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface DriverTypeDI extends DI{
    /**
     * 
     * @return the fare of this type
     */
	public int getFare();
    /**
     * 
     * @return the standing price of this type
     */
	public int getStandingPrice();
    /**
     * 
     * @return description of the type
     */
	public String getSpecification();
}
