package at.teame.busmaster.domain.domaininterface.driver;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftScheduleDI;

public interface ShiftWishDI extends DI {

	public boolean getWished();
	
	public DriverDI getDriver();
	
	public DriverShiftScheduleDI getShift();
	
	public int getID();
	
}
