package at.teame.busmaster.domain.domaininterface.bus;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface BusTypeDI extends DI{
	/**
     * 
     * @return number of persons allowed to stand in the bus
     */
	public int getStandingPlaces();
	/**
     * 
     * @return the total number of seats
     */
	public int getSeats();
    /**
     * 
     * @return the name of the type
     */
	public String getType();
    /**
     * 
     * @return the price that has to be paid if the bus is orderd
     */
	public int getBasePrice();
    /**
     * 
     * @return the price that has to be paid for each km
     */
	public int getKMPrice();
}
