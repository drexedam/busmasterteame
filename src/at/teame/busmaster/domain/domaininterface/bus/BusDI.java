package at.teame.busmaster.domain.domaininterface.bus;

import java.util.Date;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;

/**
 * @author Team E
 * @version 0.3
 */
public interface BusDI extends DI{

    /**
     * 
     * @return the text written on the numberplate.
     */
	public String getNumberPlate();
    /**
     * 
     * @return the type of the bus.
     */
	public BusTypeDI getType();
    /**
     * 
     * @return the list of modules build in the bus
     */
	public List<? extends ModuleDI> getModules();
    /**
     * 
     * @param start start date of the period
     * @param end end date of the period
     * @return if or if not the bus is available in the given time period
     * @throws PeriodException thrown if start and end date are changed
     */
	public boolean isAvailable(Date start, Date end) throws PeriodException;
	/**
	 * 
	 * @param start date of the period
	 * @param end date of the pereiod
	 * @param order to ignore
	 * @return
	 * @throws PeriodException
	 */
	public boolean isAvailable(Date start, Date end, CoachTourOrderDI order) throws PeriodException;
	/**
     * 
     * @return the total number of seats
     */
	public int getNumOfSeats();
}
