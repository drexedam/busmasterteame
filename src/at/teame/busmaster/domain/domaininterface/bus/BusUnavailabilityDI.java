package at.teame.busmaster.domain.domaininterface.bus;

import java.util.Date;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface BusUnavailabilityDI extends DI {
    /**
     * 
     * @return start of this unavailability
     */
	public Date getStart();
    /**
     * 
     * @return end of this unavailability
     */
	public Date getEnd();
    /**
     * 
     * @return bus effected by this unavailability
     */
	public BusDI getBus();
    /**
     * 
     * @return type of this unavailability
     */
	public BusUnavailabilityTypeDI getType();
}
