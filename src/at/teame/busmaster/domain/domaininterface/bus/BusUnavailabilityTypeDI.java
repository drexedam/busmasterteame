package at.teame.busmaster.domain.domaininterface.bus;

import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface BusUnavailabilityTypeDI extends DI {
    /**
     * 
     * @return the discription of the type
     */
	public String getSpecification();
   
}
