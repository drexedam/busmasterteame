package at.teame.busmaster.domain.domaininterface.offer;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.AdditionalExpenseDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.persistencedata.bus.BusType;
import at.teame.busmaster.domain.persistencedata.driver.DriverType;
import java.util.List;

/**
 * @author Team E
 * @version 0.3
 */
public interface OfferDI extends DI{
    /**
     * 
     * @param bustype the types of busses
     * @param drivertype the types of drivers
     * @param drivers the number of drivers
     * @param traveltime the travel time
     * @param standingtime the standing time
     * @param busses the number of busses
     * @param distance the distance
     * @return price in cents
     */
	public int getPrice(List<BusType> bustype, List<DriverType> drivertype, int drivers, int traveltime, int standingtime, int busses, int distance);
	/**
     * 
     * @return the list of additional expences
     */
    public List<? extends AdditionalExpenseDI> getExpenses();
    /**
     * 
     * @return the list of modules (for each schedule)
     */
	public List<List<ModuleDI>> getModules();
}
