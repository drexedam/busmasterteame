package at.teame.busmaster.domain.persistencedata.contactdetails;

import at.gruppe2.exceptions.BusmasterException;

public class ContactDetailException extends Exception implements BusmasterException {

	private static final long serialVersionUID = 1L;
	private String _bundleMsg;
	
	public ContactDetailException(String message, String bundleMessage) {
		super(message);
		_bundleMsg = bundleMessage;
	}
	
	public ContactDetailException(String bundleMessage) {
		_bundleMsg = bundleMessage;
	}
	
	@Override
	public String getBundleMessage() {
		return _bundleMsg;
	}

}
