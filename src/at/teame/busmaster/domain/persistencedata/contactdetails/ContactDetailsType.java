package at.teame.busmaster.domain.persistencedata.contactdetails;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.contactdetails.ContactDetailsTypeDI;

@Entity
@Table(name="KontaktDatenTyp")
public class ContactDetailsType implements ContactDetailsTypeDI {
	@Id @GeneratedValue
	@Column(name="KDT_ID")
	private int _id;
	@Column(name="KDT_Bezeichnung")
	private String _specification;
	@OneToMany(mappedBy="_type", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<MailContactDetails> _mail = new LinkedList<>();
	@OneToMany(mappedBy="_type", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<TelContactDetails> _tel = new LinkedList<>();
	
	public ContactDetailsType() {}
	
	public ContactDetailsType(String specification) {
		_specification = specification;
	}
	
	@Override
	public String getSpecification() { return _specification; }
	public void getSpecification(String value) { _specification = value; }

    @Override
    public DI convertToDomain() throws PeriodException
    {
        return this;
    }
	
    @Override
    public boolean equals(Object obj) {
    	if(obj == null) {
    		return false;
    	}
    	
    	if(!(obj instanceof ContactDetailsTypeDI)) {
    		return false;
    	}
    	
    	ContactDetailsTypeDI cdt = (ContactDetailsTypeDI) obj;
    	
    	return cdt.getSpecification().equals(_specification);
    }
    
    @Override
    public String toString(){
        return _specification;
    }
}
