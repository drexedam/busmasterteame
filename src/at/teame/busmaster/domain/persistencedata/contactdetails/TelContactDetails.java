package at.teame.busmaster.domain.persistencedata.contactdetails;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.contactdetails.TelContactDetailsDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourCustomer;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import javax.persistence.FetchType;

@Entity
@Table(name="TelKontaktDaten")
public class TelContactDetails implements TelContactDetailsDI {

	@Id @GeneratedValue
	@Column(name="TKD_ID")
	private int _id;
	@Column(name="TKD_Nummer")
	private String _number;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="TKD_F_ID")
	private Driver _driver;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="TKD_CK_ID")
	private CoachTourCustomer _customer;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="TKD_KDT_ID")
	private ContactDetailsType _type;
	
	public TelContactDetails() { }
	
	public TelContactDetails(String number, ContactDetailsType type){
		_number = number;
		_type = type;
	}
	
	public TelContactDetails(String number, Driver driver, ContactDetailsType type) {
		_number = number;
		_driver = driver;
		_type = type;
	}
	
	public TelContactDetails(String number, CoachTourCustomer customer, ContactDetailsType type) {
		_number = number;
		_customer = customer;
		_type = type;
	}
	
	@Override
	public String getNumber() { return _number; }
	public void setNumber(String value) { _number = value; }
	
	@Override
	public Driver getDriver() { return _driver; }
	public void setDriver(Driver value) throws ContactDetailException { 
		if(_customer == null) {
		_driver = value;
		} else {
			throw new ContactDetailException("Tried to set customer where driver is already set!", "exception.ContactDetailException.driver");
		} 
	}
	
	@Override
	public CoachTourCustomer getCoachTourCustomer() { return _customer; }
	public void setCoachServiceCustomer(CoachTourCustomer value) throws ContactDetailException { 
		if(_driver == null) {
			_customer = value;
		} else {
			throw new ContactDetailException("Tried to set driver where customer is already set!", "exception.ContactDetailException.customer");
		}
	}
	
	@Override
	public ContactDetailsType getType() { return _type; }
	public void setType(ContactDetailsType value) { _type = value; }

    @Override
    public DI convertToDomain() throws PeriodException
    {
        return this;
    }
    
    @Override
    public boolean equals(Object obj) {
    	if(obj == null) {
    		return false;
    	}
    	
    	if(!(obj instanceof TelContactDetailsDI)) {
    		return false;
    	}
    	
    	TelContactDetailsDI mcd = (TelContactDetailsDI) obj;

    	if(mcd.getCoachTourCustomer() == null && _customer == null) {
    		if(!mcd.getDriver().equals(_driver)) {
    			return false;
    		}
    	} else if(!mcd.getCoachTourCustomer().equals(_customer)) {
    		return false;
    	}
    	
    	if(!mcd.getNumber().equals(_number) || !mcd.getType().equals(_type)) {
    		return false;
    	}
    	return true;
    }
}
