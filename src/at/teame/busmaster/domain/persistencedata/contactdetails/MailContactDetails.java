package at.teame.busmaster.domain.persistencedata.contactdetails;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.contactdetails.MailContactDetailsDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourCustomer;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import javax.persistence.FetchType;

@Entity
@Table(name="MailKontaktDaten")
public class MailContactDetails implements MailContactDetailsDI {

	@Id @GeneratedValue
	@Column(name="MKD_ID")
	private int _id;
	@Column(name="MKD_Adresse")
	private String _adress;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="MKD_F_ID")
	private Driver _driver;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="MKD_CK_ID")
	private CoachTourCustomer _customer;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="MKD_KDT_ID")
	private ContactDetailsType _type;
	
	public MailContactDetails() {}
	
	public MailContactDetails(String address, ContactDetailsType type){
		_adress = address;
		_type = type;
	}
	
	public MailContactDetails(String adress, Driver driver, ContactDetailsType type) {
		_adress = adress;
		_driver= driver;
		_type = type;
	}
	
	public MailContactDetails(String adress, CoachTourCustomer customer, ContactDetailsType type) {
		_adress = adress;
		_customer = customer;
		_type = type;
	}
	
	@Override
	public String getAdress() { return _adress; }
	public void setAdress(String value) { _adress = value; }
	
	@Override
	public Driver getDriver() { return _driver; }
	public void setDriver(Driver value) throws ContactDetailException { 
		if(_customer == null) {
			_driver = value;
		} else {
			throw new ContactDetailException("Tried to set driver where customer is already set!", "exception.ContactDetailException.driver");
		}
	}
	
	@Override
	public CoachTourCustomer getCoachTourCustomer() { return _customer; }
	public void setCoachServiceCustomer(CoachTourCustomer value) throws ContactDetailException { 
		if(_driver == null) {
			_customer = value;
		} else {
			throw new ContactDetailException("Tried to set customer where driver is already set!", "exception.ContactDetailException.customer");
		}
		
	}
	
	@Override
	public ContactDetailsType getType() { return _type; }
	public void setType(ContactDetailsType value) { _type = value; }

    @Override
    public DI convertToDomain() throws PeriodException
    {
        return this;
    }
    
    @Override
    public boolean equals(Object obj) {
    	if(obj == null) {
    		return false;
    	}
    	
    	if(!(obj instanceof MailContactDetailsDI)) {
    		return false;
    	}
    	
    	MailContactDetailsDI mcd = (MailContactDetailsDI) obj;

    	if(mcd.getCoachTourCustomer() == null && _customer == null) {
    		if(!mcd.getDriver().equals(_driver)) {
    			return false;
    		}
    	} else if(!mcd.getCoachTourCustomer().equals(_customer)) {
    		return false;
    	}
    	
    	if(!mcd.getAdress().equals(_adress) || !mcd.getType().equals(_type)) {
    		return false;
    	}
    	return true;
    }
}
