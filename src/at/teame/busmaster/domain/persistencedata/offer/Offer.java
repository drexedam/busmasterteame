package at.teame.busmaster.domain.persistencedata.offer;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.AdditionalExpenseDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.domaininterface.offer.OfferDI;
import at.teame.busmaster.domain.persistencedata.bus.BusType;
import at.teame.busmaster.domain.persistencedata.coachtour.AdditionalExpense;
import at.teame.busmaster.domain.persistencedata.driver.DriverType;
import at.teame.busmaster.domain.persistencedata.module.Module;

public class Offer implements OfferDI {

	private List<AdditionalExpense> _expenses;
	private List<List<Module>> _modules;
       
	
	public Offer(List<AdditionalExpense> expenses, List<List<Module>> modules) {
		_expenses = expenses == null ? new LinkedList<AdditionalExpense>() : expenses;
		_modules = modules == null ? new LinkedList<List<Module>>() : modules;
	}
	
        
	public int getPrice(List<BusType> bustypes, List<DriverType> drivertypes,  int drivers, int traveltime, int standingtime, int busses, int distance) {
		int preis = 0;
		
		//add price of all modules
		for (List<Module> mod : _modules) {
	        for(Module m : mod) {
	        	preis += m.getPrice();
	        }
		}
		//add all additional expenses to price
		for(AdditionalExpense za : _expenses) {
			preis += za.getPrice();
		}
        
        int avgbusprice = getBusAverage(bustypes);
        
        int avgdriverprice = getDriverAverage(drivertypes, traveltime, standingtime);
        
        preis += avgbusprice * busses;
        preis += avgdriverprice * drivers;
		
		return preis;
	}


	private int getDriverAverage(List<DriverType> drivertypes, int traveltime,
			int standingtime) {
		int avgdriverprice;
		int avgfare = 0;
		int avgstandprice = 0;

		//get all possible fares and standing prices
		for (DriverType dt : drivertypes) {
		    avgfare += dt.getFare();
		    avgstandprice += dt.getStandingPrice();
		}
		//calculate average
		avgdriverprice = (int)(avgfare * (float)(traveltime/60) + avgstandprice * (float)(standingtime/60));
		avgdriverprice = avgdriverprice / drivertypes.size();
		return avgdriverprice;
	}


	private int getBusAverage(List<BusType> bustypes) {
		int avgbusprice = 0;
		
		//get all bustype base prices
		for (BusType bt : bustypes) {
		    avgbusprice += bt.getBasePrice();
		}
		
		//calculate average
		avgbusprice = avgbusprice / bustypes.size();
		return avgbusprice;
	}
	
	@Override
	public List<AdditionalExpense> getExpenses() { return _expenses; }

    @Override
    public List<List<ModuleDI>> getModules() {
        List<List<ModuleDI>> list = new LinkedList<>();
        try {
            for (List<Module> modules : _modules) {
                List<ModuleDI> mod = new LinkedList<>();
                for (Module m : modules) {
                    mod.add((ModuleDI) m.convertToDomain());
                }
                list.add(mod);
            }
        } catch (PeriodException pe) {
            //may not occure
        }
        return list;
    }
/*	@Override
	public List<List<Module>> getModules() { return _modules; } */
	
	public void addOrder(AdditionalExpense value) { _expenses.add(value); }
        
    @Override
    public DI convertToDomain() throws PeriodException
    {
        return this;
    }
    
    @Override
    public boolean equals(Object obj) {
    	if(obj == null) {
    		return false;
    	}
    	
    	if(!(obj instanceof OfferDI)) {
    		return false;
    	}
    	
    	OfferDI o = (OfferDI) obj;
    	
    	
    	if(o.getExpenses().size() != _expenses.size()) {
    		return false;
    	}
    	
    	Iterator<? extends AdditionalExpenseDI> itO = o.getExpenses().iterator();
    	Iterator<? extends AdditionalExpenseDI> it = _expenses.iterator();
    	
    	while(itO.hasNext() && it.hasNext()) {
    		if(!itO.next().equals(it.next())) {
    			return false;
    		}
    	}
    	
    	
    	return false;
    }
}
