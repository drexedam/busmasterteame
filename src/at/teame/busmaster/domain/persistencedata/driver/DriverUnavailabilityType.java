package at.teame.busmaster.domain.persistencedata.driver;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.gruppe2.domain.driver.IDriverUnavailabilityType;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverUnavailabilityTypeDI;

@Entity
@Table(name="FNVTyp")
public class DriverUnavailabilityType implements DriverUnavailabilityTypeDI, IDriverUnavailabilityType {
	@Id @GeneratedValue
	@Column(name="FNVT_ID")
	private int _id;
	@Column(name="FNVT_Bezeichnung")
	private String _specification;
	@OneToMany(mappedBy="_type", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<DriverUnavailability> _fnv = new LinkedList<>();
	
	public DriverUnavailabilityType() {}
	
	public DriverUnavailabilityType(String specification) {
		_specification = specification;
	}
	
	@Override
	public String getSpecification() { return _specification; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof DriverUnavailabilityTypeDI)) {
			return false;
		}
		
		DriverUnavailabilityTypeDI dut = (DriverUnavailabilityTypeDI) obj;
		
		return dut.getSpecification().equals(_specification);
	}

	@Override
	public void setSpecification(String value) {
		_specification = value;		
	}
}
