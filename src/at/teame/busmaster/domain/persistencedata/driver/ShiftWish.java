package at.teame.busmaster.domain.persistencedata.driver;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.ShiftWishDI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftScheduleDI;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftSchedule;

@Entity
@Table(name="FahrerSchichtWunsch")
@NamedQueries(value = {@NamedQuery(name = "ShiftWish.getByDriver", query = "SELECT w FROM ShiftWish w WHERE w._driver = :driver"), 
						@NamedQuery(name = "ShiftWish.deleteByID", query = "DELETE FROM ShiftWish w WHERE w._id = :id")})

public class ShiftWish implements ShiftWishDI {

	@Id @GeneratedValue
	@Column(name="FSW_ID")
	private int _id;
	@Column(name="FSW_Positive")
	private boolean _wished;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "FSW_F_ID")
    private Driver _driver;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "FSW_FSE_ID")
    private DriverShiftSchedule _shift;
	
	public ShiftWish(){}
	
	public ShiftWish(Driver driver, DriverShiftSchedule shift, boolean wished) {
		_driver = driver;
		_shift = shift;
		_wished = wished;
	}
	
	@Override
	public DI convertToDomain() throws PeriodException,
			UncompatibleConvertionException {
		return this;
	}

	@Override
	public boolean getWished() {
		return _wished;
	}
	public void setWished(boolean value) {_wished = value;}
	
	
	@Override
	public DriverDI getDriver() {
		return _driver;
	}
	public void setDriver(Driver value) {_driver = value;}

	@Override
	public DriverShiftScheduleDI getShift() {
		return _shift;
	}
	public void setShift(DriverShiftSchedule value) {_shift = value;}

	@Override
	public int getID() {
		return _id;
	}

}
