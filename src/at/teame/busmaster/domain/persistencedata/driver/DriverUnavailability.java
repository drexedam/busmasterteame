package at.teame.busmaster.domain.persistencedata.driver;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import at.gruppe2.domain.driver.IDriver;
import at.gruppe2.domain.driver.IDriverUnavailability;
import at.gruppe2.domain.driver.IDriverUnavailabilityType;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverUnavailabilityDI;
import javax.persistence.FetchType;

@Entity
@Table(name="FNV")
public class DriverUnavailability implements DriverUnavailabilityDI, IDriverUnavailability {
	@Id @GeneratedValue
	@Column(name="FNV_ID")
	private int _id;
	@Column(name="FNV_Start")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _start;
	@Column(name="FNV_Ende")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _end;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY) 
	@JoinColumn(name="FNV_FNVT_ID")
	private DriverUnavailabilityType _type;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="FNV_F_ID")
	private Driver _driver;
	
	public DriverUnavailability() {}
	
	public DriverUnavailability(Date start, Date end, DriverUnavailabilityType type, Driver driver) throws PeriodException {
		setStart(start);
		setEnd(end);
		setType(type);
		setDriver(driver);
	}
	
	@Override
	public Date getStart() { return _start; }
	@Override
	public Date getEnd() { return _end; }
	@Override
	public DriverUnavailabilityType getType() { return _type; }
	@Override
	public Driver getDriver() { return _driver; }
	
	public void setStart(Date value) throws PeriodException { 
		if(_end != null && value.after(_end)) {
			throw new PeriodException("Start may not be after end.", "exception.PeriodException.startAfter");
		}
		_start = value; 
		
	}
	public void setEnd(Date value) throws PeriodException { 
		if(_start != null && _start.after(value)) {
			throw new PeriodException("Start may not be after end.", "exception.PeriodException.endBefore");
		}
		_end = value; 
		
	}
	public void setType(DriverUnavailabilityType value) { _type = value; }
	public void setDriver(Driver value) { _driver = value; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof DriverUnavailabilityDI)) {
			return false;
		}
		
		DriverUnavailabilityDI du = (DriverUnavailabilityDI) obj;
		
		if(!du.getDriver().equals(_driver)
				|| !du.getEnd().equals(_end)
				|| !du.getStart().equals(_start)
				|| !du.getType().equals(_type)) {
			return false;
		}

		
		return true;
	}

	@Override
	public void setType(IDriverUnavailabilityType value) {
		_type = (DriverUnavailabilityType) value;		
	}

	@Override
	public void setDriver(IDriver value) {
		_driver = (Driver) value;
	}
	
}
