/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.domain.persistencedata.driver;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.CoachTourWishDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourSchedule;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author  Morent Jochen <jochen.morent@students.fhv.at>
 * @org     FHV <fhv.at>
 * @project BusMaster-Team-E
 * @date    30.05.2014
 */
@Entity
@Table(name = "Charterwunsch")
@NamedQueries(value =
    {
        @NamedQuery(name = "CoachTourWish.getByDriver"   , query = "SELECT w FROM CoachTourWish w WHERE w._driver = :driver"),
        @NamedQuery(name = "CoachTourWish.getByCoachTour", query = "SELECT w FROM CoachTourWish w WHERE w._coachTourSchedule = :coachTour"),
        @NamedQuery(name = "CoachTourWish.deleteByID"    , query = "DELETE FROM CoachTourWish w WHERE w._id = :id")
    }
)
public class CoachTourWish implements CoachTourWishDI
{
	@Id @GeneratedValue
	@Column(name="CW_ID")
	private int _id;
    @ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "CW_F_ID")
    private Driver    _driver;
    @ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "CW_CFP_ID")
    private CoachTourSchedule _coachTourSchedule;

    public CoachTourWish() {}
    public CoachTourWish(Driver driver, CoachTourSchedule coachTour)
    {
        _driver = driver;
        _coachTourSchedule = coachTour;
    }

    public CoachTourWish(DriverDI driver, CoachTourScheduleDI cts)
    {
        Driver dTemp = null;
        CoachTourSchedule ctsTemp = null;
        try
        {
            dTemp = (Driver) driver.convertToDomain();
            ctsTemp = (CoachTourSchedule) cts.convertToDomain();
        } catch (PeriodException | UncompatibleConvertionException ex)
        {
            Logger.getLogger(CoachTourWish.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        _driver = dTemp;
        _coachTourSchedule = ctsTemp;
    }

    public int getID()
    {
        return _id;
    }

    public void setDriver(Driver _driver)
    {
        this._driver = _driver;
    }
    
    @Override
    public Driver getDriver()
    {
        return _driver;
    }

    public void setCoachTourSchedule(CoachTourSchedule _coachTour)
    {
        this._coachTourSchedule = _coachTour;
    }

    @Override
    public CoachTourSchedule getCoachTourSchedule()
    {
        return _coachTourSchedule;
    }

    @Override
    public DI convertToDomain() throws PeriodException, UncompatibleConvertionException
    {
        return this;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                            return false;
        if (!(obj instanceof CoachTourWishDI))                      return false;
        CoachTourWishDI other = (CoachTourWishDI) obj;
        if (!Objects.equals(this._driver, other.getDriver()))       return false;
        if (!Objects.equals(this._coachTourSchedule, other.getCoachTourSchedule())) return false;
        
        
                                                                    return true;
    }
}
