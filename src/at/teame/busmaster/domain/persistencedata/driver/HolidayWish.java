package at.teame.busmaster.domain.persistencedata.driver;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.HolidayWishDI;

@Entity
@Table(name= "Urlaubswunsch")
@NamedQueries(value = {@NamedQuery(name = "HolidayWish.getByDriver", query = "SELECT w FROM HolidayWish w WHERE w._driver = :driver"), 
					@NamedQuery(name = "HolidayWish.deleteByID", query = "DELETE FROM HolidayWish w WHERE w._id = :id")})
public class HolidayWish implements HolidayWishDI {
	
	@Id @GeneratedValue
	@Column(name="UW_ID")
	private int _id;
	@Column(name="UW_Start")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _start;
	@Column(name="UW_Ende")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _end;
	@ManyToOne(cascade=CascadeType.PERSIST)
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name="UW_F_ID")
	private Driver _driver;

	public HolidayWish() {}
	public HolidayWish(Date start, Date end, Driver driver) {
		_start = start;
		_end = end;
		_driver = driver;
	}
	
	@Override
	public DI convertToDomain() throws PeriodException,
			UncompatibleConvertionException {
		return this;
	}

	@Override
	public Date getStart() {
		return _start;
	}
	public void setStart(Date value){_start = value;}

	@Override
	public Date getEnd() {
		return _end;
	}
	public void setEnd(Date value) {_end = value;}

	@Override
	public DriverDI getDriver() {
		return _driver;
	}
	public void setDriver(Driver value){_driver = value;}
	@Override
	public int getId() {
		return _id;
	}

}
