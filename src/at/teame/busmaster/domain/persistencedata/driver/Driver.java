package at.teame.busmaster.domain.persistencedata.driver;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.gruppe2.domain.driver.IDriver;
import at.gruppe2.domain.driver.IDriverType;
import at.gruppe2.domain.driver.IDriverUnavailability;
import at.gruppe2.domain.schedule.IDriverShiftSchedule;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.Updateable;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.persistencedata.State;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTour;
import at.teame.busmaster.domain.persistencedata.contactdetails.MailContactDetails;
import at.teame.busmaster.domain.persistencedata.contactdetails.TelContactDetails;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftSchedule;
import at.teame.busmaster.domain.persistencedata.user.User;

@Entity
@Table(name="Fahrer")
public class Driver implements DriverDI, Updateable, IDriver {

	@Id @GeneratedValue
	@Column(name="F_ID")
	private int _id;
	@Column(name="F_VorName")
	private String _firstName;
	@Column(name="F_NachName")
	private String _lastName;
	@Column(name="F_Strasse")
	private String _street;
	@Column(name="F_HausNummer")
	private String _houseNumber;
	@Column(name="F_PLZ")
	private String _zip;
	@Column(name="F_Ort")
	private String _city;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="F_S_ID")
	private State _state;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="F_FT_ID")
	private DriverType _type;
	@OneToMany(mappedBy="_driver", cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<CoachTour> _tours = new LinkedList<>();
	@OneToMany(mappedBy="_additionalDriver", cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<CoachTour> _additionalDriver = new LinkedList<>();
	@OneToMany(mappedBy="_driver", cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<DriverShiftSchedule> _driverSchedule = new LinkedList<>();
	@OneToMany(mappedBy="_driver", cascade=CascadeType.ALL, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<DriverUnavailability> _fnv = new LinkedList<>();
	@OneToMany(mappedBy="_driver", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<MailContactDetails> _mail = new LinkedList<>();
	@OneToMany(mappedBy="_driver", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<TelContactDetails> _tel = new LinkedList<>();
	@OneToMany(mappedBy="_driver", fetch=FetchType.LAZY, cascade=CascadeType.PERSIST, orphanRemoval=true)
	private List<User> _user = new LinkedList<>();
	
	public Driver() {}
	
	public Driver(String firstName, String lastName, String street, String houseNumber, String zip, String city, State state, DriverType type) {
		setFirstName(firstName);
		setLastName(lastName);
		setStreet(street);
		setHouseNumber(houseNumber);
		setZIP(zip);
		setCity(city);
		setState(state);
		setType(type);
	}

	@Override
	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String value) {
		_firstName = value;
	}

	@Override
	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String value) {
		_lastName = value;
	}

	@Override
	public String getStreet() {
		return _street;
	}

	public void setStreet(String value) {
		_street = value;
	}

	@Override
	public String getHouseNumber() {
		return _houseNumber;
	}

	public void setHouseNumber(String value) {
		_houseNumber = value;
	}

	@Override
	public String getZIP() {
		return _zip;
	}

	public void setZIP(String value) {
		_zip = value;
	}

	@Override
	public String getCity() {
		return _city;
	}

	public void setCity(String value) {
		_city = value;
	}

	@Override
	public State getState() {
		return _state;
	}

	public void setState(State value) {
		_state = value;
	}

	@Override
	public DriverType getType() {
		return _type;
	}

	
	public void setType(DriverType value) {
		_type = value;
	}

	@Override
	public boolean isAvailable(Date start, Date end) throws PeriodException {
		
		if(start.after(end)) {
			throw new PeriodException("Start of period may not be after end of period", "exception.PeriodException.availability");
		}
		
		
		for (DriverUnavailability du : _fnv) {
			if(start.after(du.getStart())&&start.before(du.getEnd()) ||
					end.after(du.getStart())&&end.before(du.getEnd()) ||
					du.getStart().after(start)&&du.getStart().before(end) ||
					du.getEnd().after(start)&&du.getEnd().before(end)){
				return false;
			}
		}
		
		Calendar startC= new GregorianCalendar();
		startC.setTime(start);
		Calendar endC = new GregorianCalendar();
		endC.setTime(end);
		Calendar c;
		for(DriverShiftSchedule dss : _driverSchedule){
			c=dss.getDay();
			if(startC.get(Calendar.YEAR)==c.get(Calendar.YEAR) ||
					endC.get(Calendar.YEAR)==c.get(Calendar.YEAR)) {
				if(startC.get(Calendar.DAY_OF_YEAR)<=c.get(Calendar.DAY_OF_YEAR) &&
						endC.get(Calendar.DAY_OF_YEAR) >= c.get(Calendar.DAY_OF_YEAR)) {
					return false;
				}		
			}
		}
		
		for (CoachTour tour : _tours) {
			if(tour.getSchedule().getOrder().getCancelDate()==null){
				if(start.after(tour.getSchedule().getStart())&&start.before(tour.getSchedule().getEnd()) ||
						end.after(tour.getSchedule().getStart())&&end.before(tour.getSchedule().getEnd()) ||
						tour.getSchedule().getStart().after(start)&&tour.getSchedule().getStart().before(end) ||
						tour.getSchedule().getEnd().after(start)&&tour.getSchedule().getEnd().before(end)){
					return false;
				}
			}
		}
		
		return true;
	}
	
	@Override
	public List<CoachTour> getCoachTours() {
		LinkedList<CoachTour> result = new LinkedList<>();
		result.addAll(_tours);
		result.addAll(_additionalDriver);
		return result;
	}

	@Override
	public void update(DI source) throws UncompatibleConvertionException, PeriodException {
		if(!(source instanceof DriverDI)) {
			throw new UncompatibleConvertionException("Source is not of type DriverDI", "exception.UncompatibleConvertionException.driver");
		}
		
		DriverDI s = (DriverDI) source;
		
		setFirstName(s.getFirstName());
		setLastName(s.getLastName());
		setStreet(s.getStreet());
		setHouseNumber(s.getHouseNumber());
		setZIP(s.getZIP());
		setCity(s.getCity());
		setState((State)s.getState().convertToDomain());
		setType((DriverType) s.getType());
		
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}

    @Override
    public String toString()
    {
        return _lastName + " " + _firstName + ": " + _type.getSpecification();
    }
	
    @Override
    public boolean equals(Object obj) {
    	if(obj == null) {
    		return false;
    	}
    	
    	if(!(obj instanceof DriverDI)) {
    		return false;
    	}
    	
    	DriverDI d = (DriverDI) obj;
    	
    	if(!d.getCity().equals(_city) || !d.getFirstName().equals(_firstName) 
    			|| !d.getHouseNumber().equals(_houseNumber) 
    			|| !d.getLastName().equals(_lastName)
    			|| !d.getState().equals(_state)
    			|| !d.getStreet().equals(_street)
    			|| !d.getType().equals(_type)
    			|| !d.getZIP().equals(_zip)) {
    		return false;
    	}
    	
    	return true;
    }

	@Override
	public void setHousenumber(String value) {
		_houseNumber = value;		
	}

	@Override
	public void setType(IDriverType value) {
		_type = (DriverType)value;
	}

	@Override
	public List<? extends IDriverUnavailability> getDriverUnavailabilites() {
		return _fnv;
	}

	@Override
	public List<? extends IDriverShiftSchedule> getIDriverShiftSchedules() {
		return _driverSchedule;
	}

	@Override
	public int getId() {
		return _id;
	}
	
	public boolean isAvailable(Date start, Date end, CoachTourOrderDI order) throws PeriodException {
		
		if(start.after(end)) {
			throw new PeriodException("Start of period may not be after end of period", "exception.PeriodException.availability");
		}
		
		
		for (DriverUnavailability du : _fnv) {
			if(start.after(du.getStart())&&start.before(du.getEnd()) ||
					end.after(du.getStart())&&end.before(du.getEnd()) ||
					du.getStart().after(start)&&du.getStart().before(end) ||
					du.getEnd().after(start)&&du.getEnd().before(end)){
				return false;
			}
		}
		
		Calendar startC= new GregorianCalendar();
		startC.setTime(start);
		Calendar endC = new GregorianCalendar();
		endC.setTime(end);
		Calendar c;
		for(DriverShiftSchedule dss : _driverSchedule){
			c=dss.getDay();
			if(startC.get(Calendar.YEAR)==c.get(Calendar.YEAR) ||
					endC.get(Calendar.YEAR)==c.get(Calendar.YEAR)) {
				if(startC.get(Calendar.DAY_OF_YEAR)<=c.get(Calendar.DAY_OF_YEAR) &&
						endC.get(Calendar.DAY_OF_YEAR) >= c.get(Calendar.DAY_OF_YEAR)) {
					return false;
				}		
			}
		}
		
		for (CoachTour tour : _tours) {
			for(CoachTourDI t : order.getTours()) {
				if(!tour.equals(t) && tour.getSchedule().getOrder().getBookingDate() != null) {
					if(tour.getSchedule().getOrder().getCancelDate()==null){
						if(start.after(tour.getSchedule().getStart())&&start.before(tour.getSchedule().getEnd()) ||
								end.after(tour.getSchedule().getStart())&&end.before(tour.getSchedule().getEnd()) ||
								tour.getSchedule().getStart().after(start)&&tour.getSchedule().getStart().before(end) ||
								tour.getSchedule().getEnd().after(start)&&tour.getSchedule().getEnd().before(end)){
							return false;
						}
					}
				}
			}
		}
		
		return true;
	}
}
