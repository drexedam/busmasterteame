package at.teame.busmaster.domain.persistencedata.driver;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.gruppe2.domain.driver.IDriver;
import at.gruppe2.domain.driver.IDriverType;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverTypeDI;


@Entity
@Table(name="FahrerTyp")
public class DriverType implements DriverTypeDI, IDriverType {
	@Id @GeneratedValue
	@Column(name="FT_ID")
	private int _id;
	@Column(name="FT_FahrPreis")
	private int _fare; //int -> in cent wegen rundungsfehlern
	@Column(name="FT_StehPreis")
	private int _standingPrice;
	@Column(name="FT_Bezeichnung")
	private String _specification;
	@OneToMany(mappedBy="_type", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<Driver> _fahrer = new LinkedList<>();
	
	public DriverType() {}
	
	public DriverType(int fare, int standingPrice, String specification) {
		_fare = fare;
		_standingPrice = standingPrice;
		_specification = specification;
	}
	
	@Override
	public int getFare() { return _fare; }
	public void setFare(int value) {_fare = value; }
	
	@Override
	public int getStandingPrice() { return _standingPrice; }
	public void setStandingPrice(int value) {_standingPrice = value; }
	
	@Override
	public String getSpecification() { return _specification; }
	public void setSpecification(String value) { _specification = value; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof DriverTypeDI)) {
			return false;
		}
		
		DriverTypeDI dt = (DriverTypeDI) obj;
		
		if(dt.getFare() != _fare || dt.getStandingPrice() != _standingPrice 
				|| !dt.getSpecification().equals(_specification)) {
			return false;
		}
		
		return true;
	}

	@Override
	public List<? extends IDriver> getIDrivers() {
		return _fahrer;
	}

	@Override
	public int getId() {
		return _id;
	}
}
