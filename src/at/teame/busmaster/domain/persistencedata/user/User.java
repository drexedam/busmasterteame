package at.teame.busmaster.domain.persistencedata.user;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.user.UserDI;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Table(name= "Benutzer")
@NamedQueries(value = {@NamedQuery(name = "User.getByUserName", query = "SELECT u FROM User u WHERE u._userName = :userName AND u._password = :password")})
public class User implements UserDI {
	@Id @GeneratedValue
	@Column(name="BA_ID")
	private int _id;
	@Column(name="BA_UserName")
	private String _userName;
	@Column(name="BA_Passwort")
	private String _password;
	@Column(name="BA_Berechtigung")
	private String _permition;
	@ManyToOne(cascade=CascadeType.ALL)
	//@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name="BA_F_ID")
	private Driver _driver;
	
	public User(){}
	
	public User(String userName, String password, String permition) {
		_userName = userName;
		_password = password;
		_permition = permition;
	}
	
	@Override
	public String getUserName() { return _userName; }
	
	@Override
	public String getPassword() { return _password; }
	
	@Override
	public String getPermition() { return _permition; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof UserDI)) {
			return false;
		}
		
		UserDI u = (UserDI) obj;
		
		if(!u.getPassword().equals(_password)
				|| !u.getPermition().equals(_permition)
				|| !u.getUserName().equals(_userName)) {
			return false;
		}
		
		return true;
	}

	@Override
	public DriverDI getDriver() {
		return _driver;
	}
	
}
