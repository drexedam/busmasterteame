package at.teame.busmaster.domain.persistencedata.module;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.module.ModuleTypeDI;

@Entity
@Table(name="Modultyp")
public class ModuleType implements ModuleTypeDI{

	@Id @GeneratedValue
	@Column(name="MT_ID")
	private int _id;
	@Column(name="MT_Bezeichnung")
	private String _specific;
	@Column(name="MT_Platzbedarf")
	private int _reqSpace;
	@Column(name="MT_Preis")
	private int _price;
	@OneToMany(mappedBy="_type", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<Module> _tel = new LinkedList<>();
	
	public ModuleType() {	}
	
	public ModuleType(String specification, Integer requiredSpace, Integer price) {
		_specific = specification;
		_reqSpace = requiredSpace;
		_price = price;
	}
	
	@Override
	public String getSpecification() { return _specific; }
	public void setSpecification(String value) { _specific = value; }
	
	@Override
	public int getRequiredSpace() { return _reqSpace; }
	public void setRequiredSpace(int value) { _reqSpace = value; }
	
	@Override
	public int getPrice() { return _price; }
	public void setPrice(int value) { _price = value; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof ModuleTypeDI)) {
			return false;
		}
		
		ModuleTypeDI mt = (ModuleTypeDI) obj;
		
		if(mt.getPrice() != _price
				|| mt.getRequiredSpace() != _reqSpace
				|| !mt.getSpecification().equals(_specific)) {
			return false;
		}
		
		return true;
	}
}
