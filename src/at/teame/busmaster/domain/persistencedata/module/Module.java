package at.teame.busmaster.domain.persistencedata.module;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import java.util.Objects;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Table(name="Modul")
@NamedQueries(value = {@NamedQuery(name = "Modules.getModulesNotInBus", query = "SELECT m FROM Module m WHERE m._bus IS NULL")})
public class Module implements ModuleDI {

	@Id @GeneratedValue
	@Column(name="M_ID")
	private int _id;
	@ManyToOne(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name="M_B_ID")
	private Bus _bus;
	@ManyToOne(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name="M_MT_ID")
	private ModuleType _type;
	
	public Module() { }
	
	public Module(Bus bus, ModuleType type) {
		_bus = bus;
		_type = type;
	}
	
	@Override
	public Bus getBus() { return _bus; }
	public void setBus(Bus value) { _bus = value; }
	
	@Override
	public ModuleType getType() { return _type; }
	public void setType(ModuleType value) { _type = value; }
	
	@Override
	public int getPrice() { return _type.getPrice(); }
	
	@Override
	public String toString(){
		return _type.getSpecification();
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof ModuleDI)) {
			return false;
		}
		
		ModuleDI m = (ModuleDI) obj;
		
		if(!Objects.equals(m.getBus(), _bus) 
				|| !Objects.equals(m.getType(), _type)) {
			return false;
		}
		
		return true;
	}

	@Override
	public int getRequiredSpace() {
		return _type.getRequiredSpace();
	}
}
