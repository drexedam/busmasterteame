package at.teame.busmaster.domain.persistencedata;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourAdress;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourCustomer;
import at.teame.busmaster.domain.persistencedata.driver.Driver;

@Entity
@Table(name="Staaten")
public class State implements StateDI {
	@Id @GeneratedValue
	@Column(name="S_ID")
	private int _id;
	@Column(name="S_Bezeichnung")
	private String _specification;
	@OneToMany(mappedBy="_state", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<CoachTourAdress> _adresses = new LinkedList<>();
	@OneToMany(mappedBy="_state", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<Driver> _drivers = new LinkedList<>();
	@OneToMany(mappedBy="_state", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<CoachTourCustomer> _customers = new LinkedList<>();
	
	public State() {}
	
	public State(String specification) {
		_specification = specification;
	}
	
	@Override
	public String getSpecification() { return _specification; }
	public void setSpecification(String value) { _specification = value; }
	
	@Override
	public String toString(){
		return _specification;
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof StateDI)) {
			return false;
		}
		
		StateDI s = (StateDI) obj;
		
		return s.getSpecification().equals(_specification);
	}
}
