package at.teame.busmaster.domain.persistencedata.coachtour;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.AdditionalExpenseDI;

@Entity
@Table(name="ZusatzAufwendungen")
public class AdditionalExpense implements AdditionalExpenseDI {
	
	@Id @GeneratedValue
	@Column(name="ZA_ID")
	private int _id;
	@Column(name="ZA_Bezeichnung")
	private String _specification;
	@Column(name="ZA_Preis")
	private int _price;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ZA_CA_ID")
	private CoachTourOrder _order;
	
	public AdditionalExpense() {}
	
	public AdditionalExpense(String specification, int price){
		_specification = specification;
		_price = price;
	}
	
	public AdditionalExpense(String specification, int price, CoachTourOrder order) {
		_specification = specification;
		_price = price;
		_order = order;
	}
	
	@Override
	public String getSpecification() { return _specification; }
	public void setSpecification(String value) { _specification = value; }
	
	@Override
	public int getPrice() { return _price; }
	public void setPrice(int value) { _price = value; }
	
	@Override
	public CoachTourOrder getOrder() { return _order; }
	public void setOrder(CoachTourOrder value) { _order = value; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof AdditionalExpenseDI)) {
			return false;
		}
		
		AdditionalExpenseDI ade = (AdditionalExpenseDI) obj;

		if(!(ade.getOrder().equals(_order)) || !(ade.getPrice() == _price) || !(ade.getSpecification().equals(_specification))) {
			return false;
		}
		return true;
	}
}
