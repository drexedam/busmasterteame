package at.teame.busmaster.domain.persistencedata.coachtour;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.Updateable;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.persistencedata.module.Module;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Table(name="CharterfahrtPlanung")
@NamedQueries(
		value={	@NamedQuery(name = "CoachTourSchedule.NotPlaned", query = "SELECT s FROM CoachTourSchedule s WHERE s._tours IS EMPTY"),
                @NamedQuery(name = "CoachTourSchedule.getByID",   query = "SELECT s FROM CoachTourSchedule s WHERE s._id = :id")
              }
)

public class CoachTourSchedule implements CoachTourScheduleDI, Updateable {

	@Id @GeneratedValue
	@Column(name="CFP_ID")
	private int _id;
	@Column(name="CFP_Distanz")
	private int _distance;
	@Column(name="CFP_Fahrzeit")
	private int _travelTime;
	@Column(name="CFP_Stehzeit")
	private int _standingTime;
	@Column(name="CFP_Fahreranzahl")
	private int _numOfDrivers;
	@Column(name="CFP_Busanzahl")
	private int _numOfBusses;
	@Column(name="CFP_Start")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _start;
	@Column(name="CFP_Ende")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _end;
	@Column(name="CFP_Bezeichnung")
	private String _specification;
	@ManyToOne(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name="CFP_CA_ID")
	private CoachTourOrder _order;
	@ManyToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinTable(name="CFP_Mod", joinColumns= {
			@JoinColumn(name="CM_CFP_ID")} , inverseJoinColumns= {
			@JoinColumn(name="CM_M_ID")
	})
	private List<Module> _modules = new LinkedList<>();
	@OneToMany(mappedBy="_schedule", cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<CoachTour> _tours = new LinkedList<>();
	@ManyToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinTable(name="CFP_CAdr",
               joinColumns= {@JoinColumn(name="CC_CFP_ID")},
               inverseJoinColumns= {@JoinColumn(name="CC_CAdr_ID")
	})
	@OrderColumn(name="CC_Reihenfolge")
	private List<CoachTourAdress> _adresses = new LinkedList<>();
	
	public CoachTourSchedule() {}
	
	public CoachTourSchedule(int distance, int travelTime, int standingTime, int numberOfDrivers, int numberOfBusses, Date start, Date end, String specification, List<CoachTourAdress> adresses, List<Module> modules) throws PeriodException {
		setDistance(distance);
		setTravelTime(travelTime);
		setStandingTime(standingTime);
		setNumberOfDrivers(numberOfDrivers);
		setNumberOfBusses(numberOfBusses);
		setStart(start);
		setEnd(end);
		setSpecification(specification);
		setAddresses(adresses);
		setModules(modules);
	}
	
	public CoachTourSchedule(int distance, int travelTime, int standingTime, int numberOfDrivers, int numberOfBusses, Date start, Date end, String specification, CoachTourOrder order, List<CoachTourAdress> adresses) throws PeriodException {
		setDistance(distance);
		setTravelTime(travelTime);
		setStandingTime(standingTime);
		setNumberOfDrivers(numberOfDrivers);
		setNumberOfBusses(numberOfBusses);
		setStart(start);
		setEnd(end);
		setSpecification(specification);
		setOrder(order);
		setAddresses(adresses);
	}
    
    @Override
    public int getID() { return _id; }
    
	@Override
	public int getDistance() { return _distance; }
	public void setDistance(int value) { _distance = value; }
	
	@Override
	public int getTravelTime() { return _travelTime; }
	public void setTravelTime(int value) { _travelTime = value; }
	
	@Override
	public int getStandingTime() { return _standingTime; }
	public void setStandingTime(int value) { _standingTime = value; }
	
	@Override
	public int getNumberOfDrivers() { return _numOfDrivers; }
	public void setNumberOfDrivers(int value) { _numOfDrivers = value; }
	
	@Override
	public int getNumberOfBusses() { return _numOfBusses; }
	public void setNumberOfBusses(int value) { _numOfBusses = value; }
	
	@Override
	public Date getStart() { return _start; }
	public void setStart(Date value) throws PeriodException { 
		if(_end != null && value.after(_end)) {
			throw new PeriodException("Start may not be after end.", "exception.PeriodException.startAfter");
		}
		_start = value; 
	
	}
	
	@Override
	public Date getEnd() { return _end; }
	public void setEnd(Date value) throws PeriodException { 
		if(_start != null && _start.after(value)) {
			throw new PeriodException("Start may not be after end.", "exception.PeriodException.beforeEnd");
		}
		_end = value; 
		
	}
	
	@Override
	public String getSpecification() { return _specification; }
	public void setSpecification(String value) { _specification = value; }
	
	@Override
	public List<Module> getModules() { return _modules;	}
	public void addModule(Module modul) { _modules.add(modul); }
	public void setModules(List<Module> module) { _modules = module; }
	
	@Override
	public CoachTourOrder getOrder() { return _order; }
	
	public void setOrder(CoachTourOrder value) { _order = value; }

	@SuppressWarnings("unchecked")
	@Override
	public void update(DI source) throws UncompatibleConvertionException{
		if(!(source instanceof CoachTourScheduleDI))
			throw new UncompatibleConvertionException("Source is not of type CoachTourSchedule", "exception.UncompatibleConvertionException.coachTourSchedule");
		
		CoachTourScheduleDI s = (CoachTourScheduleDI)source;
		
		setDistance(s.getDistance());
		try {
			setEnd(s.getEnd());
			setStart(s.getStart());
		} catch (PeriodException e) {
			throw new UncompatibleConvertionException("Period Exception Occured", "", e);
		}
		setModules((List<Module>)s.getModules());
		setNumberOfBusses(s.getNumberOfBusses());
		setNumberOfDrivers(s.getNumberOfDrivers());
		//may not change
        //setOrder((CoachTourOrder)s.getOrder());
		setSpecification(s.getSpecification());
		setStandingTime(s.getStandingTime());
		
		setTravelTime(s.getTravelTime());

	}

	
	public void setAddresses(List<CoachTourAdress> value) {
		_adresses = value;
	}
	@Override
	public List<CoachTourAdress> getAdresses() {
		return _adresses;
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}

	@Override
	public List<? extends CoachTourDI> getTours() {
		return _tours;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof CoachTourScheduleDI)) {
			return false;
		}
		
		CoachTourScheduleDI cts = (CoachTourScheduleDI) obj;
		
		if(cts.getDistance() != _distance || !cts.getEnd().equals(_end) 
				|| cts.getNumberOfBusses() != _numOfBusses || cts.getNumberOfDrivers() != _numOfDrivers 
			    || !cts.getSpecification().equals(_specification) //|| !_order.equals(cts.getOrder()) //Weglassen weil order in GUI nicht gesetzt wird
				|| cts.getStandingTime() != _standingTime || !cts.getStart().equals(_start) 
				|| cts.getTravelTime() != _travelTime) {
			return false;
		}
		
		return true;
	}
}
