package at.teame.busmaster.domain.persistencedata.coachtour;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourAdressDI;
import at.teame.busmaster.domain.persistencedata.State;

@Entity
@Table(name="CharterAdresse")
public class CoachTourAdress implements CoachTourAdressDI {

	@Id @GeneratedValue
	@Column(name="CAdr_ID")
	private int _id;
	@Column(name="CAdr_Strasse")
	private String _street;
	@Column(name="CAdr_HausNummer")
	private String _houseNumber;
	@Column(name="CAdr_PLZ")
	private String _zip;
	@Column(name="CAdr_Ort")
	private String _city;
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="CAdr_S_ID")
	private State _state;
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="CFP_CAdr", joinColumns= {
			@JoinColumn(name="CC_CAdr_ID")} , inverseJoinColumns= {
			@JoinColumn(name="CC_CFP_ID")
	})
	@OrderColumn(name="CC_Reihenfolge")	
	private List<CoachTourSchedule> _planungen = new LinkedList<>();
	
	public CoachTourAdress() { }
	
	public CoachTourAdress(String street, String houseNumber, String ZIP, String city, State state) {
		_street = street;
		_houseNumber = houseNumber;
		_zip = ZIP;
		_city = city;
		_state = state;
	}
	
	public CoachTourAdress(String ZIP, String city, State state) {
		this(null, null, ZIP, city, state);
	}
	
	@Override
	public String getStreet() { return _street; }
	public void setStreet(String value) { _street = value; }
	
	@Override
	public String getZIP() { return _zip; }
	public void setZIP(String value) { _zip = value; }
	
	@Override
	public String getHouseNumber() {return _houseNumber; }
	public void setHouseNumber(String value) { _houseNumber = value; }
	
	@Override
	public String getCity() { return _city; }
	public void setCity(String value) { _city = value; }
	
	@Override
	public State getState() { return _state; }
	public void setState(State value) { _state = value; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof CoachTourAdressDI)) {
			return false;
		}
		
		CoachTourAdressDI cta = (CoachTourAdressDI) obj;
		
		if(!cta.getCity().equals(_city) || !cta.getHouseNumber().equals(_houseNumber) 
				|| !cta.getState().equals(_state) || !cta.getStreet().equals(_street) || !cta.getZIP().equals(_zip)) {
			return false;
		}
		
		return true;
	}
}
