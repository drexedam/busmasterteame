package at.teame.busmaster.domain.persistencedata.coachtour;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.Updateable;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.AdditionalExpenseDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import javax.persistence.FetchType;
@NamedQueries(
		value={	@NamedQuery(name = "Order.NotCanceled",                        query = "SELECT o FROM CoachTourOrder o WHERE o._cancelDate is null"),
				@NamedQuery(name = "Order.NotCanceledAndReserved",             query = "SELECT o FROM CoachTourOrder o WHERE o._cancelDate is null AND o._reservationDate is null"),
				@NamedQuery(name = "Order.NotCanceledButReservedAndNotBooked", query = "SELECT o FROM CoachTourOrder o WHERE o._cancelDate is null AND o._reservationDate is not null AND o._bookingDate is null"),
				@NamedQuery(name = "Order.Manageable",                         query = "SELECT o FROM CoachTourOrder o WHERE o._cancelDate is null AND o._reservationDate is not null")})


@Entity
@Table(name="CharterAuftrag")
public class CoachTourOrder implements CoachTourOrderDI, Updateable {
	@Id @GeneratedValue
	@Column(name="CA_ID")
	private int _id;
	@Column(name="CA_Personenzahl")
	private int _numOfPersons;
	@Column(name="CA_ErstellungsDatum")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _creationDate;
	@Column(name="CA_ReservierungsDatum")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _reservationDate;
	@Column(name="CA_BuchungsDatum")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _bookingDate;
	@Column(name="CA_StornierungsDatum")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _cancelDate;
	@Column(name="CA_Bezeichnung")
	private String _specification;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="CA_CK_ID")
	private CoachTourCustomer _customer;
	@OneToMany(mappedBy="_order", cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<CoachTourSchedule> _schedules = new LinkedList<>();
	@OneToMany(mappedBy="_order", cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<AdditionalExpense> _expenses = new LinkedList<>();
	
	public CoachTourOrder() { }
	
	public CoachTourOrder(int numberOfPersons, Date creationDate, Date reservationDate, Date bookingDate, Date cancelDate, String specification, CoachTourCustomer customer) {
		_numOfPersons = numberOfPersons;
		_creationDate = creationDate;
		_reservationDate = reservationDate;
		_bookingDate = bookingDate;
		_cancelDate = cancelDate;
		_specification = specification;
		_customer = customer;
	}
	
	public CoachTourOrder(int numberOfPersons, Date creationDate, Date reservationDate, Date bookingDate, Date cancelDate, String specification, List<CoachTourSchedule> schedules,List<AdditionalExpense>expenses, CoachTourCustomer customer) {
		_numOfPersons = numberOfPersons;
		_creationDate = creationDate;
		_reservationDate = reservationDate;
		_bookingDate = bookingDate;
		_cancelDate = cancelDate;
		_specification = specification;
		_schedules = schedules;
		_expenses = expenses;
		
		for(CoachTourSchedule sch : _schedules){
			sch.setOrder(this);
		}
		
		for(AdditionalExpense addex :_expenses){
			addex.setOrder(this);
		}
		
		_customer = customer;
		
	}
		
	public CoachTourOrder(int numberOfPersons, Date creationDate, String specification, CoachTourCustomer customer) {
		this(numberOfPersons, creationDate, null, null, null, specification, customer);
	}
	
	@Override
	public int getNumberOfPersons() { return _numOfPersons; }
	public void setNumberOfPersons(int value) { _numOfPersons = value; }
	
	@Override
	public Date getCreationDate() { return _creationDate; }
	public void setCreationDate(Date value) { _creationDate = value; }
	
	@Override
	public Date getReservationDate() { return _reservationDate; }
	public void setReservationDate(Date value) { _reservationDate = value; }
	
	@Override
	public Date getBookingDate() { return _bookingDate; }
	public void setBookingDate(Date value) { _bookingDate = value; }
	
	@Override
	public Date getCancelDate() { return _cancelDate; }
	public void setCancelDate(Date value) { _cancelDate = value; }
	
	@Override
	public String getSpecification() { return _specification; }
	public void setSpecification(String value) { _specification = value; }
	
	@Override
	public CoachTourCustomer getCustomer() { return _customer; }
	public void setCustomer(CoachTourCustomer value) { _customer = value; }
	
	@Override
	public List<CoachTourSchedule> getSchedules(){
		return _schedules;
	}
	
	public void setSchedules(List<CoachTourSchedule> planung){
		_schedules=planung;
	}
	
	public void addSchedule(CoachTourSchedule planung){
		_schedules.add(planung);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void update(DI source) throws UncompatibleConvertionException,
                                         PeriodException{
		if(!(source instanceof CoachTourOrderDI))
        {
			throw new UncompatibleConvertionException("Source is not of type CoachTourOrder", "exception.UncompatibleConvertionException.coachTourOrder");
        }
        
		CoachTourOrderDI s = (CoachTourOrderDI)source;
		
        //creating date can not be updated
		//setCreationDate(s.getCreationDate());
		setReservationDate(s.getReservationDate());
        setBookingDate(s.getBookingDate());
		setCancelDate(s.getCancelDate());
		setCustomer((CoachTourCustomer)s.getCustomer().convertToDomain());
		setNumberOfPersons(s.getNumberOfPersons());
		
        List<CoachTourSchedule> lcts = new LinkedList<>();
        
        for(CoachTourScheduleDI cts : s.getSchedules())
        {
            CoachTourSchedule temp = (CoachTourSchedule) cts.convertToDomain();
            temp.setOrder(this);
            
            lcts.add(temp);
        }
        
		setSchedules(lcts);
		setSpecification(s.getSpecification());
	}
	
	public void setExpenses(List<AdditionalExpense> expenses){
		_expenses = expenses;
	}

	@Override
	public List<? extends AdditionalExpenseDI> getExpenses() {
		return _expenses;
	}
	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}

	@Override
	public List<? extends CoachTourDI> getTours() {
		List<CoachTourDI> tours = new LinkedList<>();
		for(CoachTourSchedule s : _schedules) {
			tours.addAll(s.getTours());
		}
		return tours;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof CoachTourOrderDI)) {
			return false;
		}
		
		CoachTourOrderDI cto = (CoachTourOrderDI) obj;
		
		if(cto.getNumberOfPersons() != _numOfPersons || !cto.getSpecification().equals(_specification)
				|| !cto.getCustomer().equals(_customer) || !cto.getCreationDate().equals(_creationDate)) {
			return false;
		}
		
		if(cto.getBookingDate() != null && _bookingDate == null) {
			return false;
		} else if(cto.getBookingDate() != null) {
			if(!cto.getBookingDate().equals(_bookingDate)) {
				return false;
			}
		}
		
		if(cto.getCancelDate() != null && _cancelDate == null) {
			return false;
		} else if(cto.getCancelDate() != null) {
			if(!cto.getCancelDate().equals(_cancelDate)) {
				return false;
			}
		}
		
		if(cto.getReservationDate() != null && _reservationDate == null) {
			return false;
		} else if(cto.getReservationDate() != null) {
			if(!cto.getReservationDate().equals(_reservationDate)) {
				return false;
			}
		}
		
		return true;
	}
}
