package at.teame.busmaster.domain.persistencedata.coachtour;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.Updateable;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import javax.persistence.FetchType;

@Entity
@Table(name="Charterfahrt")
public class CoachTour implements CoachTourDI, Updateable {
	@Id @GeneratedValue
	@Column(name="CF_ID")
	private int _id;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="CF_F_ID")
	private Driver _driver;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="CF_B_ID")
	private Bus _bus;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="CF_zusatz_F_ID")
	private Driver _additionalDriver;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="CharterfahrtPlanung_CFP_ID")
	private CoachTourSchedule _schedule;
	 
	public CoachTour() {} 
	
	public CoachTour(Bus bus){
		_bus = bus;
	}
	 
	public CoachTour(Driver driver, Driver additionalDriver, Bus bus, CoachTourSchedule schedule) {
		_driver = driver;
		_additionalDriver = additionalDriver;
		_bus = bus;
        _schedule = schedule;
	}
	 
	public CoachTour(Driver driver, Bus bus, CoachTourSchedule schedule) {
        this(driver, null, bus, schedule);
	}
	 
	@Override
	public Driver getDriver() { return _driver; }
	public void setDriver(Driver value) { _driver = value; }
	 
	@Override
	public Driver getAdditionalDriver() { return _additionalDriver; }
	public void setAdditionalDriver(Driver value) { _additionalDriver = value; }
	public boolean hasAdditionalDriver() { return _additionalDriver != null; }
	 
	@Override
	public Bus getBus() { return _bus; }
	public void setBus(Bus value) { _bus = value; }
	 
	@Override
	public CoachTourSchedule getSchedule() { return _schedule; }
    public void setSchedule(CoachTourSchedule value) { _schedule = value; } 

    @Override
    public void update(DI source) throws UncompatibleConvertionException
    {
        if(!(source instanceof CoachTourDI))
        {
            throw new UncompatibleConvertionException("source is not from type CoachTourCustomerDI", "exception.UncompatibleConvertionException.coachTourOrder");
        }
        
        try
        {
            CoachTourDI sDI = (CoachTourDI) source;
            this.setBus             ((Bus)              sDI.getBus());
            this.setDriver          ((Driver)           sDI.getDriver());
            this.setSchedule        ((CoachTourSchedule)sDI.getSchedule());
            this.setAdditionalDriver((Driver)           sDI.getAdditionalDriver());
        }
        catch(Exception e)
        {
            throw new UncompatibleConvertionException("parts of the DI were not castable", "", e);
        }
    }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof CoachTourDI)) {
			return false;
		}
		
		CoachTourDI ct = (CoachTourDI)  obj;
		
		if(ct.getAdditionalDriver() == null && _additionalDriver != null) {
			return false;
		} else if(ct.getAdditionalDriver() != null) {
			if(!ct.getAdditionalDriver().equals(_additionalDriver)) {
				return false;
			}
		}
		
		if(_bus !=null && ct.getBus() != null &&!(ct.getBus().equals(_bus))) {
			return false;
		}
		
		if(_driver !=null && ct.getDriver() != null &&!(ct.getDriver().equals(_driver))) {
			return false;
		}
		
		if(!(ct.getSchedule().equals(_schedule))) {
			return false;
		}
		
		
		return true;
	}
}
