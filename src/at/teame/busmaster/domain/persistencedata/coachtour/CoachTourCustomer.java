package at.teame.busmaster.domain.persistencedata.coachtour;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.Updateable;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.MailContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.TelContactDetailsDI;
import at.teame.busmaster.domain.persistencedata.State;
import at.teame.busmaster.domain.persistencedata.contactdetails.ContactDetailException;
import at.teame.busmaster.domain.persistencedata.contactdetails.MailContactDetails;
import at.teame.busmaster.domain.persistencedata.contactdetails.TelContactDetails;

@Entity
@Table(name="CharterKunde")
public class CoachTourCustomer implements CoachTourCustomerDI, Updateable {

	@Id @GeneratedValue
	@Column(name="CK_ID")
	private int _id;
	@Column(name="CK_VorName")
	private String _firstName;
	@Column(name="CK_Nachname")
	private String _lastName;
	@Column(name="CK_JurPers")
	private String _jurPers;
	@Column(name="CK_strasse")
	private String _street;
	@Column(name="CK_HausNummer")
	private String _houseNumber;
	@Column(name="CK_PLZ")
	private String _zip;
	@Column(name="CK_Ort")
	private String _city;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="CK_S_ID")
	private State _state;
	@OneToMany(mappedBy="_customer", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<CoachTourOrder> _orders = new LinkedList<>();
	@OneToMany(mappedBy="_customer", cascade=CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.TRUE)
	private List<MailContactDetails> _mail = new LinkedList<>();
	@OneToMany(mappedBy="_customer", cascade=CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.TRUE)
	private List<TelContactDetails> _tel = new LinkedList<>();
	
	public CoachTourCustomer() {}
	
	public CoachTourCustomer(String firstName, String lastName, String jurPerson, String street, String houseNumber, String ZIP, String city, State state) {
		_firstName = firstName;
		_lastName = lastName;
		_jurPers = jurPerson;
		_street = street;
		_houseNumber = houseNumber;
		_zip = ZIP;
		_city = city;
		_state = state;
	}
	
	public CoachTourCustomer(String firstName, String lastName, String jurPerson, String street, String houseNumber, String ZIP, String city, State state,List<TelContactDetails> phones, List<MailContactDetails> mails) throws ContactDetailException {
		_firstName = firstName;
		_lastName = lastName;
		_jurPers = jurPerson;
		_street = street;
		_houseNumber = houseNumber;
		_zip = ZIP;
		_city = city;
		_state = state;
		_tel = phones;
		_mail = mails;
		
		for(TelContactDetails t: _tel){
			t.setCoachServiceCustomer(this);
		}
		
		for(MailContactDetails m : _mail){
			m.setCoachServiceCustomer(this);
		}
	}
	
	@Override
	public String getFirstName() { return _firstName; }
	public void setFirstName(String value) { _firstName = value; }
	
	@Override
	public String getLastName() { return _lastName; }
	public void setLastName(String value) { _lastName = value; }
	
	
	@Override
	public String getJurPerson() { return _jurPers; }
	public void setJurPerson(String value) { _jurPers = value; }
	
	
	@Override	
	public String getStreet() { return _street; }
	public void setStreet(String value) { _street = value; }
	
	@Override
	public String getHouseNumber() { return _houseNumber; }
	public void setHouseNumber(String value) { _houseNumber = value; }
	
	@Override
	public String getZIP() { return _zip; }
	public void setZIP(String value) { _zip = value; }
	
	@Override
	public String getCity() { return _city; }
	public void setCity(String value) { _city = value; }
	
	@Override
	public State getState() { return _state; }
	public void setState(State value) { _state = value; }

    @Override
    public List<? extends MailContactDetailsDI> getMail() { return _mail; }
    public void setMail(List<MailContactDetails> mail) { _mail = mail; }

    @Override
    public List<? extends TelContactDetailsDI> getTel() { return _tel; }
    public void setTel(List<TelContactDetails> tel) { _tel = tel; }
	
	@Override
	public List<CoachTourOrder> getOrders(){ return _orders; }
	
	public void setOrders(List<CoachTourOrder> auftrag){ _orders=auftrag; }
	public void addOrder(CoachTourOrder auftrag){ _orders.add(auftrag);	}
	
	@Override
	public String toString(){ return _id+" - "+_lastName+" "+_firstName; }

    @Override
    public void update(DI source) throws UncompatibleConvertionException,
                                         PeriodException
    {
        if(!(source instanceof CoachTourCustomerDI))
        {
            throw new UncompatibleConvertionException("source is not from type CoachTourCustomerDI", "exception.UncompatibleConvertionException.customer");
        }
        
        CoachTourCustomerDI sDI = (CoachTourCustomerDI) source;
        this.setCity        (sDI.getCity());
        this.setFirstName   (sDI.getFirstName());
        this.setHouseNumber (sDI.getHouseNumber());
        this.setJurPerson   (sDI.getJurPerson());
        this.setLastName    (sDI.getLastName());

        List<CoachTourOrder> lcto = new LinkedList<>();
        if (sDI.getOrders() != null) {
            for(CoachTourOrderDI ct : sDI.getOrders())
            {
                lcto.add((CoachTourOrder) ct.convertToDomain());
            }
        }

        this.setOrders      (lcto);
        this.setState       ((State)sDI.getState().convertToDomain());
        this.setStreet      (sDI.getStreet());
        this.setZIP         (sDI.getZIP());
    }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof CoachTourCustomerDI)) {
			return false;
		}
		
		CoachTourCustomerDI ctc = (CoachTourCustomerDI) obj;
		
		if(ctc.getJurPerson() == null && _jurPers != null) {
			return false;
		} else if(ctc.getJurPerson() != null) {
			if(!ctc.getJurPerson().equals(_jurPers)) {
				return false;
			}
		}
		
		if(!ctc.getCity().equals(_city) || !ctc.getFirstName().equals(_firstName) || !ctc.getHouseNumber().equals(_houseNumber)
				|| !ctc.getLastName().equals(_lastName) || !ctc.getState().equals(_state) 
				|| !ctc.getStreet().equals(_street) || !ctc.getZIP().equals(_zip)) {
			return false;
		}
		
		return true;
	}
}
