package at.teame.busmaster.domain.persistencedata.schedule;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.gruppe2.domain.schedule.IDayType;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.schedule.DayTypeDI;

@Entity
@Table(name="TagTyp")
public class DayType implements DayTypeDI, IDayType {

	@Id @GeneratedValue
	@Column(name="TT_ID")
	private int _id;
	@Column(name="TT_Bezeichnung")
	private String _specification;
	@Column(name="TT_Beschreibung")
	private String _description;
	@OneToMany(mappedBy="_dayType", fetch=FetchType.LAZY, cascade=CascadeType.ALL,orphanRemoval=true)
	private List<DriverShiftDefinition> _shifts = new LinkedList<>();
	@OneToMany(mappedBy="_dayType", fetch=FetchType.LAZY, cascade=CascadeType.ALL,orphanRemoval=true)
	private List<CircleDefinition> _circle = new LinkedList<>();
	
	public DayType() {}
	
	public DayType(String specification, String description) {
		_specification = specification;
		_description = description;
	}
	
	@Override
	public String getSpecification() { return _specification; }
	public void setSpecification(String value) { _specification = value; }
	
	@Override
	public String getDescription() {  return _description; }
	public void setDescription(String value) { _description = value; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof DayTypeDI)) {
			return false;
		}
		
		DayTypeDI dt = (DayTypeDI) obj;
		
		if(!dt.getDescription().equals(_description)
				|| !dt.getSpecification().equals(_specification)) {
			return false;
		}
		
		return true;
	}
	
}
