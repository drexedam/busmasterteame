package at.teame.busmaster.domain.persistencedata.schedule;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import at.gruppe2.domain.driver.IDriver;
import at.gruppe2.domain.schedule.IDriverShiftDefinition;
import at.gruppe2.domain.schedule.IDriverShiftSchedule;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftScheduleDI;
import at.teame.busmaster.domain.persistencedata.driver.Driver;

import javax.persistence.FetchType;

@Entity
@Table(name="FahrerSchichtEinteilung")
@NamedQueries(value =
{
    @NamedQuery(name = "DriverShiftSchedule.getByDriverAndDate"   , query = "SELECT w FROM DriverShiftSchedule w WHERE w._driver = :driver AND w._date >= :start and w._date <= :end")
}
)
public class DriverShiftSchedule implements DriverShiftScheduleDI, IDriverShiftSchedule {

	@Id @GeneratedValue
	@Column(name="FSE_ID")
	private int _id;
	@Column(name="FSE_Datum")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _date;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="FSE_FSD_ID")
	private DriverShiftDefinition _definition;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="FSE_F_ID")
	private Driver _driver;
	
	public DriverShiftSchedule() {}
	
	public DriverShiftSchedule(Date date, DriverShiftDefinition definition, Driver driver) {
		_date = date;
		_definition = definition;
		_driver = driver;
	}
	
	@Override
	public Date getDate() { return _date; }
	public void setDate(Date value) { _date = value; }
	
	@Override
	public DriverShiftDefinition getDefinition() { return _definition; }
	public void setDefinition(DriverShiftDefinition value) { _definition = value; }
	
	@Override
	public Driver getDriver() { return _driver; }
	public void setDriver(Driver value) { _driver = value; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	public Calendar getDay(){
		Calendar c = new GregorianCalendar();
		c.setTime(_date);
		return c;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof DriverShiftScheduleDI)) {
			return false;
		}
		
		DriverShiftScheduleDI dss = (DriverShiftScheduleDI) obj;
		
		if(!dss.getDate().equals(_date)
				|| !dss.getDefinition().equals(_definition)
				|| !dss.getDriver().equals(_driver)) {
			return false;
		}
		
		return true;
	}

	@Override
	public void setDefinition(IDriverShiftDefinition value) {
		_definition = (DriverShiftDefinition) value;
	}

	@Override
	public void setDriver(IDriver value) {
		_driver = (Driver) value;
	}

	@Override
	public int getID() {
		return _id;
	}
}
