package at.teame.busmaster.domain.persistencedata.schedule;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.gruppe2.domain.publicbustransport.IPublicBusTransport;
import at.gruppe2.domain.schedule.IDayType;
import at.gruppe2.domain.schedule.IDriverShiftDefinition;
import at.gruppe2.domain.schedule.IDriverShiftSchedule;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftDefinitionDI;
import at.teame.busmaster.domain.persistencedata.publicbustransport.PublicBusTransport;

@Entity
@Table(name="FahrerSchichtDefinition")
public class DriverShiftDefinition implements DriverShiftDefinitionDI, IDriverShiftDefinition {
	
	@Id @GeneratedValue
	@Column(name="FSD_ID")
	private int _id;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="FSD_TT_ID")
	private DayType _dayType;
	@Column(name="FSD_Bezeichnung")
	private String _specification;
	@OneToMany(mappedBy="_definition", fetch=FetchType.LAZY, cascade=CascadeType.ALL,orphanRemoval=true)
	private List<DriverShiftSchedule> _schedule = new LinkedList<>();
	@OneToMany(mappedBy="_driverShiftDef", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<PublicBusTransport> _publicBusTransport = new LinkedList<>();
	
	public DriverShiftDefinition() {}
	
	public DriverShiftDefinition(DayType dayType, String specification) {
		_dayType = dayType;
		_specification = specification;
	}
	
	@Override
	public DayType getDayType() { return _dayType; }
	public void setDayType(DayType value) { _dayType = value; }
	
	@Override
	public String getSpecification() { return _specification; }
	public void setSpecification(String value) { _specification = value; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof DriverShiftDefinitionDI)) {
			return false;
		}
		
		DriverShiftDefinitionDI dsd = (DriverShiftDefinitionDI) obj;
		
		if(!dsd.getDayType().equals(_dayType)
				|| !dsd.getSpecification().equals(_specification)) {
			return false;
		}
		
		return true;
	}

	@Override
	public void setDayType(IDayType value) {
		_dayType = (DayType) value;
	}

	@Override
	public List<? extends IDriverShiftSchedule> getIDriverShiftSchedules() {
		return _schedule;
	}

	@Override
	public List<? extends IPublicBusTransport> getIPublicBusTransports() {
		return _publicBusTransport;
	}
}
