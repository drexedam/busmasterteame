package at.teame.busmaster.domain.persistencedata.schedule;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.gruppe2.domain.bus.IBus;
import at.gruppe2.domain.publicbustransport.IPublicBusTransport;
import at.gruppe2.domain.schedule.ICircleDefinition;
import at.gruppe2.domain.schedule.IDayType;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.PublicBusTransportDI;
import at.teame.busmaster.domain.domaininterface.schedule.CircleDefinitionDI;
import at.teame.busmaster.domain.domaininterface.schedule.DayTypeDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.publicbustransport.PublicBusTransport;

@Entity
@Table(name="UmlaufDefinition")
public class CircleDefinition implements CircleDefinitionDI, ICircleDefinition {

	@Id @GeneratedValue
	@Column(name="UD_ID")
	private int _id;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="UD_TT_ID")
	private DayType _dayType;
	@Column(name="UD_Bezeichnung")
	private String _specification;
	@OneToMany(mappedBy="_circleDef", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<PublicBusTransport> _publicBusTransports = new LinkedList<>();
	@OneToMany(mappedBy="_circleDefinition", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<CircleSchedule> _schedules = new LinkedList<>();
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name="UD_B_ID")
    private Bus _defaultBus;

	public CircleDefinition() {}

	public CircleDefinition(DayTypeDI dayType, String specification) {
		_dayType = (DayType) dayType;
		_specification = specification;
	}
	public CircleDefinition(DayTypeDI dayType, String specification, BusDI defaultBus) {
		_dayType = (DayType) dayType;
		_specification = specification;
        _defaultBus = (Bus) defaultBus;
	}

    @SuppressWarnings("unchecked")
	public CircleDefinition(DayTypeDI dayType, String specification, BusDI defaultBus, List<? extends PublicBusTransportDI> publicBusTransports)
    {
        _dayType = (DayType) dayType;
        _specification = specification;
        _defaultBus = (Bus) defaultBus;
        _publicBusTransports = (List<PublicBusTransport>) publicBusTransports;
    }
	
	@Override
	public DayType getDayType() { return _dayType; }
	public void setDayType(DayType value) { _dayType = value; }
	
	@Override
	public String getSpecification() { return _specification; }
	public void setSpecification(String value) { _specification = value; }

    @Override
    public List<PublicBusTransport> getPublicBusTransports()
    {
        return _publicBusTransports;
    }

    @Override
    public Bus getDefaultBus()
    {
        return _defaultBus;
    }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof CircleDefinitionDI)) {
			return false;
		}
		
		CircleDefinitionDI cd = (CircleDefinitionDI) obj;
		
		if(!cd.getDayType().equals(_dayType)
				|| !cd.getSpecification().equals(_specification)) {
			return false;
		}
        if (!Objects.equals(this._defaultBus, cd.getDefaultBus())) {
            return false;
        }
        
        if (this._publicBusTransports.size() != cd.getPublicBusTransports().size()) {
            return false;
        }
        
        boolean containsAll = true;
        for(PublicBusTransportDI pbt : this._publicBusTransports)
        {
            containsAll &= cd.getPublicBusTransports().contains(pbt);
        }
        
        if(!containsAll) {
            return false;
        }
		
		return true;
	}
	@Override
	public IBus getBus() {
		return _defaultBus;
	}

    @Override
    public void setTransports(List<? extends IPublicBusTransport> value) {
        _publicBusTransports = (List<PublicBusTransport>) value;
    }

    @Override
	public void setDayType(IDayType value) {
		_dayType = (DayType) value;
	}
	@Override
	public void setBus(IBus value) {
		_defaultBus = (Bus) value;
	}
	@Override
	public List<? extends IPublicBusTransport> getIPublicBusTransports() {
		return _publicBusTransports;
	}
}
