package at.teame.busmaster.domain.persistencedata.schedule;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import at.gruppe2.domain.bus.IBus;
import at.gruppe2.domain.schedule.ICircleDefinition;
import at.gruppe2.domain.schedule.ICircleSchedule;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.schedule.CircleScheduleDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import javax.persistence.FetchType;

@Entity
@Table(name="UmlaufEinteilung")
public class CircleSchedule implements CircleScheduleDI, ICircleSchedule {
	@Id @GeneratedValue
	@Column(name="UE_ID")
	private int _id;
	@Column(name="UE_Datum")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _date;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="UE_UD_ID")
	private CircleDefinition _circleDefinition;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="UE_B_ID")
	private Bus _bus;
	
	public CircleSchedule() {}
	
	public CircleSchedule(Date date, CircleDefinition definition, Bus bus) {
		_date = date;
		_circleDefinition = definition;
		_bus = bus;
	}
	
	@Override
	public Date getDate() { return _date; }
	public void setDate(Date value) { _date = value; }
	
	@Override
	public CircleDefinition getDefinition() { return _circleDefinition; }
	public void setDefinition(CircleDefinition definition) { _circleDefinition = definition;}
	
	@Override
	public Bus getBus() { return _bus; }
	public void setBus(Bus value) {  _bus = value; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}
	public Calendar getDay(){
		Calendar c = new GregorianCalendar();
		c.setTime(_date);
		return c;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof CircleScheduleDI)) {
			return false;
		}
		
		CircleScheduleDI cs = (CircleScheduleDI) obj;
		
		if(!cs.getBus().equals(_bus)
				|| !cs.getDate().equals(_date)
				|| !cs.getDefinition().equals(_circleDefinition)) {
			return false;
		}
		
		return true;
	}

	@Override
	public void setDefinition(ICircleDefinition value) {
		_circleDefinition = (CircleDefinition) value;
	}

	@Override
	public void setBus(IBus value) {
		_bus = (Bus) value;
	}
	
}
