package at.teame.busmaster.domain.persistencedata.bus;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.gruppe2.domain.bus.IBus;
import at.gruppe2.domain.bus.IBusType;
import at.gruppe2.domain.schedule.ICircleDefinition;
import at.gruppe2.domain.schedule.IDayType;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.Updateable;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTour;
import at.teame.busmaster.domain.persistencedata.module.Module;
import at.teame.busmaster.domain.persistencedata.schedule.CircleDefinition;
import at.teame.busmaster.domain.persistencedata.schedule.CircleSchedule;
import javax.persistence.FetchType;

@Entity
@Table(name="Bus")
public class Bus implements BusDI, Updateable, IBus {

	@Id @GeneratedValue
	@Column(name="B_ID")
	private int _id;
	@Column(name="B_NummernTafel")
	private String _numberPlate;
	@ManyToOne(cascade=CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name="B_BT_ID")
	private BusType _type;
	@OneToMany(mappedBy="_bus", cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<Module> _modules = new LinkedList<>();
	@OneToMany(mappedBy="_bus", cascade=CascadeType.ALL, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<BusUnavailability> _busUnavailabilties = new LinkedList<>();
	@OneToMany(mappedBy="_bus", cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<CoachTour> _coachTours = new LinkedList<>();
	@OneToMany(mappedBy="_bus", cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<CircleSchedule> _schedules = new LinkedList<>();
    @OneToMany(mappedBy="_defaultBus", cascade=CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<CircleDefinition> _definitions = new LinkedList<>();
	
	public Bus() {}
	
	public Bus(String numberPlate, BusType type) {
		_numberPlate = numberPlate;
		_type = type;
		_modules = new LinkedList<>();
	}
	
	
	@Override
	public String getNumberPlate() { return _numberPlate; }
	public void setNumberPlate(String value) { _numberPlate = value; }
	
	@Override
	public BusType getType() { return _type; }
	public void setType(BusType value) { _type = value; }
	
	@Override
	public List<Module> getModules(){
		return _modules;
	}
	public void setModules(List<Module>modul){
		_modules=modul;
	}
	public void addModule(Module modul){
		_modules.add(modul);
	}

	
	@Override
	public void update(DI source) throws UncompatibleConvertionException, PeriodException {
		if(!(source instanceof BusDI)) {
			throw new UncompatibleConvertionException("Source is not of type BusDI", "exception.UncompatibleConvertionException.bus");
		}
		
		BusDI b = (BusDI) source;
		_numberPlate = b.getNumberPlate();
		_type =(BusType) b.getType().convertToDomain();
		_modules = new LinkedList<>();
		for(ModuleDI m : b.getModules()) {
			_modules.add((Module) m.convertToDomain()) ;
		}
	}

	@Override
	public DI convertToDomain() {
		return this;
	}

	@Override
	public boolean isAvailable(Date start, Date end) throws PeriodException {
		
		if(start.after(end)) {
			throw new PeriodException("Start of period may not be after end of period", "exception.PeriodException.availability");
		}
		
		for (BusUnavailability bu : _busUnavailabilties) {
			if(start.after(bu.getStart())&&start.before(bu.getEnd()) ||
					end.after(bu.getStart())&&end.before(bu.getEnd())){
				return false;
			}
		}
		Calendar startC= new GregorianCalendar();
		startC.setTime(start);
		Calendar endC = new GregorianCalendar();
		endC.setTime(end);
		Calendar c;
		for(CircleSchedule cs : _schedules){
			c=cs.getDay();
			if(startC.get(Calendar.YEAR)==c.get(Calendar.YEAR) ||
					endC.get(Calendar.YEAR)==c.get(Calendar.YEAR)) {
				if(startC.get(Calendar.DAY_OF_YEAR)<=c.get(Calendar.DAY_OF_YEAR) &&
						endC.get(Calendar.DAY_OF_YEAR) >= c.get(Calendar.DAY_OF_YEAR)) {
					return false;
				}		
			}
		}
		
		for (CoachTour tour : _coachTours) {
			if(tour.getSchedule().getOrder().getCancelDate()==null){
				if(start.after(tour.getSchedule().getStart())&&start.before(tour.getSchedule().getEnd()) ||
						end.after(tour.getSchedule().getStart())&&end.before(tour.getSchedule().getEnd()) ||
						tour.getSchedule().getStart().after(start)&&tour.getSchedule().getStart().before(end) ||
						tour.getSchedule().getEnd().after(start)&&tour.getSchedule().getEnd().before(end)){
					return false;
				}
			}
		}

		return true;
	}

    @Override
    public String toString()
    {
        return _type.getType() + ": " +  _numberPlate + " ("+getNumOfSeats()+")";
    }
    
    @Override
    public boolean equals(Object obj) {
    	if(obj == null) {
    		return false;
    	}
    	
    	if(!(obj instanceof BusDI)) {
    		return false;
    	}
    	
    	BusDI b = (BusDI) obj;
    	if(!b.getNumberPlate().equals(_numberPlate) || b.getModules().size() != _modules.size() || !b.getType().equals(_type)) {
    		return false;
    	}
    	return true;
    }

	@Override
	public int getNumOfSeats() {
		int total = _type.getSeats();
		for(Module m : _modules) {
			total -= m.getRequiredSpace();
		}
		
		return total;
	}

	public List<CoachTour> getTours(){
		return _coachTours;
	}

	@Override
	public void setType(IBusType value) {
		_type = (BusType) value;
		
	}

    @Override
    public void addCircleDefinition(ICircleDefinition circleDef) {
        _definitions.add((CircleDefinition) circleDef);
    }

	@Override
	public boolean isAvailable(IDayType dayType) {
		for(CircleDefinition cd : _definitions){
			if(cd.getDayType().equals(dayType))
				return false;
		}
		return true;
	}
	
	public boolean isAvailable(Date start, Date end, CoachTourOrderDI order) throws PeriodException {
		if(start.after(end)) {
			throw new PeriodException("Start of period may not be after end of period", "exception.PeriodException.availability");
		}
		
		for (BusUnavailability bu : _busUnavailabilties) {
			if(start.after(bu.getStart())&&start.before(bu.getEnd()) ||
					end.after(bu.getStart())&&end.before(bu.getEnd())){
				return false;
			}
		}
		Calendar startC= new GregorianCalendar();
		startC.setTime(start);
		Calendar endC = new GregorianCalendar();
		endC.setTime(end);
		Calendar c;
		for(CircleSchedule cs : _schedules){
			c=cs.getDay();
			if(startC.get(Calendar.YEAR)==c.get(Calendar.YEAR) ||
					endC.get(Calendar.YEAR)==c.get(Calendar.YEAR)) {
				if(startC.get(Calendar.DAY_OF_YEAR)<=c.get(Calendar.DAY_OF_YEAR) &&
						endC.get(Calendar.DAY_OF_YEAR) >= c.get(Calendar.DAY_OF_YEAR)) {
					return false;
				}		
			}
		}
		
		for (CoachTour tour : _coachTours) {
			for(CoachTourDI t : order.getTours()) {
				if(!tour.equals(t) && tour.getSchedule().getOrder().getBookingDate() != null) {
					if(tour.getSchedule().getOrder().getCancelDate()==null){
						if(start.after(tour.getSchedule().getStart())&&start.before(tour.getSchedule().getEnd()) ||
								end.after(tour.getSchedule().getStart())&&end.before(tour.getSchedule().getEnd()) ||
								tour.getSchedule().getStart().after(start)&&tour.getSchedule().getStart().before(end) ||
								tour.getSchedule().getEnd().after(start)&&tour.getSchedule().getEnd().before(end)){
							return false;
						}
					}
				}
			}
		}
	
		return true;
	}
}
