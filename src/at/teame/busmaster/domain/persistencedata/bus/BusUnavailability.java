package at.teame.busmaster.domain.persistencedata.bus;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import at.gruppe2.domain.bus.IBus;
import at.gruppe2.domain.bus.IBusUnavailability;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusUnavailabilityDI;

@Entity
@Table(name="BNV")
public class BusUnavailability implements BusUnavailabilityDI, IBusUnavailability {

	@Id @GeneratedValue
	@Column(name="BNV_ID")
	private int _id;
	@Column(name="BNV_Start")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _start;
	@Column(name="BNV_Ende")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _end;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="BNV_B_ID")
	private Bus _bus;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="BNV_BNVT_ID")
	private BusUnavailabilityType _type;
	
	public BusUnavailability() {}
	
	public BusUnavailability(Date start, Date end, Bus bus, BusUnavailabilityType type) throws PeriodException {
		setStart(start);
		setEnd(end);
		setBus(bus);
		setType(type);
	}
	
	@Override
	public Date getStart() { return _start; }
	public void setStart(Date value) throws PeriodException { 
		if(_end != null && value.after(_end)) {
			throw new PeriodException("Start may not be after end.", "exception.PeriodException.startAfter");
		}
		_start = value; 
		
	}
	
	@Override
	public Date getEnd() { return _end; }
	public void setEnd(Date value) throws PeriodException { 
		if(_start != null && _start.after(value)) {
			throw new PeriodException("Start may not be after end.", "exception.PeriodException.endBefore");
		}
		_end = value; 
		
	}
	
	@Override
	public Bus getBus() { return _bus; }
	public void setBus(Bus value) { _bus = value; }
	
	@Override
	public BusUnavailabilityType getType() { return _type; }
	public void setType(BusUnavailabilityType value) { _type = value; }

	@Override
	public DI convertToDomain() {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof BusUnavailabilityDI)) {
			return false;
		}
		
		BusUnavailabilityDI bu = (BusUnavailabilityDI) obj;
		if(!(bu.getBus().equals(_bus)) 
				|| !(bu.getEnd().equals(_end)) || !(bu.getStart().equals(_start)) 
				|| !(bu.getType().equals(_type))) {
			return false;
		}
		return true;
	}

	@Override
	public void setBus(IBus value) {
		_bus= (Bus)value;
	}
}
