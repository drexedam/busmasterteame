package at.teame.busmaster.domain.persistencedata.bus;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.gruppe2.domain.bus.IBusType;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusTypeDI;


@Entity
@Table(name="BusTyp")
public class BusType implements BusTypeDI, IBusType {

	@Id @GeneratedValue
	@Column(name="BT_ID")
	private int _id;
	@Column(name="BT_Stehplaetze")
	private int _standingPlaces;
	@Column(name="BT_Sitzplaetze")
	private int _seats;
	@Column(name="BT_Art")
	private String _type;
	@Column(name="BT_GrundPreis")
	private int _basePrice;
	@Column(name="BT_KMPreis")
	private int _kmPrice;
	@OneToMany(mappedBy="_type", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<Bus> _busses = new LinkedList<>();
	
	public BusType() {}
	
	public BusType(int standingPlaces, int seats, String type, int basePrice, int kmPrice) {
		_standingPlaces = standingPlaces;
		_seats = seats;
		_type = type;
		_basePrice = basePrice;
		_kmPrice = kmPrice;
	}
	
	@Override
	public int getStandingPlaces() { return _standingPlaces; }
	public void setStandingPlaces(int value) { _standingPlaces = value; }
	
	@Override
	public int getSeats() { return _seats; }
	public void setSeats(int value) { _seats = value; }
	
	@Override
	public String getType() { return _type; }
	public void setType(String value) { _type = value; }
	
	@Override
	public int getBasePrice() { return _basePrice; }
	public void setBasePrice(int value) { _basePrice = value; }
	
	@Override
	public int getKMPrice() { return _kmPrice; }
	public void setKMPrice(int value) { _kmPrice = value; }

	@Override
	public DI convertToDomain() {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof BusTypeDI)) {
			return false;
		}
		
		BusTypeDI bType = (BusTypeDI) obj;
		if(!(bType.getBasePrice() == _basePrice) || !(bType.getKMPrice() == _kmPrice) 
				|| !(bType.getSeats() == _seats) || !(bType.getStandingPlaces() == _standingPlaces 
				|| !(bType.getType().equals(_type)))) {
			return false;
		}
		return true;
	}

    @Override
    public String toString()
    {
        return _type + "[" + "standing places: " + _standingPlaces + ", seats: " + _seats + ", baseprice: " + ((double)_basePrice)/100 + ", km price:" + ((double)_kmPrice)/100 + ']';
    }
	
}
