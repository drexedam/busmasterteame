package at.teame.busmaster.domain.persistencedata.bus;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import at.gruppe2.domain.bus.IBusUnavailabilityType;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusUnavailabilityTypeDI;

@Entity
@Table(name="BNVTyp")
public class BusUnavailabilityType implements BusUnavailabilityTypeDI, IBusUnavailabilityType {
	
	@Id @GeneratedValue
	@Column(name="BNVT_ID")
	private int _id;
	@Column(name="BNVT_Bezeichnung")
	private String _specification;
	@OneToMany( mappedBy="_type", fetch=FetchType.LAZY, cascade=CascadeType.ALL,orphanRemoval=true)
	private List<BusUnavailability> _busUnavailabilities = new LinkedList<>();
	
	public BusUnavailabilityType() {}
	
	public BusUnavailabilityType(String specification) {
		_specification = specification;
	}
	
	@Override
	public String getSpecification() { return _specification; }
	public void setSpecification(String value) { _specification = value; }
	

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof BusUnavailabilityTypeDI)) {
			return false;
		}
		
		
		BusUnavailabilityTypeDI but = (BusUnavailabilityTypeDI) obj;
		
		if(!(but.getSpecification().equals(_specification))) {
			return false;
		}
		
		return true;
	}
}
