package at.teame.busmaster.domain.persistencedata.publicbustransport;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.gruppe2.domain.publicbustransport.IRoute;
import at.gruppe2.domain.publicbustransport.IRouteSection;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.Updateable;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteSectionDI;

@Entity
@Table(name="Linie")
public class Route implements RouteDI, IRoute, Updateable {

	@Id @GeneratedValue
	@Column(name="L_ID")
	private int _id;
	@Column(name="L_Nummer")
	private String _number;
	@Column(name="L_Bezeichnung")
	private String _spec;
	@ManyToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinTable(name="L_SA", joinColumns={
			@JoinColumn(name="LSA_L_ID")}, inverseJoinColumns={
			@JoinColumn(name="LSA_SA_ID")
	})
	@OrderColumn(name="LSA_Sortierung")
	private List<RouteSection> _sections = new LinkedList<>();
	
	public Route() {}
	
	public Route(String number, String spec, List<RouteSection> sections) {
		setNumber(number);
		setSpecification(spec);
		setSection(sections);
	}
	
	@Override
	public DI convertToDomain() throws PeriodException,
			UncompatibleConvertionException {
		return this;
	}

	@Override
	public String getNumber() {
		return _number;
	}
	public void setNumber(String value) { _number = value; }

	@Override
	public String getSpecification() {
		return _spec;
	}
	public void setSpecification(String value) {_spec = value; }
	
	
	@Override
	public List<? extends RouteSectionDI> getSections() {
		return _sections;
	}
	public void setSection(List<RouteSection> value) { _sections = value; }
	public void addSection(RouteSection value) {
		_sections.add(value);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        if (!(obj instanceof RouteDI))
            return false;
        RouteDI other = (RouteDI) obj;
        if (!Objects.equals(this._number, other.getNumber()))
            return false;
        if (!Objects.equals(this._spec, other.getSpecification()))
            return false;
        
        if (this._sections.size() != other.getSections().size()) {
            return false;
        }
        
        boolean containsAll = true;
        for(RouteSectionDI pbt : this._sections)
        {
            containsAll &= other.getSections().contains(pbt);
        }
        
        if(!containsAll) {
            return false;
        }
        
        return true;
    }

	@Override
	public void addSection(IRouteSection value) {
		_sections.add((RouteSection) value);
	}

	@Override
	public void addSection(IRouteSection value, int index) {
		_sections.add(index, (RouteSection) value);
	}

	@Override
	public void removeSection(IRouteSection value) {
		_sections.remove((RouteSection)value);
	}

	@Override
	public void removeSection(int index) {
		_sections.remove(index);
	}

	@Override
	public List<? extends IRouteSection> getAllSections() {
		return _sections;
	}

	@Override
	public void update(DI source) throws UncompatibleConvertionException,
			PeriodException {
		if(!(source instanceof RouteDI)) {
			throw new UncompatibleConvertionException("Source is not of type RouteDI", "exception.UncompatibleConvertionException.route");
		}
		
		RouteDI r = (RouteDI) source;
		_number = r.getNumber();
		_spec = r.getSpecification();
		
		_sections = new LinkedList<>();
		for(RouteSectionDI s : r.getSections()) {
			_sections.add((RouteSection) s.convertToDomain()) ;
		}
		
	}
}
