package at.teame.busmaster.domain.persistencedata.publicbustransport;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.gruppe2.domain.publicbustransport.IRouteSection;
import at.gruppe2.domain.publicbustransport.IStation;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteSectionDI;

@Entity
@Table(name="Streckenabschnitt")
public class RouteSection implements RouteSectionDI, IRouteSection {
	
	@Id @GeneratedValue
	@Column(name="SA_ID")
	private int _id;
	@Column(name="SA_Distanz")
	private int _dist;
	@Column(name="SA_Fahrzeit")
	private int _driveTime;
	@ManyToOne(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name="SA_Startstation")
	private Station _start;
	@ManyToOne(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.TRUE)
	@JoinColumn(name="SA_Endstation")
	private Station _end;

	public RouteSection() {}
	
	public RouteSection(int distance, int drivingTime, Station start, Station end) {
		setDistance(distance);
		setDrivingTime(drivingTime);
		setStart(start);
		setEnd(end);
	}
	
	@Override
	public DI convertToDomain() throws PeriodException,
			UncompatibleConvertionException {
		return this;
	}

	@Override
	public int getDistance() {
		return _dist;
	}
	public void setDistance(int value) {_dist = value; }
	
	@Override
	public int getDrivingTime() {
		return _driveTime;
	}
	public void setDrivingTime(int value) {_driveTime = value; }

	@Override
	public Station getStart() {
		return _start;
	}
	public void setStart(Station value) {_start = value; }

	@Override
	public Station getEnd() {
		return _end;
	}
	public void setEnd(Station value) {_end = value; }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        if (!(obj instanceof RouteSectionDI))
            return false;
        RouteSectionDI other = (RouteSectionDI) obj;
        if (this._dist != other.getDistance())
            return false;
        if (this._driveTime != other.getDrivingTime())
            return false;
        if (!Objects.equals(this._start, other.getStart()))
            return false;
        if (!Objects.equals(this._end, other.getEnd()))
            return false;
        return true;
    }

	@Override
	public void setStart(IStation value) {
		setStart((Station) value);
	}

	@Override
	public void setEnd(IStation value) {
		setEnd((Station) value);
	}
	
	
}
