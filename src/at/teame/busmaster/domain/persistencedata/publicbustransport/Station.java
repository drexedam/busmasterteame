package at.teame.busmaster.domain.persistencedata.publicbustransport;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import at.gruppe2.domain.publicbustransport.IStation;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.StationDI;

@Entity
@Table(name="Station")
public class Station implements StationDI, IStation {

	@Id @GeneratedValue
	@Column(name="S_ID")
	private int _id;
	@Column(name="S_Nummer")
	private String _number;
	@Column(name="S_Name")
	private String _name;
	@Column(name="S_Kurzebezeichnung")
	private String _desc;
	
	public Station(){}
	
	public Station(String number, String name, String desc) {
		setNumber(number);
		setName(name);
		setShortDesc(desc);
	}
	
	@Override
	public DI convertToDomain() throws PeriodException,
			UncompatibleConvertionException {
		return this;
	}

	@Override
	public String getNumber() {
		return _number;
	}
	
	public void setNumber(String value) {_number = value; }

	@Override
	public String getName() {
		return _name;
	}
	
	public void setName(String value) {_name = value; }

	@Override
	public String getShortDesc() {
		return _desc;
	}
	
	public void setShortDesc(String value){_desc = value;}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        if (!(obj instanceof StationDI))
            return false;
        StationDI other = (StationDI) obj;
        if (!Objects.equals(this._number, other.getNumber()))
            return false;
        if (!Objects.equals(this._name, other.getName()))
            return false;
        if (!Objects.equals(this._desc, other.getShortDesc()))
            return false;
        return true;
    }
	
}
