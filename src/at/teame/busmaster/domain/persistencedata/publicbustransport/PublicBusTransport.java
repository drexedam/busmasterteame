package at.teame.busmaster.domain.persistencedata.publicbustransport;

import java.util.Date;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import at.gruppe2.domain.publicbustransport.IPublicBusTransport;
import at.gruppe2.domain.publicbustransport.IRoute;
import at.gruppe2.domain.publicbustransport.IRouteSection;
import at.gruppe2.domain.schedule.ICircleDefinition;
import at.gruppe2.domain.schedule.IDayType;
import at.gruppe2.domain.schedule.IDriverShiftDefinition;
import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.PublicBusTransportDI;
import at.teame.busmaster.domain.persistencedata.schedule.CircleDefinition;
import at.teame.busmaster.domain.persistencedata.schedule.DayType;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftDefinition;
import javax.persistence.FetchType;

@Entity
@Table(name="Linienfahrt")
public class PublicBusTransport implements PublicBusTransportDI,IPublicBusTransport {
	
	public static final int MILLISECONDS_PER_MINUTE = 60000; 
	
	@Id @GeneratedValue
	@Column(name="LF_ID")
	private int _id;
	@Column(name="LF_Start")
	@Temporal(TemporalType.TIMESTAMP)
	private Date _start;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="LF_UD_ID")
	private CircleDefinition _circleDef;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="LF_FSD_ID")
	private DriverShiftDefinition _driverShiftDef;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="LF_TT_ID")
	private DayType _dayType;
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="LF_L_ID")
	private Route _route;
	
	public PublicBusTransport() {}
	
	public PublicBusTransport(Date start, CircleDefinition circle, DriverShiftDefinition shift, DayType dayType, Route route) throws PeriodException {
		setStart(start);
		setCircleDefinition(circle);
		setShiftDefinition(shift);
		setDayType(dayType);
		setRoute(route);
	}
	
	private void setDayType(DayType dayType) {
		_dayType = dayType;
	}
	
	private void setRoute(Route route) {
		_route = route;
	}

	@Override
	public Date getStart() { return _start; }
	public void setStart(Date value) throws PeriodException { 
		_start = value; 
		
	}
	
	
	@Override
	public CircleDefinition getCircleDefinition() { return _circleDef; }
	public void setCircleDefinition(CircleDefinition value) { _circleDef = value; }
	
	@Override
	public DriverShiftDefinition getShiftDefinition() { return _driverShiftDef; }
	public void setShiftDefinition(DriverShiftDefinition value) { _driverShiftDef = value; }

	@Override
	public DI convertToDomain() throws PeriodException {
		return this;
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        if (!(obj instanceof PublicBusTransportDI))
            return false;
        PublicBusTransportDI other = (PublicBusTransportDI) obj;
        if (!Objects.equals(this._start, other.getStart()))
            return false;
        if (!Objects.equals(this._circleDef, other.getCircleDefinition()))
            return false;
        if (!Objects.equals(this._driverShiftDef, other.getShiftDefinition()))
            return false;
        if (!Objects.equals(this._dayType, other.getDayType()))
            return false;
        if (!Objects.equals(this._route, other.getRoute()))
            return false;
        return true;
    }

	@Override
	public DayType getDayType() {
		return _dayType;
	}

	@Override
	public Route getRoute() {
		return _route;
	}

	@Override
	public void setCircleDefinition(ICircleDefinition value) {
		_circleDef = (CircleDefinition) value;
	}

	@Override
	public void setShiftDefinition(IDriverShiftDefinition value) {
		_driverShiftDef = (DriverShiftDefinition) value;
	}

	@Override
	public void setDayType(IDayType value) {
		_dayType = (DayType) value;
	}

	@Override
	public void setRoute(IRoute value) {
		_route = (Route) value;
	}

	@Override
	public Date getEnd() {
		int drivingTime = 0;
		for(IRouteSection rs: _route.getAllSections()){
			
			drivingTime += rs.getDrivingTime();
		}
		return new Date(_start.getTime()+(MILLISECONDS_PER_MINUTE*drivingTime));
	}
}
