package at.teame.busmaster.domain.guidata.coachtour;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.MailContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.TelContactDetailsDI;
import at.teame.busmaster.domain.persistencedata.State;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourCustomer;
import at.teame.busmaster.domain.persistencedata.contactdetails.ContactDetailException;
import at.teame.busmaster.domain.persistencedata.contactdetails.MailContactDetails;
import at.teame.busmaster.domain.persistencedata.contactdetails.TelContactDetails;

public class GuiCoachTourCustomer implements CoachTourCustomerDI{
	
	private String _firstName;
	private String _lastName;
	private String _jurPerson;
	private String _street;
	private String _houseNumber;
	private String _zip;
	private String _city;
	private StateDI _state;
	private List<CoachTourOrderDI> _orders;
	private List<TelContactDetailsDI> _telephones;
	private List<MailContactDetailsDI> _mails;
	
	public GuiCoachTourCustomer(String firstName, String lastName, String jurPerson, String street, String houseNumber, String zip, String city, StateDI state, List<TelContactDetailsDI> phones, List<MailContactDetailsDI> mails){
		_firstName = firstName;
		_lastName =lastName;
		_jurPerson = jurPerson;
		_street = street;
		_houseNumber = houseNumber;
		_zip = zip;
		_city = city;
		_state =state;
		_telephones = phones != null ? (List<TelContactDetailsDI>)phones : new LinkedList<TelContactDetailsDI>();
		_mails = mails != null ? (List<MailContactDetailsDI>)mails : new LinkedList<MailContactDetailsDI>();
	}
	
	public GuiCoachTourCustomer(String firstName, String lastName, String jurPerson, String street, String houseNumber, String zip, String city, StateDI state, List<CoachTourOrderDI> orders){
		_firstName = firstName;
		_lastName =lastName;
		_jurPerson = jurPerson;
		_street = street;
		_houseNumber = houseNumber;
		_zip = zip;
		_city = city;
		_state =state;
		_orders = orders;
	}

	@Override
	public String getFirstName() {
		return _firstName;
	}

	@Override
	public String getLastName() {
		return _lastName;
	}

	@Override
	public String getJurPerson() {
		return _jurPerson;
	}

	@Override
	public String getStreet() {
		return _street;
	}

	@Override
	public String getHouseNumber() {
		return _houseNumber;
	}

	@Override
	public String getZIP() {
		return _zip;
	}

	@Override
	public String getCity() {
		return _city;
	}

	@Override
	public StateDI getState() {
		return _state;
	}

	@Override
	public List<? extends CoachTourOrderDI> getOrders() {
		return _orders;
	}

    @Override
    public List<? extends MailContactDetailsDI> getMail() { return _mails; }
    public void setMail(List<MailContactDetailsDI> mail) { _mails = mail; }

    @Override
    public List<? extends TelContactDetailsDI> getTel() { return _telephones; }
    public void setTel(List<TelContactDetailsDI> tel) { _telephones = tel; }

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		List<TelContactDetails> phones = new LinkedList<>();
		List<MailContactDetails> mails = new LinkedList<>();
		
		for(TelContactDetailsDI t : _telephones){
			phones.add((TelContactDetails)t.convertToDomain());
		}
		
		for(MailContactDetailsDI m : _mails){
			mails.add((MailContactDetails)m.convertToDomain());
		}
		
		
		try {
			return new CoachTourCustomer(_firstName, _lastName, _jurPerson, _street, _houseNumber, _zip, _city, (State)_state.convertToDomain(), phones, mails);
		} catch (ContactDetailException e) {
			// imposible in this way of use
		}
		
		return null;
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                return false;
        if (!(obj instanceof CoachTourCustomerDI))                      return false;
        
        CoachTourCustomerDI other = (CoachTourCustomerDI) obj;
        
        if (!Objects.equals(this._firstName, other.getFirstName()))     return false;
        if (!Objects.equals(this._lastName, other.getLastName()))       return false;
        if (!Objects.equals(this._jurPerson, other.getJurPerson()))     return false;
        if (!Objects.equals(this._street, other.getStreet()))           return false;
        if (!Objects.equals(this._houseNumber, other.getHouseNumber())) return false;
        if (!Objects.equals(this._zip, other.getZIP()))                 return false;
        if (!Objects.equals(this._city, other.getCity()))               return false;
        if (!Objects.equals(this._state, other.getState()))             return false;
        
        return true;
    }

}
