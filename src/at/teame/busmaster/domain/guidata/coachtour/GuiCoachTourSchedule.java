package at.teame.busmaster.domain.guidata.coachtour;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourAdressDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourAdress;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourSchedule;
import at.teame.busmaster.domain.persistencedata.module.Module;

public class GuiCoachTourSchedule implements CoachTourScheduleDI {

	private int _distance;
	private int _travelTime;
	private int _standingTime;
	private int _numberOfDrivers;
	private int _numberOfBusses;
	private Date _start;
	private Date _end;
	private String _specification;
	private List<ModuleDI> _modules;
	private CoachTourOrderDI _order;
	private List<CoachTourAdressDI> _adresses;
	private List<CoachTourDI> _tours;
	
	@SuppressWarnings("unchecked")
	public GuiCoachTourSchedule(int distance, int travelTime, int standingTime, int numberOfDrivers, int numberOfBusses, Date start, Date end, String specification, List<? extends ModuleDI> modules, CoachTourOrderDI order, List<? extends CoachTourAdressDI> adresses, List<? extends CoachTourDI> tours){
		_distance = distance;
		_travelTime = travelTime;
		_standingTime = standingTime;
		_numberOfDrivers = numberOfDrivers;
		_numberOfBusses = numberOfBusses;
		_start = start;
		_end = end;
		_specification = specification;
		_modules = (List<ModuleDI>) (modules != null ? modules : new LinkedList<ModuleDI>());
		_order = order;
		_adresses = (List<CoachTourAdressDI>) adresses;
		_tours = (List<CoachTourDI>) tours;
	}
	
	@Override
	public int getDistance() {
		return _distance;
	}

	@Override
	public int getTravelTime() {
		return _travelTime;
	}

	@Override
	public int getStandingTime() {
		return _standingTime;
	}

	@Override
	public int getNumberOfDrivers() {
		return _numberOfDrivers;
	}

	@Override
	public int getNumberOfBusses() {
		return _numberOfBusses;
	}

	@Override
	public Date getStart() {
		return _start;
	}

	@Override
	public Date getEnd() {
		return _end;
	}

	@Override
	public String getSpecification() {
		return _specification;
	}

	@Override
	public List<? extends ModuleDI> getModules() {
		return _modules;
	}

	@Override
	public CoachTourOrderDI getOrder() {
		return _order;
	}

	@Override
	public List<? extends CoachTourAdressDI> getAdresses() {
		return _adresses;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		List<Module> modules = new LinkedList<>();
		List<CoachTourAdress> addresses = new LinkedList<>();
		
		for(ModuleDI m : _modules){
			modules.add((Module)m.convertToDomain());
		}
		
		for(CoachTourAdressDI add : _adresses){
			addresses.add((CoachTourAdress)add.convertToDomain());
		}
		
		return new CoachTourSchedule(_distance, _travelTime, _standingTime, _numberOfDrivers, _numberOfBusses, _start, _end, _specification, addresses, modules);
	}

	@Override
	public List<? extends CoachTourDI> getTours() {
		return _tours;
	}
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                    return false;
        if (!(obj instanceof CoachTourScheduleDI))                          return false;
        
        CoachTourScheduleDI other = (CoachTourScheduleDI) obj;
        
        if (this._distance != other.getDistance())                          return false;
        if (this._travelTime != other.getTravelTime())                      return false;
        if (this._standingTime != other.getStandingTime())                  return false;
        if (this._numberOfDrivers != other.getNumberOfDrivers())            return false;
        if (this._numberOfBusses != other.getNumberOfBusses())              return false;
        if (!Objects.equals(this._start, other.getStart()))                 return false;
        if (!Objects.equals(this._end, other.getEnd()))                     return false;
        if (!Objects.equals(this._specification, other.getSpecification())) return false;
        if (!Objects.equals(this._order, other.getOrder()))                 return false;
        
        return true;
    }

    @Override
    public int getID()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
