package at.teame.busmaster.domain.guidata.coachtour;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourAdressDI;
import at.teame.busmaster.domain.persistencedata.State;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourAdress;

public class GuiCoachTourAddress implements CoachTourAdressDI {
	
	private String _street;
	private String _zip;
	private String _houseNumber;
	private String _city;
	private StateDI _state;
	
	public GuiCoachTourAddress(String street, String zip, String houseNumber, String city, StateDI state){
		_street = street;
		_zip = zip;
		_houseNumber = houseNumber;
		_city = city;
		_state = state;
	}

	@Override
	public String getStreet() {
		return _street;
	}

	@Override
	public String getZIP() {
		return _zip;
	}

	@Override
	public String getHouseNumber() {
		return _houseNumber;
	}

	@Override
	public String getCity() {
		return _city;
	}

	@Override
	public StateDI getState() {
		return _state;
	}

    @Override
    public String toString() {
        return  _street + " " + _houseNumber + " " + _zip + " " + _city + " " + _state;
    }

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		return new CoachTourAdress(_street, _houseNumber, _zip, _city, (State)_state.convertToDomain());
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                     return false;
        if (!(obj instanceof CoachTourAdressDI))                             return false;
        
        CoachTourAdressDI other = (CoachTourAdressDI) obj;
        
        if (this._street == null && other.getStreet() != null)               return false;
        else
        {
            if (this._street != null && other.getStreet() == null)           return false;
            if (!Objects.equals(this._street, other.getStreet()))            return false;
        }
        if (!Objects.equals(this._zip, other.getZIP()))                      return false;
        if (this._houseNumber == null && other.getHouseNumber() != null)     return false;
        else
        {
            if (this._houseNumber != null && other.getHouseNumber() == null) return false;
            if (!Objects.equals(this._houseNumber, other.getHouseNumber()))  return false;
        }
        if (!Objects.equals(this._city, other.getCity()))                    return false;
        if (!Objects.equals(this._state, other.getState()))                  return false;
        
        return true;
    }
}
