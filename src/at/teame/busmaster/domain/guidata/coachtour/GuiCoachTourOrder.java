package at.teame.busmaster.domain.guidata.coachtour;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.AdditionalExpenseDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.persistencedata.coachtour.AdditionalExpense;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourCustomer;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourOrder;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourSchedule;

public class GuiCoachTourOrder implements CoachTourOrderDI {
	
	private int _numberOfPersons;
	private Date _creationDate;
	private Date _reservationDate;
	private Date _cancelDate;
	private Date _bookingDate;
	private String _specification;
	private CoachTourCustomerDI _customer;
	private List<CoachTourScheduleDI> _schedules;
	private List<AdditionalExpenseDI> _expenses;
	
	@SuppressWarnings("unchecked")
	public GuiCoachTourOrder(int numberOfPersons, Date creationDate, Date reservationDate, Date cancleDate, Date bookingDate, String specification, CoachTourCustomerDI customer, List<? extends CoachTourScheduleDI> schedules, List<? extends AdditionalExpenseDI> expenses){
		_numberOfPersons = numberOfPersons;
		_creationDate= creationDate;
		_reservationDate = reservationDate;
		_cancelDate = cancleDate;
		_bookingDate = bookingDate;
		_specification = specification;
		_customer = customer;
		_schedules = (List<CoachTourScheduleDI>) schedules;
		_expenses = expenses != null ? (List<AdditionalExpenseDI>) expenses : new LinkedList<AdditionalExpenseDI>();
	}

	@Override
	public int getNumberOfPersons() {
		return _numberOfPersons;
	}

	@Override
	public Date getCreationDate() {
		return _creationDate;
	}

	@Override
	public Date getReservationDate() {
		return _reservationDate;
	}

	@Override
	public Date getCancelDate() {
		return _cancelDate;
	}

	@Override
	public Date getBookingDate() {
		return _bookingDate;
	}

	@Override
	public String getSpecification() {
		return _specification;
	}

	@Override
	public CoachTourCustomerDI getCustomer() {
		return _customer;
	}

	@Override
	public List<? extends CoachTourScheduleDI> getSchedules() {
		return _schedules;
	}

	@Override
	public List<? extends AdditionalExpenseDI> getExpenses() {
		return _expenses;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		List<CoachTourSchedule> schedules = new LinkedList<>();
		List<AdditionalExpense> expenses = new LinkedList<>();
		
		for (AdditionalExpenseDI ex : _expenses) {
			expenses.add((AdditionalExpense)ex.convertToDomain());
		}
		
		for (CoachTourScheduleDI sch : _schedules) {
			schedules.add((CoachTourSchedule)sch.convertToDomain());
		}
		
		return new CoachTourOrder(_numberOfPersons, _creationDate, _reservationDate, _bookingDate, _cancelDate, _specification, schedules ,expenses, (CoachTourCustomer) _customer.convertToDomain());
	}

	@Override
	public List<? extends CoachTourDI> getTours() {
		List<CoachTourDI> tours = new LinkedList<>();
		for(CoachTourScheduleDI s : _schedules) {
			tours.addAll(s.getTours());
		}
		return tours;
	}
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                           return false;
        if (!(obj instanceof CoachTourOrderDI))                                    return false;
        
        CoachTourOrderDI other = (CoachTourOrderDI) obj;
  
        if(other.getNumberOfPersons() != _numberOfPersons)                         return false;
        if(!Objects.equals(this._specification, other.getSpecification()))         return false;
        if(!Objects.equals(this._customer, other.getCustomer()))                   return false;
        if(!Objects.equals(this._creationDate, other.getCreationDate()))           return false;
        if(this._bookingDate == null && other.getBookingDate() != null)            return false;
        else
        {
            if(this._bookingDate != null && other.getBookingDate() == null)        return false;
            if(!Objects.equals(this._bookingDate, other.getBookingDate()))         return false;
        }
        if(this._reservationDate == null && other.getReservationDate() != null)    return false;
        else
        {
            if(this._reservationDate != null && other.getReservationDate()== null) return false;
            if(!Objects.equals(this._reservationDate, other.getReservationDate())) return false;
        }
        if(this._cancelDate == null && other.getCancelDate() != null)              return false;
        else
        {
            if(this._cancelDate != null && other.getCancelDate()== null)           return false;
            if(!Objects.equals(this._cancelDate, other.getCancelDate()))           return false;
        }
        
        return true;
    }

}
