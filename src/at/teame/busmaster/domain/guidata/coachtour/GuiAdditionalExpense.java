package at.teame.busmaster.domain.guidata.coachtour;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.AdditionalExpenseDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.persistencedata.coachtour.AdditionalExpense;

public class GuiAdditionalExpense implements AdditionalExpenseDI {
	
	private String _specification;
	private int _price;
	private CoachTourOrderDI _order;
	
	public GuiAdditionalExpense(String specification, int price, CoachTourOrderDI order){
		_specification =specification;
		_price = price;
		_order = order;
	}

    public GuiAdditionalExpense(String specification, int price)
    {
        this._specification = specification;
        this._price = price;
    }

	@Override
	public String getSpecification() {
		return _specification;
	}

	@Override
	public int getPrice() {
		return _price;
	}

	@Override
	public CoachTourOrderDI getOrder() {
		return _order;
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return new AdditionalExpense(_specification, _price);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                    return false;
        if (!(obj instanceof AdditionalExpenseDI))                          return false;
        
        AdditionalExpenseDI other = (AdditionalExpenseDI) obj;
        
        if (!Objects.equals(this._specification, other.getSpecification())) return false;
        if (this._price != other.getPrice())                                return false;
        if (!Objects.equals(this._order, other.getOrder()))                 return false;
        
        return true;
    }

}
