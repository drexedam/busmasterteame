package at.teame.busmaster.domain.guidata.coachtour;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTour;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourSchedule;
import at.teame.busmaster.domain.persistencedata.driver.Driver;

public class GuiCoachTour implements CoachTourDI {
	
	private DriverDI _driver;
	private DriverDI _additionalDriver;
	private BusDI _bus;
	private CoachTourScheduleDI _schedule;
	
	public GuiCoachTour(DriverDI driver, DriverDI additionalDriver, BusDI bus, CoachTourScheduleDI schedule){
		_driver = driver;
		_additionalDriver = additionalDriver;
		_bus = bus;
		_schedule = schedule;
	}

	@Override
 	public DriverDI getDriver() {
		return _driver;
	}

	@Override
	public DriverDI getAdditionalDriver() {
		return _additionalDriver;
	}

	@Override
	public BusDI getBus() {
		return _bus;
	}

	@Override
	public CoachTourScheduleDI getSchedule() {
		return _schedule;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		Driver addDriver = null;
		if(_additionalDriver != null) {
			addDriver = (Driver)_additionalDriver.convertToDomain();
		}
		return new CoachTour((Driver) _driver.convertToDomain(), addDriver, (Bus) _bus.convertToDomain(), (CoachTourSchedule) _schedule.convertToDomain());
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                          return false;
        if (!(obj instanceof CoachTourDI))                                        return false;
        
        CoachTourDI other = (CoachTourDI) obj;
        
        if (!Objects.equals(this._driver, other.getDriver()))                     return false;
        if (!Objects.equals(this._additionalDriver, other.getAdditionalDriver())) return false;
        if (!Objects.equals(this._bus, other.getBus()))                           return false;
        if (!Objects.equals(this._schedule, other.getSchedule()))                 return false;
        
        return true;
    }

}
