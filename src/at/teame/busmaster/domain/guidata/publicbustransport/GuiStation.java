package at.teame.busmaster.domain.guidata.publicbustransport;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.StationDI;
import at.teame.busmaster.domain.persistencedata.publicbustransport.Station;

public class GuiStation implements StationDI {
	
	private String _number;
	private String _name;
	private String _desc;

	public GuiStation(String number, String name, String desc) {
		_number = number;
		_name = name;
		_desc = desc;
	}
	
	@Override
	public DI convertToDomain() throws PeriodException,
			UncompatibleConvertionException {
		return new Station(_number, _name, _desc);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        if (!(obj instanceof StationDI))
            return false;
        StationDI other = (StationDI) obj;
        if (!Objects.equals(this._number, other.getNumber()))
            return false;
        if (!Objects.equals(this._name, other.getName()))
            return false;
        if (!Objects.equals(this._desc, other.getShortDesc()))
            return false;
        return true;
    }

	@Override
	public String getNumber() {
		return _number;
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public String getShortDesc() {
		return _desc;
	}

}
