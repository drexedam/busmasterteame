package at.teame.busmaster.domain.guidata.publicbustransport;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteSectionDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.StationDI;
import at.teame.busmaster.domain.persistencedata.publicbustransport.RouteSection;
import at.teame.busmaster.domain.persistencedata.publicbustransport.Station;

public class GuiRouteSection implements RouteSectionDI {

	private int _dist;
	private int _driveTime;
	private StationDI _start;
	private StationDI _end;
	
	public GuiRouteSection(int dist, int drivingtime, StationDI start, StationDI end) {
		_dist = dist;
		_driveTime = drivingtime;
		_start = start;
		_end = end;
	}
	
	@Override
	public DI convertToDomain() throws PeriodException,
			UncompatibleConvertionException {
		return new RouteSection(_dist, _driveTime, (Station) _start.convertToDomain(), (Station) _end.convertToDomain());
	}

	@Override
	public int getDistance() {
		return _dist;
	}

	@Override
	public int getDrivingTime() {
		return _driveTime;
	}

	@Override
	public StationDI getStart() {
		return _start;
	}

	@Override
	public StationDI getEnd() {
		return _end;
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        if (!(obj instanceof RouteSectionDI))
            return false;
        RouteSectionDI other = (RouteSectionDI) obj;
        if (this._dist != other.getDistance())
            return false;
        if (this._driveTime != other.getDrivingTime())
            return false;
        if (!Objects.equals(this._start, other.getStart()))
            return false;
        if (!Objects.equals(this._end, other.getEnd()))
            return false;
        return true;
    }

}
