package at.teame.busmaster.domain.guidata.publicbustransport;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteSectionDI;
import at.teame.busmaster.domain.persistencedata.publicbustransport.Route;
import at.teame.busmaster.domain.persistencedata.publicbustransport.RouteSection;
public class GuiRoute implements RouteDI{

	private String _number;
	private String _specification;
	private List<RouteSectionDI> _sections;
	
	
	public GuiRoute(String number, String specification, List<RouteSectionDI> sections) {
		_number = number;
		_specification = specification;
		_sections = sections;
	}
	
	@Override
	public DI convertToDomain() throws PeriodException,
			UncompatibleConvertionException {
		List<RouteSection> sections = new LinkedList<>();
		for(RouteSectionDI r : _sections) {
			sections.add((RouteSection) r.convertToDomain());
		}
		return new Route(_number, _specification, sections);
	}

	@Override
	public String getNumber() {
		return _number;
	}

	@Override
	public String getSpecification() {
		return _specification;
	}

	@Override
	public List<? extends RouteSectionDI> getSections() {
		return _sections;
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        if (!(obj instanceof RouteDI))
            return false;
        RouteDI other = (RouteDI) obj;
        if (!Objects.equals(this._number, other.getNumber()))
            return false;
        if (!Objects.equals(this._specification, other.getSpecification()))
            return false;
        
        if (this._sections.size() != other.getSections().size()) {
            return false;
        }
        
        boolean containsAll = true;
        for(RouteSectionDI pbt : this._sections)
        {
            containsAll &= other.getSections().contains(pbt);
        }
        
        if(!containsAll) {
            return false;
        }
        
        return true;
    }

}
