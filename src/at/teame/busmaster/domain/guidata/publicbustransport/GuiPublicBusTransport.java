package at.teame.busmaster.domain.guidata.publicbustransport;

import java.util.Date;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.PublicBusTransportDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.RouteDI;
import at.teame.busmaster.domain.domaininterface.schedule.CircleDefinitionDI;
import at.teame.busmaster.domain.domaininterface.schedule.DayTypeDI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftDefinitionDI;
import at.teame.busmaster.domain.persistencedata.publicbustransport.PublicBusTransport;
import at.teame.busmaster.domain.persistencedata.publicbustransport.Route;
import at.teame.busmaster.domain.persistencedata.schedule.CircleDefinition;
import at.teame.busmaster.domain.persistencedata.schedule.DayType;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftDefinition;

public class GuiPublicBusTransport implements PublicBusTransportDI {
	
	private Date _start;
	private CircleDefinitionDI _circleDefinition;
	private DriverShiftDefinitionDI _driverDefinition;
	private DayTypeDI _dayType;
	private RouteDI _route;
	
	public GuiPublicBusTransport(Date start, DayTypeDI dayType, RouteDI route){
		_start = start;
		_dayType = dayType;
		_route = route;
	}
	
	public GuiPublicBusTransport(Date start, CircleDefinitionDI circleDefinition, DriverShiftDefinitionDI driverDefinition, DayTypeDI dayType, RouteDI route){
		_start = start;
		_circleDefinition = circleDefinition;
		_driverDefinition = driverDefinition;
		_dayType = dayType;
		_route = route;
	}

	@Override
	public Date getStart() {
		return _start;
	}


	@Override
	public CircleDefinitionDI getCircleDefinition() {
		return _circleDefinition;
	}

	@Override
	public DriverShiftDefinitionDI getShiftDefinition() {
		return _driverDefinition;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		return new PublicBusTransport(_start, (CircleDefinition) _circleDefinition.convertToDomain(), (DriverShiftDefinition) _driverDefinition.convertToDomain(), (DayType) _dayType.convertToDomain(), (Route) _route.convertToDomain());
	}

    @Override
    public int hashCode()
    {
        return 5;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        if (!(obj instanceof PublicBusTransportDI))
            return false;
        PublicBusTransportDI other = (PublicBusTransportDI) obj;
        if (!Objects.equals(this._start, other.getStart()))
            return false;
        if (!Objects.equals(this._circleDefinition, other.getCircleDefinition()))
            return false;
        if (!Objects.equals(this._driverDefinition, other.getShiftDefinition()))
            return false;
        if (!Objects.equals(this._dayType, other.getDayType()))
            return false;
        if (!Objects.equals(this._route, other.getRoute()))
            return false;
        return true;
    }

	@Override
	public DayTypeDI getDayType() {
		return _dayType;
	}

	@Override
	public RouteDI getRoute() {
		return _route;
	}

}
