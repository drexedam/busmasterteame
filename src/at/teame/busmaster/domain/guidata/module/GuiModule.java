package at.teame.busmaster.domain.guidata.module;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleTypeDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.module.Module;
import at.teame.busmaster.domain.persistencedata.module.ModuleType;

public class GuiModule implements ModuleDI {

	private BusDI _bus;
	private ModuleTypeDI _type;
	
	public GuiModule(BusDI bus, ModuleTypeDI type) {
		_bus = bus;
		_type = type;
	}
	
	@Override
	public BusDI getBus() {
		return _bus;
	}

	@Override
	public ModuleTypeDI getType() {
		return _type;
	}

	@Override
	public int getPrice() {
		return _type.getPrice();
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		Bus b = null;
		if(_bus != null) {
			b = (Bus) _bus.convertToDomain();
		}
		return new Module(b, (ModuleType) _type.convertToDomain());
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                  return false;
        if (!(obj instanceof ModuleDI))                   return false;
        
        ModuleDI other = (ModuleDI) obj;
        
        if (!Objects.equals(this._bus, other.getBus()))   return false;
        if (!Objects.equals(this._type, other.getType())) return false;
        
        return true;
    }

	@Override
	public int getRequiredSpace() {
		return _type.getRequiredSpace();
	}

}
