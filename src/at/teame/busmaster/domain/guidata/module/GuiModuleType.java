package at.teame.busmaster.domain.guidata.module;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.module.ModuleTypeDI;
import at.teame.busmaster.domain.persistencedata.module.ModuleType;

public class GuiModuleType implements ModuleTypeDI {

	private String _specification;
	private int _reqSpace;
	private int _price;
	
	public GuiModuleType(String specification, int requiredSpace, int price) {
		_specification = specification;
		_reqSpace = requiredSpace;
		_price = price;
	}
	
	@Override
	public String getSpecification() {
		return _specification;
	}

	@Override
	public int getRequiredSpace() {
		return _reqSpace;
	}

	@Override
	public int getPrice() {
		return _price;
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return new ModuleType(_specification, _reqSpace, _price);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                    return false;
        if (!(obj instanceof ModuleTypeDI))                                 return false;

        ModuleTypeDI other = (ModuleTypeDI) obj;
        
        if (!Objects.equals(this._specification, other.getSpecification())) return false;
        if (this._reqSpace != other.getRequiredSpace())                     return false;
        if (this._price != other.getPrice())                                return false;
        
        return true;
    }

}
