/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.domain.guidata.module;

import at.teame.busmaster.domain.domaininterface.module.ModuleTypeDI;

/**
 * @author  Morent Jochen <jochen.morent@students.fhv.at>
 * @org     FHV <fhv.at>
 * @project BusMaster-Team-E
 * @date    25.04.2014
 */
public class GuiModuleTypeWrapper
{
    private ModuleTypeDI _module = null;
    private String   _text   = null;

    public GuiModuleTypeWrapper(ModuleTypeDI module)
    {
        _module = module;
    }
    public GuiModuleTypeWrapper(ModuleTypeDI module, String text)
    {
        _module = module;
        _text = text;
    }

    public ModuleTypeDI getModuleType()
    {
        return _module;
    }
    
    @Override
    public String toString()
    {
        if(_text != null)
        {
            return _text;
        }
        else
        {
            return _module.getSpecification();
        }
    }
}
