package at.teame.busmaster.domain.guidata.offer;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.AdditionalExpenseDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.domaininterface.offer.OfferDI;
import at.teame.busmaster.domain.guidata.coachtour.GuiCoachTourOrder;
import at.teame.busmaster.domain.persistencedata.bus.BusType;
import at.teame.busmaster.domain.persistencedata.coachtour.AdditionalExpense;
import at.teame.busmaster.domain.persistencedata.driver.DriverType;
import at.teame.busmaster.domain.persistencedata.module.Module;
import at.teame.busmaster.domain.persistencedata.offer.Offer;

public class GuiOffer implements OfferDI {
	
	private int _price;
	private GuiCoachTourOrder _order;
	private List<AdditionalExpenseDI> _expenses;
	private List<List<ModuleDI>> _modules;
	
	@SuppressWarnings("unchecked")
	public GuiOffer(int price, GuiCoachTourOrder order, List<? extends AdditionalExpenseDI> expenses, List<? extends ModuleDI> modules){
		_price = price;
		_order = order;
		_expenses = (List<AdditionalExpenseDI>) expenses;
		_modules = new LinkedList<>();
        _modules.add(new LinkedList<>(modules));
	}
        
        @Override
        public int getPrice(List<BusType> bustype, List<DriverType> drivertype, int drivers, int traveltime, int standingtime, int busses, int distance) {
            return _price;
        }

    public int getPrice() {
		return _price;
	}

	public CoachTourOrderDI getOrder() {
		return _order;
	}

	@Override
	public List<? extends AdditionalExpenseDI> getExpenses() {
		return _expenses;
	}

    @Override
	public List<List<ModuleDI>> getModules() {
		return _modules;
	}

    @Override
    public DI convertToDomain() throws PeriodException, UncompatibleConvertionException
    {
        List<AdditionalExpense> expenses = new LinkedList<>();
        List<Module>            modules  = new LinkedList<>();
        
        for(AdditionalExpenseDI ad : _expenses)
        {
            expenses.add((AdditionalExpense)ad.convertToDomain());
        }
        List<List<Module>> module = new LinkedList<>();
        for (List<ModuleDI> mo : _modules) {
            for (ModuleDI m : mo) {
                modules.add((Module) m.convertToDomain());
            }
            module.add(modules);
        }
        return new Offer(expenses, module);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                           return false;
        if (!(obj instanceof OfferDI))                                             return false;
        
        OfferDI other = (OfferDI) obj;
        
        if (this._expenses.size() != other.getExpenses().size())                   return false;
        for(int i = 0; i < this._expenses.size(); i++)
        {
            if(!Objects.equals(this._expenses.get(i), other.getExpenses().get(i))) return false;
        }
        if (this._modules.size() != other.getModules().size())                     return false;
        for(int i = 0; i < this._modules.size(); i++)
        {
            if(!Objects.equals(this._modules.get(i), other.getModules().get(i)))   return false;
        }
        return true;
    }

}
