package at.teame.busmaster.domain.guidata.user;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.user.UserDI;
import at.teame.busmaster.domain.persistencedata.user.User;

public class GuiUser implements UserDI {

	private String _uName;
	private String _pw;
	private String _perm;
	private DriverDI _driver;
	
	public GuiUser(String userName, String password, String permition) {
		_uName = userName;
		_pw = password;
		_perm = permition;
	}
	
	@Override
	public String getUserName() {
		return _uName;
	}

	@Override
	public String getPassword() {
		return _pw;
	}

	@Override
	public String getPermition() {
		return _perm;
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return new User(_uName, _pw, _perm);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                        return false;
        if (!(obj instanceof UserDI))                           return false;
        
        UserDI other = (UserDI) obj;
        if (!Objects.equals(this._uName, other.getUserName() )) return false;
        if (!Objects.equals(this._pw,    other.getPassword() )) return false;
        if (!Objects.equals(this._perm,  other.getPermition())) return false;
        
        return true;
    }

	@Override
	public DriverDI getDriver() {
		return _driver;
	}

}
