package at.teame.busmaster.domain.guidata.tb2;

import java.util.LinkedList;
import java.util.List;

import at.gruppe2.domain.bus.IBus;
import at.gruppe2.domain.publicbustransport.IPublicBusTransport;
import at.gruppe2.domain.schedule.IDayType;
import at.teame.busmaster.domain.persistencedata.publicbustransport.PublicBusTransport;

public class CircleDefinitionDummy {
	
	private IDayType _dayType;
	private String _specification;
	private IBus _bus;
	private List<? extends IPublicBusTransport> _publicTransports = new LinkedList<>();
	
	public CircleDefinitionDummy(){}
	
	public CircleDefinitionDummy(IDayType dayType, String specification, IBus bus, List<? extends PublicBusTransport> tranports){
		_dayType = dayType;
		_specification = specification;
		_bus = bus;
		_publicTransports = tranports;
	}

	public IDayType getDayType() {
		return _dayType;
	}

	public void setDayType(IDayType dayType) {
		this._dayType = dayType;
	}

	public String getSpecification() {
		return _specification;
	}

	public void setSpecification(String specification) {
		this._specification = specification;
	}

	public IBus getBus() {
		return _bus;
	}

	public void setBus(IBus bus) {
		this._bus = bus;
	}

	public List<? extends IPublicBusTransport> getPublicTransports() {
		return _publicTransports;
	}

	public void setPublicTransports(List<? extends IPublicBusTransport> publicTransports) {
		this._publicTransports = publicTransports;
	}
	
	
}
