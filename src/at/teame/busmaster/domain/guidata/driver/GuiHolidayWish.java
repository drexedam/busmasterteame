package at.teame.busmaster.domain.guidata.driver;

import java.util.Date;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.HolidayWishDI;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.HolidayWish;

public class GuiHolidayWish implements HolidayWishDI {
	private Date _start;
	private Date _end;
	private DriverDI _driver;
	
	public GuiHolidayWish(Date start, Date end, DriverDI driver) {
		_start = start;
		_end = end;
		_driver = driver;
	}

	@Override
	public DI convertToDomain() throws PeriodException,
			UncompatibleConvertionException {
		return new HolidayWish(_start, _end, (Driver)_driver.convertToDomain());
	}

	@Override
	public Date getStart() {
		return _start;
	}

	@Override
	public Date getEnd() {
		return _end;
	}

	@Override
	public DriverDI getDriver() {
		return _driver;
	}

	@Override
	public int getId() {
		return -1;
	}

}
