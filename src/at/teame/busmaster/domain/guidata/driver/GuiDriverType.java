package at.teame.busmaster.domain.guidata.driver;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverTypeDI;
import at.teame.busmaster.domain.persistencedata.driver.DriverType;

public class GuiDriverType implements DriverTypeDI {

	private int _fare;
	private int _standingPrice;
	private String _specification;
	
	public GuiDriverType(int fare, int standingPrice, String specification) {
		_fare = fare;
		_standingPrice = standingPrice;
		_specification = specification;
	}
	
	@Override
	public int getFare() {
		return _fare;
	}

	@Override
	public int getStandingPrice() {
		return _standingPrice;
	}

	@Override
	public String getSpecification() {
		return _specification;
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return new DriverType(_fare, _standingPrice, _specification);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                    return false;
        if (!(obj instanceof DriverTypeDI))                                 return false;
        
        DriverTypeDI other = (DriverTypeDI) obj;
        
        if (this._fare != other.getFare())                                  return false;
        if (this._standingPrice != other.getStandingPrice())                return false;
        if (!Objects.equals(this._specification, other.getSpecification())) return false;
        
        return true;
    }

}
