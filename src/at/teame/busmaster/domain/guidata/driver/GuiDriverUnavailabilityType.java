package at.teame.busmaster.domain.guidata.driver;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverUnavailabilityTypeDI;
import at.teame.busmaster.domain.persistencedata.driver.DriverUnavailabilityType;

public class GuiDriverUnavailabilityType implements DriverUnavailabilityTypeDI {

	private String _spec;
	
	public GuiDriverUnavailabilityType(String specification) {
		_spec = specification;
	}
	
	@Override
	public String getSpecification() {
		return _spec;
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return new DriverUnavailabilityType(_spec);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                           return false;
        if (!(obj instanceof DriverUnavailabilityTypeDI))          return false;
        
        DriverUnavailabilityTypeDI other = (DriverUnavailabilityTypeDI) obj;
        
        if (!Objects.equals(this._spec, other.getSpecification())) return false;
        
        return true;
    }

}
