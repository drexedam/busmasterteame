package at.teame.busmaster.domain.guidata.driver;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.MailContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.TelContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverTypeDI;
import at.teame.busmaster.domain.persistencedata.State;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.DriverType;

public class GuiDriver implements DriverDI {

	private String _fName;
	private String _lName;
	private String _street;
	private String _hNumber;
	private String _zip;
	private String _city;
	private StateDI _state;
	private DriverTypeDI _type;
	private List<CoachTourDI> _tours;
	@SuppressWarnings("unused")
	private List<TelContactDetailsDI> _phones;
	@SuppressWarnings("unused")
	private List<MailContactDetailsDI> _mails;
	
	public GuiDriver(String firstName, String lastName, String street, String houseNumber, String zip, String city, StateDI state, DriverTypeDI type, List<TelContactDetailsDI> phones, List<MailContactDetailsDI> mails) {
		_fName = firstName;
		_lName = lastName;
		_street = street;
		_hNumber = houseNumber;
		_zip = zip;
		_city = city;
		_state = state;
		_type = type;
		_phones =phones;
		_mails = mails;
	}
	
	public GuiDriver(String firstName, String lastName, String street, String houseNumber, String zip, String city, StateDI state, DriverTypeDI type, List<CoachTourDI> tours) {
		_fName = firstName;
		_lName = lastName;
		_street = street;
		_hNumber = houseNumber;
		_zip = zip;
		_city = city;
		_state = state;
		_type = type;
		_tours = tours;
	}
	
	@Override
	public String getFirstName() {
		return _fName;
	}

	@Override
	public String getLastName() {
		return _lName;
	}

	@Override
	public String getStreet() {
		return _street;
	}

	@Override
	public String getHouseNumber() {
		return _hNumber;
	}

	@Override
	public String getZIP() {
		return _zip;
	}

	@Override
	public String getCity() {
		return _city;
	}

	@Override
	public StateDI getState() {
		return _state;
	}

	@Override
	public DriverTypeDI getType() {
		return _type;
	}

	@Override
	public boolean isAvailable(Date start, Date end) {
		//Immer true, weil Objekt noch nicht in der DB
		return true;
	}

	@Override
	public List<? extends CoachTourDI> getCoachTours() {
		return _tours;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		return new Driver(_fName, _lName, _street, _hNumber, _zip, _city, (State) _state.convertToDomain(),(DriverType) _type.convertToDomain());
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                            return false;
        if (!(obj instanceof DriverDI))                             return false;
        
        DriverDI other = (DriverDI) obj;
        
        if (!Objects.equals(this._fName, other.getFirstName()))     return false;
        if (!Objects.equals(this._lName, other.getLastName()))      return false;
        if (!Objects.equals(this._street, other.getStreet()))       return false;
        if (!Objects.equals(this._hNumber, other.getHouseNumber())) return false;
        if (!Objects.equals(this._zip, other.getZIP()))             return false;
        if (!Objects.equals(this._city, other.getCity()))           return false;
        if (!Objects.equals(this._state, other.getState()))         return false;
        if (!Objects.equals(this._type, other.getType()))           return false;
        
        return true;
    }

	@Override
	public boolean isAvailable(Date start, Date end, CoachTourOrderDI order)
			throws PeriodException {
		//Da Objekt noch nicht in der DB
		return true;
	}


}
