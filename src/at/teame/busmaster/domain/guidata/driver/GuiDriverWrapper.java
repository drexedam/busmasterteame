/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.domain.guidata.driver;

import java.util.logging.Level;
import java.util.logging.Logger;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;

/**
 * @author  Morent Jochen <jochen.morent@students.fhv.at>
 * @org     FHV <fhv.at>
 * @project BusMaster-Team-E
 * @date    25.04.2014
 */
public class GuiDriverWrapper
{
    private DriverDI         _driver = null;
    private CoachTourOrderDI _order  = null;

    public GuiDriverWrapper(DriverDI driver, CoachTourOrderDI order)
    {
        _driver = driver;
        _order  = order;
    }

    public DriverDI getDriver()
    {
        return _driver;
    }

    private boolean isAvailable()
    {
        boolean ret = true;
        
        for(CoachTourScheduleDI cts : _order.getSchedules())
        {
            try
            {
                if(!_driver.isAvailable(cts.getStart(), cts.getEnd()))                
                {
                    ret = false;
                    break;
                }
            } catch (PeriodException ex)
            {
                //should never happen
                Logger.getLogger(GuiDriverWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return ret;
    }
    
    @Override
    public String toString()
    {
        if(_driver == null)
        {
            return "";
        }
        boolean available = isAvailable();
        return _driver.toString() + (available ? "" : " (!)");
    }
    
    
}
