package at.teame.busmaster.domain.guidata.driver;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.ShiftWishDI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftScheduleDI;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.ShiftWish;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftSchedule;

public class GuiShiftWish implements ShiftWishDI {
	
	private DriverDI _driver;
	private DriverShiftScheduleDI _shift;
	private boolean _wished;

	public GuiShiftWish(DriverDI driver, DriverShiftScheduleDI shift, boolean wished) {
		_driver = driver;
		_shift = shift;
		_wished = wished;
	}
	
	@Override
	public DI convertToDomain() throws PeriodException,
			UncompatibleConvertionException {
		return new ShiftWish((Driver) _driver.convertToDomain(), (DriverShiftSchedule) _shift.convertToDomain(), _wished); 
	}

	@Override
	public boolean getWished() {
		return _wished;
	}

	@Override
	public DriverDI getDriver() {
		return _driver;
	}

	@Override
	public DriverShiftScheduleDI getShift() {
		return _shift;
	}

	@Override
	public int getID() {
		return -1;
	}

}
