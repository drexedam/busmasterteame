package at.teame.busmaster.domain.guidata.driver;

import java.util.Date;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverUnavailabilityDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverUnavailabilityTypeDI;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.DriverUnavailability;
import at.teame.busmaster.domain.persistencedata.driver.DriverUnavailabilityType;

public class GuiDriverUnavailability implements DriverUnavailabilityDI {

	private Date _start;
	private Date _end;
	private DriverUnavailabilityTypeDI _type;
	private DriverDI _driver;
	
	public GuiDriverUnavailability(Date start, Date end, DriverUnavailabilityTypeDI type, DriverDI driver) {
		_start = start;
		_end = end;
		_type = type;
		_driver = driver;
	}
	
	@Override
	public Date getStart() {
		return _start;
	}

	@Override
	public Date getEnd() {
		return _end;
	}

	@Override
	public DriverUnavailabilityTypeDI getType() {
		return _type;
	}

	@Override
	public DriverDI getDriver() {
		return _driver;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		return new DriverUnavailability(_start, _end,(DriverUnavailabilityType) _type.convertToDomain(), (Driver) _driver.convertToDomain());
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                      return false;
        if (!(obj instanceof DriverUnavailabilityDI))         return false;
        
        DriverUnavailabilityDI other = (DriverUnavailabilityDI) obj;
        
        if (!Objects.equals(this._start, other.getStart()))   return false;
        if (!Objects.equals(this._end, other.getEnd()))       return false;
        if (!Objects.equals(this._type, other.getType()))     return false;
        if (!Objects.equals(this._driver, other.getDriver())) return false;
        
        return true;
    }

}
