/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.domain.guidata.driver;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.CoachTourWishDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTour;
import at.teame.busmaster.domain.persistencedata.coachtour.CoachTourSchedule;
import at.teame.busmaster.domain.persistencedata.driver.CoachTourWish;
import at.teame.busmaster.domain.persistencedata.driver.Driver;

/**
 * @author  Morent Jochen <jochen.morent@students.fhv.at>
 * @org     FHV <fhv.at>
 * @project BusMaster-Team-E
 * @date    30.05.2014
 */
public class GuiCoachTourWish implements CoachTourWishDI
{
    private DriverDI            _driver;
    private CoachTourScheduleDI _coachTour;

    public GuiCoachTourWish(DriverDI driver, CoachTourScheduleDI coachTour)
    {
        _driver = driver;
        _coachTour = coachTour;
    }

    @Override
    public int getID()
    {
        return -1;
    }

    public void setDriver(DriverDI _driver)
    {
        this._driver = _driver;
    }
    
    @Override
    public DriverDI getDriver()
    {
        return _driver;
    }

    public void setCoachTour(CoachTourScheduleDI _coachTour)
    {
        this._coachTour = _coachTour;
    }

    @Override
    public CoachTourScheduleDI getCoachTourSchedule()
    {
        return _coachTour;
    }

    @Override
    public DI convertToDomain() throws PeriodException, UncompatibleConvertionException
    {
        return new CoachTourWish((Driver)            _driver   .convertToDomain(),
                                 (CoachTourSchedule) _coachTour.convertToDomain());
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                            return false;
        if (!(obj instanceof CoachTourWishDI))                      return false;
        CoachTourWishDI other = (CoachTourWishDI) obj;
        if (!Objects.equals(this._driver, other.getDriver()))       return false;
        if (!Objects.equals(this._coachTour, other.getCoachTourSchedule())) return false;
        
                                                                    return true;
    }
}
