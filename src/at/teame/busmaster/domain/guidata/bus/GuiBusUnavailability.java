package at.teame.busmaster.domain.guidata.bus;

import java.util.Date;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.bus.BusUnavailabilityDI;
import at.teame.busmaster.domain.domaininterface.bus.BusUnavailabilityTypeDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.bus.BusUnavailability;
import at.teame.busmaster.domain.persistencedata.bus.BusUnavailabilityType;

public class GuiBusUnavailability implements BusUnavailabilityDI {

	private Date _start;
	private Date _end;
	private BusDI _bus;
	private BusUnavailabilityTypeDI _type;
	
	public GuiBusUnavailability(Date start, Date end, BusDI bus, BusUnavailabilityTypeDI type) {
		_start = start;
		_end = end;
		_bus = bus;
		_type = type;
	}
	
	@Override
	public Date getStart() {
		return _start;
	}

	@Override
	public Date getEnd() {
		return _end;
	}

	@Override
	public BusDI getBus() {
		return _bus;
	}

	@Override
	public BusUnavailabilityTypeDI getType() {
		return _type;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		return new BusUnavailability(_start, _end, (Bus)_bus.convertToDomain(), (BusUnavailabilityType)_type.convertToDomain());
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                    return false;
        if (!(obj instanceof BusUnavailabilityDI))          return false;
        
        BusUnavailabilityDI other = (BusUnavailabilityDI) obj;
        
        if (!Objects.equals(this._start, other.getStart())) return false;
        if (!Objects.equals(this._end, other.getEnd()))     return false;
        if (!Objects.equals(this._bus, other.getBus()))     return false;
        if (!Objects.equals(this._type, other.getType()))   return false;
        
        return true;
    }

}
