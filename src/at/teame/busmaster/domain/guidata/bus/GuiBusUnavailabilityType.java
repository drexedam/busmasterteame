package at.teame.busmaster.domain.guidata.bus;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusUnavailabilityTypeDI;
import at.teame.busmaster.domain.persistencedata.bus.BusUnavailabilityType;

public class GuiBusUnavailabilityType implements BusUnavailabilityTypeDI {

	private String _specification;
	
	public GuiBusUnavailabilityType(String specification) {
		_specification = specification;
	}
	
	@Override
	public String getSpecification() {
		return _specification;
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return new BusUnavailabilityType(_specification);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                    return false;
        if (!(obj instanceof BusUnavailabilityTypeDI))                      return false;
        
        BusUnavailabilityTypeDI other = (BusUnavailabilityTypeDI) obj;
        
        if (!Objects.equals(this._specification, other.getSpecification())) return false;
        
        return true;
    }

}
