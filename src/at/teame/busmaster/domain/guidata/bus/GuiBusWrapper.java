/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.domain.guidata.bus;

import java.util.logging.Level;
import java.util.logging.Logger;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;

/**
 * @author  Morent Jochen <jochen.morent@students.fhv.at>
 * @org     FHV <fhv.at>
 * @project BusMaster-Team-E
 * @date    25.04.2014
 */
public class GuiBusWrapper
{
    private BusDI            _bus   = null;
    private CoachTourOrderDI _order = null;
    private String           _text  = null;

    public GuiBusWrapper(BusDI bus)
    {
        _bus  = bus;
    }
    public GuiBusWrapper(BusDI bus, CoachTourOrderDI order)
    {
        _bus   = bus;
        _order = order;
    }
    public GuiBusWrapper(BusDI bus, String text)
    {
        _bus  = bus;
        _text = text;
    }

    public BusDI getBus()
    {
        return _bus;
    }

    private boolean isAvailable()
    {
        boolean ret = true;
        
        for(CoachTourScheduleDI cts : _order.getSchedules())
        {
            try
            {
                if(!_bus.isAvailable(cts.getStart(), cts.getEnd()))                
                {
                    ret = false;
                    break;
                }
            } catch (PeriodException ex)
            {
                //should never happen
                Logger.getLogger(GuiBusWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return ret;
    }
    
    @Override
    public String toString()
    {
        if(_order != null)
        {
            boolean available = isAvailable();
            return _bus.toString() + (available ? "" : " (!)");
        }
        else if(_text != null)
        {
            return _text;
        }
        else
        {
            return _bus.toString();
        }
    }
    
    
}
