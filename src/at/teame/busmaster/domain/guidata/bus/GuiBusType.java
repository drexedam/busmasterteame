package at.teame.busmaster.domain.guidata.bus;

import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusTypeDI;
import at.teame.busmaster.domain.persistencedata.bus.BusType;
import java.util.Objects;

public class GuiBusType implements BusTypeDI {
	
	private int _standingPlaces;
	private int _seats;
	private String _type;
	private int _basePrice;
	private int _kmPrice;
	
	public GuiBusType(int standingPlaces, int seats, String type, int basePrice, int kmPrice) {
		_standingPlaces = standingPlaces;
		_seats = seats;
		_type = type;
		_basePrice = basePrice;
		_kmPrice = kmPrice;
	}

	@Override
	public int getStandingPlaces() {
		return _standingPlaces;
	}

	@Override
	public int getSeats() {
		return _seats;
	}

	@Override
	public String getType() {
		return _type;
	}

	@Override
	public int getBasePrice() {
		return _basePrice;
	}

	@Override
	public int getKMPrice() {
		return _kmPrice;
	}

	@Override
	public DI convertToDomain() {
		return new BusType(_standingPlaces, _seats, _type, _basePrice, _kmPrice);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                       return false;
        if (!(obj instanceof BusTypeDI))                       return false;
        
        BusTypeDI other = (BusTypeDI) obj;
        
        if (this._standingPlaces != other.getStandingPlaces()) return false;
        if (this._seats != other.getSeats())                   return false;
        if (!Objects.equals(this._type, other.getType()))      return false;
        if (this._basePrice != other.getBasePrice())           return false;
        if (this._kmPrice != other.getKMPrice())               return false;
        
        return true;
    }

    @Override
    public String toString()
    {
        return _type + "[" + "standing places: " + _standingPlaces + ", seats: " + _seats + ", baseprice: " + _basePrice + ", km price:" + _kmPrice + ']';
    }

}
