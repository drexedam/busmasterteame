package at.teame.busmaster.domain.guidata.bus;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.bus.BusTypeDI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourOrderDI;
import at.teame.busmaster.domain.domaininterface.module.ModuleDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.bus.BusType;
import at.teame.busmaster.domain.persistencedata.module.Module;

public class GuiBus implements BusDI {
	
	private String _numberPlate;
	private BusTypeDI _type;
	private List<ModuleDI> _modules;

	public GuiBus(String numberPlate, BusTypeDI type, List<ModuleDI> modules) {
		_numberPlate = numberPlate;
		_type = type;
		_modules = modules;
	}
	
	@Override
	public String getNumberPlate() {
		return _numberPlate;
	}

	@Override
	public BusTypeDI getType() {
		return _type;
	}

	@Override
	public List<? extends ModuleDI> getModules() {
		return _modules;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {

		List<Module> modules = new LinkedList<>();
		for(ModuleDI m : _modules) {
			modules.add((Module) m.convertToDomain());
		}
		
		return new Bus(_numberPlate, (BusType) _type.convertToDomain());

	}

	@Override
	public boolean isAvailable(Date start, Date end) throws PeriodException {
		//Immer true, weil Objekt noch nicht in der DB
		return true;
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                return false;
        if (!(obj instanceof BusDI))                                    return false;
        
        BusDI other = (BusDI) obj;
        
        if (!Objects.equals(this._numberPlate, other.getNumberPlate())) return false;
        if (!Objects.equals(this._type, other.getType()))               return false;
        
        return true;
    }

	@Override
	public int getNumOfSeats() {
		int total = _type.getSeats();
		for(ModuleDI m : _modules) {
			total -= m.getRequiredSpace();
		}
		
		return total;
	}

	@Override
	public boolean isAvailable(Date start, Date end, CoachTourOrderDI order)
			throws PeriodException {
		//Da Objekt noch nicht in der DB
		return true;
	}

}
