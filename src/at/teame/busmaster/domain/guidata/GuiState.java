package at.teame.busmaster.domain.guidata;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.StateDI;
import at.teame.busmaster.domain.persistencedata.State;

public class GuiState implements StateDI {

	private String _spec;
	
	public GuiState(String specification) {
		_spec = specification;
	}
	
	@Override
	public String getSpecification() {
		return _spec;
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return new State(_spec);
	}

    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof StateDI))           return false;
        
        StateDI s = (StateDI) obj;
        
        if(!_spec.equals(s.getSpecification())) return false;
        
        return true;
    }

}
