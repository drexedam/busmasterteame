package at.teame.busmaster.domain.guidata.contactdetails;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.contactdetails.ContactDetailsTypeDI;
import at.teame.busmaster.domain.persistencedata.contactdetails.ContactDetailsType;

public class GuiContactDetailsType implements ContactDetailsTypeDI {

	private String _specification;
	
	public GuiContactDetailsType(String specification) {
		_specification = specification;
	}
	
	@Override
	public String getSpecification() {
		return _specification;
	}

    @Override
    public DI convertToDomain() throws PeriodException
    {
        return new ContactDetailsType(_specification);
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                    return false;
        if (!(obj instanceof ContactDetailsTypeDI))                         return false;
        
        ContactDetailsTypeDI other = (ContactDetailsTypeDI) obj;

        if (!Objects.equals(this._specification, other.getSpecification())) return false;
        
        return true;
    }

}
