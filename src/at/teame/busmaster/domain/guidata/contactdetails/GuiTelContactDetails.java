package at.teame.busmaster.domain.guidata.contactdetails;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourCustomerDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.ContactDetailsTypeDI;
import at.teame.busmaster.domain.domaininterface.contactdetails.TelContactDetailsDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.persistencedata.contactdetails.ContactDetailsType;
import at.teame.busmaster.domain.persistencedata.contactdetails.TelContactDetails;

public class GuiTelContactDetails implements TelContactDetailsDI {
	private String _number;
	private DriverDI _driver;
	private CoachTourCustomerDI _customer;
	private ContactDetailsTypeDI _type;
	
	public GuiTelContactDetails(String number, DriverDI driver, CoachTourCustomerDI customer, ContactDetailsTypeDI type) {
		_number = number;
		_driver = driver;
		_customer = customer;
		_type = type;
	}
	
	@Override
	public String getNumber() {
		return _number;
	}

	@Override
	public DriverDI getDriver() {
		return _driver;
	}

	@Override
	public CoachTourCustomerDI getCoachTourCustomer() {
		return _customer;
	}

	@Override
	public ContactDetailsTypeDI getType() {
		return _type;
	}

    @Override
    public DI convertToDomain() throws PeriodException, UncompatibleConvertionException
    {
        ContactDetailsType type     = (ContactDetailsType) _type    .convertToDomain();
        
        return new TelContactDetails(_number, type);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                       return false;
        if (!(obj instanceof TelContactDetailsDI))                             return false;
        
        TelContactDetailsDI other = (TelContactDetailsDI) obj;
        
        if (!Objects.equals(this._number, other.getNumber()))                  return false;
        if (this._driver == null && other.getDriver() != null)                 return false;
        else
        {
            if(this._driver != null && other.getDriver() == null)              return false;
            if (!Objects.equals(this._driver, other.getDriver()))              return false;
        }
        if (this._customer == null && other.getCoachTourCustomer() != null)    return false;
        else
        {
            if(this._customer != null && other.getCoachTourCustomer() == null) return false;
            if (!Objects.equals(this._customer, other.getCoachTourCustomer())) return false;
        }
        if (!Objects.equals(this._type, other.getType()))                      return false;
        
        return true;
    }

}
