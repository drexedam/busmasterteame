package at.teame.busmaster.domain.guidata.schedule;

import java.util.Date;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftDefinitionDI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftScheduleDI;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftDefinition;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftSchedule;

public class GuiDriverShiftSchedule implements DriverShiftScheduleDI {
	
	private Date _date;
	private DriverShiftDefinitionDI _definition;
	private DriverDI _driver;
	
	public GuiDriverShiftSchedule(Date date, DriverShiftDefinitionDI definition, DriverDI driver){
		_date = date;
		_definition = definition;
		_driver = driver;
	}

	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public DriverShiftDefinitionDI getDefinition() {
		return _definition;
	}

	@Override
	public DriverDI getDriver() {
		return _driver;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		return new DriverShiftSchedule(_date,(DriverShiftDefinition) _definition.convertToDomain(),(Driver) _driver.convertToDomain());
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                              return false;
        if (!(obj instanceof DriverShiftScheduleDI))                  return false;
        
        DriverShiftScheduleDI other = (DriverShiftScheduleDI) obj;
        
        if (!Objects.equals(this._date, other.getDate()))             return false;
        if (!Objects.equals(this._definition, other.getDefinition())) return false;
        if (!Objects.equals(this._driver, other.getDriver()))         return false;
        
        return true;
    }

	@Override
	public int getID() {
		return -1;
	}

}
