package at.teame.busmaster.domain.guidata.schedule;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.publicbustransport.PublicBusTransportDI;
import at.teame.busmaster.domain.domaininterface.schedule.CircleDefinitionDI;
import at.teame.busmaster.domain.domaininterface.schedule.DayTypeDI;
import at.teame.busmaster.domain.persistencedata.schedule.CircleDefinition;

public class GuiCircleDefinition implements CircleDefinitionDI {
	
	private DayTypeDI _dayType;
	private String _specification;
    private BusDI _defaultBus;
    private List<PublicBusTransportDI> _publicBusTransports;
	
	public GuiCircleDefinition(DayTypeDI dayType, String specification){
		_dayType = dayType;
		_specification = specification;
	}

	@Override
	public DayTypeDI getDayType() {
		return _dayType;
	}

	@Override
	public String getSpecification() {
		return _specification;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
        BusDI defaultBus = _defaultBus;
        if(defaultBus != null)
        {
            defaultBus = (BusDI) _defaultBus.convertToDomain();
        }
        
        List<PublicBusTransportDI> publicBusTransports = new LinkedList<>();
        for(PublicBusTransportDI pbt : _publicBusTransports)
        {
            publicBusTransports.add((PublicBusTransportDI) pbt.convertToDomain());
        }
        
		return new CircleDefinition((DayTypeDI) _dayType.convertToDomain(),
                                    _specification,
                                    defaultBus,
                                    publicBusTransports);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                               return false;
        if (!(obj instanceof CircleDefinitionDI))                                      return false;
        
        CircleDefinitionDI other = (CircleDefinitionDI) obj;
        
        if (!Objects.equals(this._dayType, other.getDayType()))                        return false;
        if (!Objects.equals(this._specification, other.getSpecification()))            return false;
        if (!Objects.equals(this._defaultBus, other.getDefaultBus()))                  return false;
        
        if (this._publicBusTransports.size() != other.getPublicBusTransports().size()) return false;
        
        boolean containsAll = true;
        for(PublicBusTransportDI pbt : this._publicBusTransports)
        {
            containsAll &= other.getPublicBusTransports().contains(pbt);
        }
        
        if(!containsAll)                                                               return false;
        
        return true;
    }

    @Override
    public BusDI getDefaultBus()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<? extends PublicBusTransportDI> getPublicBusTransports()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
