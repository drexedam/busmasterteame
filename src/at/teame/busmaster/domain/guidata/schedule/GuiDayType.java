package at.teame.busmaster.domain.guidata.schedule;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.schedule.DayTypeDI;
import at.teame.busmaster.domain.persistencedata.schedule.DayType;

public class GuiDayType implements DayTypeDI {
	
	private String _specification;
	private String _description;
	
	public GuiDayType(String specification, String description){
		_specification = specification;
		_description = description;
	}

	@Override
	public String getSpecification() {
		return _specification;
	}

	@Override
	public String getDescription() {
		return _description;
	}

	@Override
	public DI convertToDomain() throws PeriodException {
		return new DayType(_specification, _description);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        if (!(obj instanceof DayTypeDI))                                    return false;
        
        DayTypeDI other = (DayTypeDI) obj;
        
        if (!Objects.equals(this._specification, other.getSpecification())) return false;
        if (!Objects.equals(this._description, other.getDescription()))     return false;
        
        return true;
    }

}
