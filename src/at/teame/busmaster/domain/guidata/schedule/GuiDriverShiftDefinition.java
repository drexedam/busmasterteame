package at.teame.busmaster.domain.guidata.schedule;

import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.schedule.DayTypeDI;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftDefinitionDI;
import at.teame.busmaster.domain.persistencedata.schedule.DayType;
import at.teame.busmaster.domain.persistencedata.schedule.DriverShiftDefinition;

public class GuiDriverShiftDefinition implements DriverShiftDefinitionDI {

	private DayTypeDI _dayType;
	private String _specification;
	
	public GuiDriverShiftDefinition(GuiDayType dayType, String specification){
		_dayType = dayType;
		_specification = specification;
	}
	
	@Override
	public DayTypeDI getDayType() {
		return _dayType;
	}

	@Override
	public String getSpecification() {
		return _specification;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		return new DriverShiftDefinition((DayType) _dayType.convertToDomain(), _specification);
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                                    return false;
        if (!(obj instanceof DriverShiftDefinitionDI))                      return false;
        
        DriverShiftDefinitionDI other = (DriverShiftDefinitionDI) obj;
        
        if (!Objects.equals(this._dayType, other.getDayType()))             return false;
        if (!Objects.equals(this._specification, other.getSpecification())) return false;
        
        return true;
    }

}
