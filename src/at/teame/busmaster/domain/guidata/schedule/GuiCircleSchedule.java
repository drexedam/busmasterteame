package at.teame.busmaster.domain.guidata.schedule;

import java.util.Date;
import java.util.Objects;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.DI;
import at.teame.busmaster.domain.domaininterface.bus.BusDI;
import at.teame.busmaster.domain.domaininterface.schedule.CircleDefinitionDI;
import at.teame.busmaster.domain.domaininterface.schedule.CircleScheduleDI;
import at.teame.busmaster.domain.persistencedata.bus.Bus;
import at.teame.busmaster.domain.persistencedata.schedule.CircleDefinition;
import at.teame.busmaster.domain.persistencedata.schedule.CircleSchedule;

public class GuiCircleSchedule implements CircleScheduleDI {
	
	private Date _date;
	private CircleDefinitionDI _definition;
	private BusDI _bus;

	public GuiCircleSchedule(Date date, CircleDefinitionDI definition, BusDI bus){
		_date = date;
		_definition = definition;
		_bus = bus;
	}
	
	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public CircleDefinitionDI getDefinition() {
		return _definition;
	}

	@Override
	public BusDI getBus() {
		return _bus;
	}

	@Override
	public DI convertToDomain() throws PeriodException, UncompatibleConvertionException {
		return new CircleSchedule(_date,(CircleDefinition) _definition.convertToDomain(),(Bus) _bus.convertToDomain());
	}

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)                                              return false;
        if (!(obj instanceof CircleScheduleDI))                       return false;
        
        CircleScheduleDI other = (CircleScheduleDI) obj;
        
        if (!Objects.equals(this._date, other.getDate()))             return false;
        if (!Objects.equals(this._definition, other.getDefinition())) return false;
        if (!Objects.equals(this._bus, other.getBus()))               return false;
        
        return true;
    }

}
