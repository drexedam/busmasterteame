package at.teame.busmaster.domain;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.domain.domaininterface.DI;

/**
 * @author Team E
 * @version 0.3
 */
public interface Updateable {
    /**
     * 
     * @param source the datas source
     * @throws UncompatibleConvertionException if there is an error in the data
     * @throws PeriodException if there is a twist in the datas
     */
	public void update(DI source)  throws UncompatibleConvertionException, PeriodException;
}
