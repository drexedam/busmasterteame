package at.teame.busmaster.bundles;

import java.util.Locale;

public enum LanguageLocale {
	english("en","GB"),
	deutsch("de","DE");
	
	private String language;
	private String country;
	
	private  LanguageLocale(String lang, String count){
		language=lang;
		country = count;
	}
	
	public static Locale getLanguageLocale(LanguageLocale ll){
		return new Locale(ll.language,ll.country);
	}
}
