package at.teame.busmaster.bundles;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;


public final class Msg {
	private static Locale loc = new Locale("en","GB");
	private static ResourceBundle msg = ResourceBundle.getBundle("at.teame.busmaster.bundles/MessagesBundle",loc);

	public static String getString(String key){
		return msg.getString(key);
	}
	
	public static String getString(String key, Object[] args){
		return MessageFormat.format(msg.getString(key), args);
	}
	
	public static void switchLanguage(Locale loc){
		msg = ResourceBundle.getBundle("at.teame.busmaster.bundles/MessagesBundle",loc);
	}
}
