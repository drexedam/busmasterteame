function popup_wait_open() {
	var l = document.getElementById('light');
	
	l.innerHTML = "Executing task. Please wait<br /><img src='ajax-loader.gif' />"; 
    window.scrollTo(0,0);
    l.style.display='block';
    document.getElementById('fade').style.display='block';  
}
function popup_open(code, message){
	var l = document.getElementById('light');
	if(code === "OK") {
		l.innerHTML = message + "<br /><br /><form action='overview.xhtml'><input value='Ok' type='submit' class='pure-button pure-button-primary' /></form>"; 
	} else {
		l.innerHTML = message + "<br /><br /><input value='Ok' type='submit' class='pure-button pure-button-primary' onclick='popup_close()'/>"; 

	}
    window.scrollTo(0,0);
    l.style.display='block';
    document.getElementById('fade').style.display='block';  
}

function popup_close(){
    document.getElementById('light').style.display='none';
    document.getElementById('fade').style.display='none';
}