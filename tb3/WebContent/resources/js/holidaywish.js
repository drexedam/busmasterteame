function wishHol() {
	 xhr = new XMLHttpRequest();
	 if (!xhr) {
		 alert("An Error occurred when trying to initialize XMLHttpRequest!");
		 return; // exit
	 } 
	 xhr.onreadystatechange = wishHol_callback;
	 start = encodeURIComponent(document.getElementById("datepicker_start").value);
	 end = encodeURIComponent(document.getElementById("datepicker_end").value);
	 xhr.open("GET", "HolidayController?start="+start+"&end="+end, true);
	 xhr.send(null);
	 popup_wait_open();
}

function wishHol_callback() {
	if((xhr.readyState == 4) && (xhr.status==200)) {
		popup_open(
				xhr.responseXML.getElementsByTagName("code").item(0).firstChild.nodeValue,
				xhr.responseXML.getElementsByTagName("message").item(0).firstChild.nodeValue
		);
	}
}

