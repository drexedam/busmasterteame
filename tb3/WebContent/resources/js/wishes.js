function delWish(wishId, identifier) {
	 xhr = new XMLHttpRequest();
	 if (!xhr) {
		 alert("An Error occurred when trying to initialize XMLHttpRequest!");
		 return; // exit
	 } 
	 xhr.onreadystatechange = delWish_callback;
	 xhr.open("GET", "WishesController?wishID="+wishId+"&ident="+identifier, true);
	 xhr.send(null);
	 popup_wait_open();
}

function delWish_callback() {
	if((xhr.readyState == 4) && (xhr.status==200)) {
		popup_open(
				xhr.responseXML.getElementsByTagName("code").item(0).firstChild.nodeValue,
				xhr.responseXML.getElementsByTagName("message").item(0).firstChild.nodeValue
		);
	}
}
