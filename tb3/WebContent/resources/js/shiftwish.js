function wishShi() {
	 xhr = new XMLHttpRequest();
	 if (!xhr) {
		 alert("An Error occurred when trying to initialize XMLHttpRequest!");
		 return; // exit
	 } 
	 xhr.onreadystatechange = wishShi_callback;
	 start = encodeURIComponent(document.getElementById("startDate").innerHTML);
	 end = encodeURIComponent(document.getElementById("endDate").innerHTML);
	 xhr.open("GET", "ShiftController?start="+start+"&end="+end, true);
	 xhr.send(null);
	 //popup_wait_open();
}

function wishShi_callback() {
	if((xhr.readyState == 4) && (xhr.status==200)) {		
		shifts = xhr.responseXML.getElementsByTagName("shift");
		document.getElementById("shifts").innerHTML = "";
		for(var i = 0; i < shifts.length; i++) {
			res = "";
			var input = document.createElement("input");
			input.setAttribute("type", "checkbox");
			input.setAttribute("name", "wished");
			input.setAttribute("value", shifts.item(i).getElementsByTagName("id").item(0).firstChild.nodeValue);
			input.setAttribute("id", "cb"+i);
			document.getElementById("shifts").appendChild(input);
			lbl = document.createElement("label");
			lbl.htmlFor = "cb"+i;
			res += shifts.item(i).getElementsByTagName("def").item(0).firstChild.nodeValue;
			res += shifts.item(i).getElementsByTagName("date").item(0).firstChild.nodeValue;
			lbl.appendChild(document.createTextNode(res));
			document.getElementById("shifts").appendChild(lbl);
			document.getElementById("shifts").appendChild(document.createElement("br"));
		}
	}
}
function doWish() {
	 xhr = new XMLHttpRequest();
	 if (!xhr) {
		 alert("An Error occurred when trying to initialize XMLHttpRequest!");
		 return; // exit
	 } 
	 xhr.onreadystatechange = doWish_callback;
	 
	 checkboxes = document.getElementsByName("wished");
     params = "";
     parNum = 0;
     for (var i = 0; i < checkboxes.length; i++) {
        
     	params += "id"+parNum+"="+checkboxes[i].value.replace(/(\r\n|\n|\r)/gm,"")+"&"+"w"+parNum+"="+checkboxes[i].checked;
    	parNum++;
    	if(i+1<checkboxes.length) {
        	params += "&";
        }
        
        
        
     }
	 xhr.open("POST", "ShiftController", true);
	 xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	 xhr.setRequestHeader("Content-length", params.length);
	 xhr.setRequestHeader("Connection", "close");
	 xhr.send(params);
	 popup_wait_open();
}

function doWish_callback() {
	if((xhr.readyState == 4) && (xhr.status==200)) {
		popup_open(
				xhr.responseXML.getElementsByTagName("code").item(0).firstChild.nodeValue,
				xhr.responseXML.getElementsByTagName("message").item(0).firstChild.nodeValue
		);
		
	}
}

