function coachTourHol() {
    xhr = new XMLHttpRequest();
    if (!xhr) {
        alert("An Error occurred when trying to initialize XMLHttpRequest!");
        return; // exit
    }
    xhr.onreadystatechange = coachTour_callback;
    checkboxes = document.getElementsByName("selectable");
    params = "";
    parNum = 0;
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
        	params += "s"+parNum+"="+checkboxes[i].value;
        	parNum++;
        	if(i+1<checkboxes.length && checkboxes[i+1].checked) {
            	params += "&";
            }
        }
        
        
    }
    params = "CoachTourWishController?"+params;
    xhr.open("GET", params, true);
	xhr.send(null);
    popup_wait_open();
}

function coachTour_callback() {
    if ((xhr.readyState == 4) && (xhr.status == 200)) {
        popup_open(
                xhr.responseXML.getElementsByTagName("code").item(0).firstChild.nodeValue,
                xhr.responseXML.getElementsByTagName("message").item(0).firstChild.nodeValue
                );
    }
}