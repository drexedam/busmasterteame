/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import at.teame.busmaster.beans.User;

/**
 *
 * @author Morent Jochen <jochen.morent@students.fhv.at>
 */
public class LoginFilter implements Filter
{
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
            throws IOException, ServletException
    {
        HttpServletRequest httpSR = (HttpServletRequest) request;
        User user = (User) httpSR.getSession().getAttribute("user");
        //check if user is logged in
        if(user == null || !user.isLoggedIn())
        {
            //not logged in
            String url = httpSR.getRequestURL().toString();
            httpSR.getSession().setAttribute("beforFilter", url);
            
            request.getRequestDispatcher("/faces/driverLogin.xhtml").forward(request, response);
        }
        else
        {
            //logged in
            chain.doFilter(request, response);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}
    @Override
    public void destroy() {}
}
