package at.teame.busmaster.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.beans.User;
import at.teame.busmaster.controller.tb3.HolidayWishController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.persistencedata.driver.Driver;
import at.teame.busmaster.domain.persistencedata.driver.HolidayWish;


@WebServlet(urlPatterns = {"/HolidayController"})
public class HolidayGuiController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//get request parameter for start date
        String start = request.getParameter("start");
        //get request parameter for end date
		String end = request.getParameter("end");
		
		//if both parameters are not null a response message is generated
		if(start != null && end != null) {
			response.setContentType("text/xml");
			PrintWriter out = response.getWriter();
			out.println("<content>");
            //try to save the holiday request and write the corresponding response code and message
			if(saveWish(start, end, request)) {
				out.println("<code>OK</code>");
				out.println("<message>Holiday request saved!</message>");
			} else {
				out.println("<code>FAILURE</code>");
				out.println("<message>Holiday could not be requested! Please select another period.</message>");
			}
			
			out.println("</content>");
			out.flush();
		}
	}

	/**
     *
     * @param start begin date of holiday request
     * @param end   end date of holiday request
     * @param request httprequest
     * @return default return value is true; if there is an error while saving return false
     */
	private boolean saveWish(String start, String end, HttpServletRequest request) {
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		Date startD = null;
		Date endD = null;
		try {
			startD = df.parse(start);
			endD = df.parse(end);
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		
		HolidayWishController hwc = new HolidayWishController();
        //check available resources
		if(hwc.enoughResourcesAvailable(startD, endD)) {
			try {
				hwc.saveHolidayWish(new HolidayWish(startD, endD, (Driver) ((User)request.getSession().getAttribute("user")).getDriver().convertToDomain()));
			} catch (DBException | PeriodException
					| UncompatibleConvertionException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
		
		
		return true;
	}
	
}
