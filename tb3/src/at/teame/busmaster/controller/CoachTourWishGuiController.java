package at.teame.busmaster.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.beans.User;
import at.teame.busmaster.controller.tb3.CoachTourWishController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.persistencedata.driver.CoachTourWish;

@WebServlet(urlPatterns =
{
    "/CoachTourWishController"
})
public class CoachTourWishGuiController extends HttpServlet
{
    private final CoachTourWishController _CTWC = CoachTourWishController.getInstance();
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //----------------------------------
        //Read Parameters
        //----------------------------------
        //List of Schedules (selected in form)
        List<CoachTourScheduleDI> checkedIDs = new LinkedList<>();
        String paramID = request.getParameter("s0");
        for (int i = 1; paramID != null; i++)
        {
            checkedIDs.add(_CTWC.getCoachTourScheduleByID(Integer.parseInt(paramID)));
            paramID = request.getParameter("s" + i);
        }
        
        //----------------------------------
        //Generate Response
        //----------------------------------
        response.setContentType("text/xml");
        PrintWriter out = response.getWriter();
        out.println("<content>");
        if (saveWish(checkedIDs, request))
        {
            out.println("<code>OK</code>");
            out.println("<message>Coachtour request saved!</message>");
        } else
        {
            out.println("<code>FAILURE</code>");
            out.println("<message>Could not save coach tour request!</message>");
        }
        out.println("</content>");
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }

    private boolean saveWish(List<CoachTourScheduleDI> selected, HttpServletRequest request)
    {
        //get the user currently logged in
        User logedInUser = (User) ((HttpServletRequest) request).getSession().getAttribute("user");
        //check if a tour is selected and user is logged in
        if(selected.isEmpty() && logedInUser != null)
        {
            return false;
        }
        //get the driver connected to this user account
        DriverDI driver = logedInUser.getDriver();
        //for each selected tour try to save a coach tour request
        for(CoachTourScheduleDI cts : selected)
        {
            try
            {
                _CTWC.saveCoachtourWish(new CoachTourWish(driver, cts));
            } catch (DBException | PeriodException | UncompatibleConvertionException ex)
            {
                Logger.getLogger(CoachTourWishGuiController.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        //if save is successful return true
        return true;
    }

}
