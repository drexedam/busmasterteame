package at.teame.busmaster.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.controller.tb3.CoachTourWishController;
import at.teame.busmaster.controller.tb3.HolidayWishController;
import at.teame.busmaster.controller.tb3.ShiftWishController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.driver.CoachTourWishDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.HolidayWishDI;
import at.teame.busmaster.domain.domaininterface.driver.ShiftWishDI;

@WebServlet(urlPatterns = {"/WishesController"})
public class WishesGuiController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//get id from parameter
        int wishID = Integer.parseInt(request.getParameter("wishID"));
		//identifier for the type of request
        String ident = request.getParameter("ident");
        //check request type and remove it
		switch (ident) {
		case "holiday":
			new HolidayWishController().removeHolidayWish(wishID);
			break;
		case "coachtour":
			new CoachTourWishController().removeCoachtourWish(wishID);
			break;
		case "shift":
			new ShiftWishController().removeShiftWish(wishID);
			break;
		default:
			break;
		}
		//generate response message
		response.setContentType("text/xml");
		PrintWriter out = response.getWriter();
		out.println("<content>");
		out.println("<code>OK</code>");
		out.println("<message>Request deleted!</message>");
		out.println("</content>");
		out.flush();
	}

	/**
     *
     * @param driver driver
     * @return all holiday request made by this driver
     */
	@SuppressWarnings("unchecked")
	public static List<HolidayWishDI> getWishes(DriverDI driver) {
		
		HolidayWishController hwc = new HolidayWishController();
		try {
			return (List<HolidayWishDI>) hwc.getAllHolidayWishes(driver);
		} catch (DBException | PeriodException
				| UncompatibleConvertionException e) {
			e.printStackTrace();
		}
		
		return new LinkedList<>();
	}

    /**
     *
     * @param driver driver
     * @return all coach tour request made by this driver
     */
	@SuppressWarnings("unchecked")
	public static List<CoachTourWishDI> getCoachTourWishes(DriverDI driver) {
		
		CoachTourWishController ctwc = new CoachTourWishController();
		try {
			return (List<CoachTourWishDI>) ctwc.getAllCoachtourWishes(driver);
		} catch (DBException | PeriodException
				| UncompatibleConvertionException e1) {
			e1.printStackTrace();
		}
		
		
		return new LinkedList<>();
	}

    /**
     *
     * @param driver driver
     * @return all shift requests made by this driver
     */
	@SuppressWarnings("unchecked")
	public static List<ShiftWishDI> getShiftWishes(DriverDI driver) {
		ShiftWishController swc = new ShiftWishController();
		try {
			return (List<ShiftWishDI>) swc.getAllShiftWishes(driver);
		} catch (DBException | PeriodException
				| UncompatibleConvertionException e) {
			e.printStackTrace();
		}
		
		return new LinkedList<>();
	}
}
