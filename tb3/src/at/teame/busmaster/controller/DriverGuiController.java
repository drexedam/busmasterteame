package at.teame.busmaster.controller;

import at.teame.busmaster.controller.LoginController;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.user.UserDI;

public class DriverGuiController {


    /**
     *
     * @param uName Username
     * @param pw Password
     * @return If login was successful return the driver else return null
     */
	public DriverDI login(String uName, String pw) {
		
		LoginController lc = new LoginController();
        
		
        UserDI u = lc.driverLogin(uName, pw);
        
        if(u != null)
        {
            return u.getDriver();
        }
        
		return null;
	}
	
}
