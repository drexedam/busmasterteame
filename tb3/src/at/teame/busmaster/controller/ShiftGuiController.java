package at.teame.busmaster.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.beans.User;
import at.teame.busmaster.controller.tb3.ShiftWishController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.schedule.DriverShiftScheduleDI;
import at.teame.busmaster.domain.guidata.driver.GuiShiftWish;

/**
 * Servlet implementation class ShiftGuiController
 */
@WebServlet(urlPatterns = {"/ShiftController"})
public class ShiftGuiController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ShiftWishController swc = new ShiftWishController();
	private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShiftGuiController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//get request parameter for start date
        String s = request.getParameter("start");
        //get request parameter for end dates
		String e = request.getParameter("end");


		if(s != null && e != null) {
            //parse date from string
			Date start = formatDate(s);
			Date end = formatDate(e);
			response.setContentType("text/xml");
			PrintWriter out = response.getWriter();
			out.println("<content>");

            //get all shifts the driver is assigned to
			try {
				for(DriverShiftScheduleDI dss : new ShiftWishController().getAllSchedules(((User)request.getSession().getAttribute("user")).getDriver(), start, end)) {
					out.println("<shift>");
					out.println("<id>");
					out.println(dss.getID());
					out.println("</id>");
					out.println("<def>");
					out.println(dss.getDefinition().getSpecification());
					out.println("</def>");
					out.println("<date>");
					out.println(sdf.format(dss.getDate()));
					out.println("</date>");
					out.println("</shift>");
				}
			} catch (DBException | PeriodException
					| UncompatibleConvertionException e1) {
				e1.printStackTrace();
			}
			
			
			out.println("</content>");
			out.flush();
			
		} else if(request.getParameter("id0") != null && request.getParameter("w0") != null) {
			String paramID = request.getParameter("id0");
			String paramW = request.getParameter("w0");
			DriverShiftScheduleDI temp = null;
	        for (int i = 1; paramID != null && paramW != null; i++)
	        {
	        	
	           temp = swc.getShiftByID(Integer.parseInt(paramID));
                //check for positive request
	           if(Boolean.parseBoolean(paramW)) {
	               //check if the shift request is possible and save the request
		           if(swc.isPossible(temp)) {
		        	   try {
						swc.saveShiftWish(new GuiShiftWish(((User)request.getSession().getAttribute("user")).getDriver(), temp, !Boolean.parseBoolean(paramW)));
		        	   } catch (DBException | PeriodException
							| UncompatibleConvertionException e1) {
						e1.printStackTrace();
		        	   }
		           }
	           } else {
	        	   try {
						swc.saveShiftWish(new GuiShiftWish(((User)request.getSession().getAttribute("user")).getDriver(), temp, !Boolean.parseBoolean(paramW)));
					} catch (DBException | PeriodException
							| UncompatibleConvertionException e1) {
						e1.printStackTrace();
					}
	           }
	           paramID = request.getParameter("id" + i);
	           paramW = request.getParameter("w" + i);
	        }
	        
	        response.setContentType("text/xml");
	        PrintWriter out = response.getWriter();
	        out.println("<content>");
	        out.println("<code>OK</code>");
	        out.println("<message>All possible requests saved!</message>");
	        out.println("</content>");
	        out.flush();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

    /**
     *
     * @param s input string
     * @return the date object parsed from the string
     */
	private static Date formatDate(String s) {
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		try {
			return df.parse(s);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
