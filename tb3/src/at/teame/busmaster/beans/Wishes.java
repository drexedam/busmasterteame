package at.teame.busmaster.beans;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import at.teame.busmaster.controller.WishesGuiController;
import at.teame.busmaster.domain.domaininterface.driver.CoachTourWishDI;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;
import at.teame.busmaster.domain.domaininterface.driver.HolidayWishDI;
import at.teame.busmaster.domain.domaininterface.driver.ShiftWishDI;

@ManagedBean
@SessionScoped
public class Wishes {

	private List<HolidayWishDI> holidays = new LinkedList<>();
	private List<CoachTourWishDI> coachTours = new LinkedList<>();
	private List<ShiftWishDI> shifts = new LinkedList<>();
	@ManagedProperty(value = "#{user}")
	private User user;

    /**
     *
     * @return the driver connected to this user account
     */
	public DriverDI getDriver() { return user.getDriver(); }

    /**
     *
     * @return list of all holiday request made by the driver
     */
	public List<HolidayWishDI> getHolidays() {
		
		holidays = WishesGuiController.getWishes(user.getDriver());
		
		return holidays;
	}

    /**
     *
     * @param values list of holiday requests to be set
     */
	public void setHolidays(List<HolidayWishDI> values) {holidays = values;}

    /**
     *
     * @return list of all coach tour requests made by the driver
     */
	public List<CoachTourWishDI> getCoachTours(){
		coachTours = WishesGuiController.getCoachTourWishes(user.getDriver());
		
		return coachTours;
	}

    /**
     *
     * @param values list of coach tour requests to be set
     */
	public void setCoachTours(List<CoachTourWishDI> values){coachTours = values;}

    /**
     *
     * @return list of shift requests made by the driver
     */
	public List<ShiftWishDI> getShifts(){
		shifts = WishesGuiController.getShiftWishes(user.getDriver());
		
		return shifts;
	}

    /**
     *
     * @param values list of shift requests to be set
     */
	public void setShifts(List<ShiftWishDI> values){shifts = values;}

    /**
     *
     * @return the user
     */
	public User getUser() {return user;}

    /**
     *
     * @param value user to be set
     */
	public void setUser(User value){user = value;}
	
	//TODO andere wuensche hinzufuegen
	
	
}
