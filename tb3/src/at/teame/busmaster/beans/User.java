package at.teame.busmaster.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import at.teame.busmaster.controller.DriverGuiController;
import at.teame.busmaster.domain.domaininterface.driver.DriverDI;

@ManagedBean
@SessionScoped
public class User  {
	private String   name;
	private String   pw;
	private DriverDI d;
	private String err = "";
	
	public void setName(String value) {name = value;}
	public String getName() {return name;}
	
	public void setPw(String value) {pw = value;}
	public String getPw() {return pw;}
	
	public String getErr() {
		return err;
	}

    /**
     *
     * @return response message
     */
	public String login() {
		err = "";
		d = new DriverGuiController().login(name, pw);
		if(d == null) {
			err = "Login failed! Please try again.";
			return "failure";
		}
		return "overview";
	}

    /**
     *
     * @return response message
     */
	public String logout() {
		d = null;
		return "driverLogin";
	}

    /**
     *
     * @return driver first name and last name as single string
     */
	public String getDriverName() {return d.getFirstName()+" "+d.getLastName();}

    /**
     *
     * @return true if the user is loged in
     */
    public boolean isLoggedIn()
    {
        return d != null;
    }

    /**
     *
     * @return the driver connected to this user account
     */
	public DriverDI getDriver() {
		return d;
	}
	
}

