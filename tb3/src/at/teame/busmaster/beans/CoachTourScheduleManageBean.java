/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.teame.busmaster.beans;

import at.gruppe2.exceptions.PeriodException;
import at.teame.busmaster.controller.DBException;
import at.teame.busmaster.controller.tb3.CoachTourWishController;
import at.teame.busmaster.domain.UncompatibleConvertionException;
import at.teame.busmaster.domain.domaininterface.coachtour.CoachTourScheduleDI;
import at.teame.busmaster.domain.domaininterface.driver.CoachTourWishDI;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Morent Jochen <jochen.morent@students.fhv.at>
 */
@ManagedBean
@SessionScoped
public class CoachTourScheduleManageBean
{
	@ManagedProperty(value = "#{user}")
	private User user;
    
	public User getUser() {return user;}
	public void setUser(User value){user = value;}
    
    
    private CoachTourWishController cwc = new CoachTourWishController();
    /**
     * Creates a new instance of CoachTourManageBean
     */
    public CoachTourScheduleManageBean()
    {
    }

    /**
     *
     * @return list of all coach tour schedules
     */
    @SuppressWarnings("unchecked")
	public List<CoachTourScheduleDI> getCoachTourSchedules()
    {
        try
        {
            List<CoachTourScheduleDI> ret = (List<CoachTourScheduleDI>) cwc.getAllNotPlanedSchedules();
            List<CoachTourScheduleDI> wishesSchedules = new LinkedList<>();
            
            List<CoachTourWishDI> wishes = (List<CoachTourWishDI>) cwc.getAllCoachtourWishes(user.getDriver());
            
            for(CoachTourWishDI ctw : wishes)
            {
                wishesSchedules.add(ctw.getCoachTourSchedule());
            }
            
            ret.removeAll(wishesSchedules);
            
            return ret;
        } catch (DBException | PeriodException | UncompatibleConvertionException ex)
        {
            Logger.getLogger(CoachTourScheduleManageBean.class.getName()).log(Level.SEVERE, null, ex);
            return new LinkedList<>(); //return empty list
        }
    }
}
